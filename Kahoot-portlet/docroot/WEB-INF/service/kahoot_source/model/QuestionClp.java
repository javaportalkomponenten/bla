/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import kahoot_source.service.ClpSerializer;
import kahoot_source.service.QuestionLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author krzysiek
 */
public class QuestionClp extends BaseModelImpl<Question> implements Question {
	public QuestionClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Question.class;
	}

	@Override
	public String getModelClassName() {
		return Question.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _questionid;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setQuestionid(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _questionid;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("questionid", getQuestionid());
		attributes.put("questiontext", getQuestiontext());
		attributes.put("time", getTime());
		attributes.put("bildurl", getBildurl());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long questionid = (Long)attributes.get("questionid");

		if (questionid != null) {
			setQuestionid(questionid);
		}

		String questiontext = (String)attributes.get("questiontext");

		if (questiontext != null) {
			setQuestiontext(questiontext);
		}

		Long time = (Long)attributes.get("time");

		if (time != null) {
			setTime(time);
		}

		String bildurl = (String)attributes.get("bildurl");

		if (bildurl != null) {
			setBildurl(bildurl);
		}
	}

	@Override
	public long getQuestionid() {
		return _questionid;
	}

	@Override
	public void setQuestionid(long questionid) {
		_questionid = questionid;

		if (_questionRemoteModel != null) {
			try {
				Class<?> clazz = _questionRemoteModel.getClass();

				Method method = clazz.getMethod("setQuestionid", long.class);

				method.invoke(_questionRemoteModel, questionid);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getQuestiontext() {
		return _questiontext;
	}

	@Override
	public void setQuestiontext(String questiontext) {
		_questiontext = questiontext;

		if (_questionRemoteModel != null) {
			try {
				Class<?> clazz = _questionRemoteModel.getClass();

				Method method = clazz.getMethod("setQuestiontext", String.class);

				method.invoke(_questionRemoteModel, questiontext);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Long getTime() {
		return _time;
	}

	@Override
	public void setTime(Long time) {
		_time = time;

		if (_questionRemoteModel != null) {
			try {
				Class<?> clazz = _questionRemoteModel.getClass();

				Method method = clazz.getMethod("setTime", Long.class);

				method.invoke(_questionRemoteModel, time);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getBildurl() {
		return _bildurl;
	}

	@Override
	public void setBildurl(String bildurl) {
		_bildurl = bildurl;

		if (_questionRemoteModel != null) {
			try {
				Class<?> clazz = _questionRemoteModel.getClass();

				Method method = clazz.getMethod("setBildurl", String.class);

				method.invoke(_questionRemoteModel, bildurl);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getQuestionRemoteModel() {
		return _questionRemoteModel;
	}

	public void setQuestionRemoteModel(BaseModel<?> questionRemoteModel) {
		_questionRemoteModel = questionRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _questionRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_questionRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			QuestionLocalServiceUtil.addQuestion(this);
		}
		else {
			QuestionLocalServiceUtil.updateQuestion(this);
		}
	}

	@Override
	public Question toEscapedModel() {
		return (Question)ProxyUtil.newProxyInstance(Question.class.getClassLoader(),
			new Class[] { Question.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		QuestionClp clone = new QuestionClp();

		clone.setQuestionid(getQuestionid());
		clone.setQuestiontext(getQuestiontext());
		clone.setTime(getTime());
		clone.setBildurl(getBildurl());

		return clone;
	}

	@Override
	public int compareTo(Question question) {
		long primaryKey = question.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof QuestionClp)) {
			return false;
		}

		QuestionClp question = (QuestionClp)obj;

		long primaryKey = question.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{questionid=");
		sb.append(getQuestionid());
		sb.append(", questiontext=");
		sb.append(getQuestiontext());
		sb.append(", time=");
		sb.append(getTime());
		sb.append(", bildurl=");
		sb.append(getBildurl());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(16);

		sb.append("<model><model-name>");
		sb.append("kahoot_source.model.Question");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>questionid</column-name><column-value><![CDATA[");
		sb.append(getQuestionid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>questiontext</column-name><column-value><![CDATA[");
		sb.append(getQuestiontext());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>time</column-name><column-value><![CDATA[");
		sb.append(getTime());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>bildurl</column-name><column-value><![CDATA[");
		sb.append(getBildurl());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _questionid;
	private String _questiontext;
	private Long _time;
	private String _bildurl;
	private BaseModel<?> _questionRemoteModel;
	private Class<?> _clpSerializerClass = kahoot_source.service.ClpSerializer.class;
}