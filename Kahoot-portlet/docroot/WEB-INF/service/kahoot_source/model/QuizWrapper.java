/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Quiz}.
 * </p>
 *
 * @author krzysiek
 * @see Quiz
 * @generated
 */
public class QuizWrapper implements Quiz, ModelWrapper<Quiz> {
	public QuizWrapper(Quiz quiz) {
		_quiz = quiz;
	}

	@Override
	public Class<?> getModelClass() {
		return Quiz.class;
	}

	@Override
	public String getModelClassName() {
		return Quiz.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("quizid", getQuizid());
		attributes.put("title", getTitle());
		attributes.put("timestamp", getTimestamp());
		attributes.put("running", getRunning());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long quizid = (Long)attributes.get("quizid");

		if (quizid != null) {
			setQuizid(quizid);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		Long timestamp = (Long)attributes.get("timestamp");

		if (timestamp != null) {
			setTimestamp(timestamp);
		}

		Boolean running = (Boolean)attributes.get("running");

		if (running != null) {
			setRunning(running);
		}
	}

	/**
	* Returns the primary key of this quiz.
	*
	* @return the primary key of this quiz
	*/
	@Override
	public long getPrimaryKey() {
		return _quiz.getPrimaryKey();
	}

	/**
	* Sets the primary key of this quiz.
	*
	* @param primaryKey the primary key of this quiz
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_quiz.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the quizid of this quiz.
	*
	* @return the quizid of this quiz
	*/
	@Override
	public long getQuizid() {
		return _quiz.getQuizid();
	}

	/**
	* Sets the quizid of this quiz.
	*
	* @param quizid the quizid of this quiz
	*/
	@Override
	public void setQuizid(long quizid) {
		_quiz.setQuizid(quizid);
	}

	/**
	* Returns the title of this quiz.
	*
	* @return the title of this quiz
	*/
	@Override
	public java.lang.String getTitle() {
		return _quiz.getTitle();
	}

	/**
	* Sets the title of this quiz.
	*
	* @param title the title of this quiz
	*/
	@Override
	public void setTitle(java.lang.String title) {
		_quiz.setTitle(title);
	}

	/**
	* Returns the timestamp of this quiz.
	*
	* @return the timestamp of this quiz
	*/
	@Override
	public long getTimestamp() {
		return _quiz.getTimestamp();
	}

	/**
	* Sets the timestamp of this quiz.
	*
	* @param timestamp the timestamp of this quiz
	*/
	@Override
	public void setTimestamp(long timestamp) {
		_quiz.setTimestamp(timestamp);
	}

	/**
	* Returns the running of this quiz.
	*
	* @return the running of this quiz
	*/
	@Override
	public boolean getRunning() {
		return _quiz.getRunning();
	}

	/**
	* Returns <code>true</code> if this quiz is running.
	*
	* @return <code>true</code> if this quiz is running; <code>false</code> otherwise
	*/
	@Override
	public boolean isRunning() {
		return _quiz.isRunning();
	}

	/**
	* Sets whether this quiz is running.
	*
	* @param running the running of this quiz
	*/
	@Override
	public void setRunning(boolean running) {
		_quiz.setRunning(running);
	}

	@Override
	public boolean isNew() {
		return _quiz.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_quiz.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _quiz.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_quiz.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _quiz.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _quiz.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_quiz.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _quiz.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_quiz.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_quiz.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_quiz.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new QuizWrapper((Quiz)_quiz.clone());
	}

	@Override
	public int compareTo(kahoot_source.model.Quiz quiz) {
		return _quiz.compareTo(quiz);
	}

	@Override
	public int hashCode() {
		return _quiz.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<kahoot_source.model.Quiz> toCacheModel() {
		return _quiz.toCacheModel();
	}

	@Override
	public kahoot_source.model.Quiz toEscapedModel() {
		return new QuizWrapper(_quiz.toEscapedModel());
	}

	@Override
	public kahoot_source.model.Quiz toUnescapedModel() {
		return new QuizWrapper(_quiz.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _quiz.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _quiz.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_quiz.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof QuizWrapper)) {
			return false;
		}

		QuizWrapper quizWrapper = (QuizWrapper)obj;

		if (Validator.equals(_quiz, quizWrapper._quiz)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Quiz getWrappedQuiz() {
		return _quiz;
	}

	@Override
	public Quiz getWrappedModel() {
		return _quiz;
	}

	@Override
	public void resetOriginalValues() {
		_quiz.resetOriginalValues();
	}

	private Quiz _quiz;
}