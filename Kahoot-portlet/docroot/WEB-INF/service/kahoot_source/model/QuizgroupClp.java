/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import kahoot_source.service.ClpSerializer;
import kahoot_source.service.QuizgroupLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author krzysiek
 */
public class QuizgroupClp extends BaseModelImpl<Quizgroup> implements Quizgroup {
	public QuizgroupClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Quizgroup.class;
	}

	@Override
	public String getModelClassName() {
		return Quizgroup.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _quizgroupid;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setQuizgroupid(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _quizgroupid;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("quizgroupid", getQuizgroupid());
		attributes.put("name", getName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long quizgroupid = (Long)attributes.get("quizgroupid");

		if (quizgroupid != null) {
			setQuizgroupid(quizgroupid);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}
	}

	@Override
	public long getQuizgroupid() {
		return _quizgroupid;
	}

	@Override
	public void setQuizgroupid(long quizgroupid) {
		_quizgroupid = quizgroupid;

		if (_quizgroupRemoteModel != null) {
			try {
				Class<?> clazz = _quizgroupRemoteModel.getClass();

				Method method = clazz.getMethod("setQuizgroupid", long.class);

				method.invoke(_quizgroupRemoteModel, quizgroupid);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getName() {
		return _name;
	}

	@Override
	public void setName(String name) {
		_name = name;

		if (_quizgroupRemoteModel != null) {
			try {
				Class<?> clazz = _quizgroupRemoteModel.getClass();

				Method method = clazz.getMethod("setName", String.class);

				method.invoke(_quizgroupRemoteModel, name);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getQuizgroupRemoteModel() {
		return _quizgroupRemoteModel;
	}

	public void setQuizgroupRemoteModel(BaseModel<?> quizgroupRemoteModel) {
		_quizgroupRemoteModel = quizgroupRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _quizgroupRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_quizgroupRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			QuizgroupLocalServiceUtil.addQuizgroup(this);
		}
		else {
			QuizgroupLocalServiceUtil.updateQuizgroup(this);
		}
	}

	@Override
	public Quizgroup toEscapedModel() {
		return (Quizgroup)ProxyUtil.newProxyInstance(Quizgroup.class.getClassLoader(),
			new Class[] { Quizgroup.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		QuizgroupClp clone = new QuizgroupClp();

		clone.setQuizgroupid(getQuizgroupid());
		clone.setName(getName());

		return clone;
	}

	@Override
	public int compareTo(Quizgroup quizgroup) {
		long primaryKey = quizgroup.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof QuizgroupClp)) {
			return false;
		}

		QuizgroupClp quizgroup = (QuizgroupClp)obj;

		long primaryKey = quizgroup.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{quizgroupid=");
		sb.append(getQuizgroupid());
		sb.append(", name=");
		sb.append(getName());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append("kahoot_source.model.Quizgroup");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>quizgroupid</column-name><column-value><![CDATA[");
		sb.append(getQuizgroupid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>name</column-name><column-value><![CDATA[");
		sb.append(getName());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _quizgroupid;
	private String _name;
	private BaseModel<?> _quizgroupRemoteModel;
	private Class<?> _clpSerializerClass = kahoot_source.service.ClpSerializer.class;
}