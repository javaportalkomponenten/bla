/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author krzysiek
 * @generated
 */
public class QuestionSoap implements Serializable {
	public static QuestionSoap toSoapModel(Question model) {
		QuestionSoap soapModel = new QuestionSoap();

		soapModel.setQuestionid(model.getQuestionid());
		soapModel.setQuestiontext(model.getQuestiontext());
		soapModel.setTime(model.getTime());
		soapModel.setBildurl(model.getBildurl());

		return soapModel;
	}

	public static QuestionSoap[] toSoapModels(Question[] models) {
		QuestionSoap[] soapModels = new QuestionSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static QuestionSoap[][] toSoapModels(Question[][] models) {
		QuestionSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new QuestionSoap[models.length][models[0].length];
		}
		else {
			soapModels = new QuestionSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static QuestionSoap[] toSoapModels(List<Question> models) {
		List<QuestionSoap> soapModels = new ArrayList<QuestionSoap>(models.size());

		for (Question model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new QuestionSoap[soapModels.size()]);
	}

	public QuestionSoap() {
	}

	public long getPrimaryKey() {
		return _questionid;
	}

	public void setPrimaryKey(long pk) {
		setQuestionid(pk);
	}

	public long getQuestionid() {
		return _questionid;
	}

	public void setQuestionid(long questionid) {
		_questionid = questionid;
	}

	public String getQuestiontext() {
		return _questiontext;
	}

	public void setQuestiontext(String questiontext) {
		_questiontext = questiontext;
	}

	public Long getTime() {
		return _time;
	}

	public void setTime(Long time) {
		_time = time;
	}

	public String getBildurl() {
		return _bildurl;
	}

	public void setBildurl(String bildurl) {
		_bildurl = bildurl;
	}

	private long _questionid;
	private String _questiontext;
	private Long _time;
	private String _bildurl;
}