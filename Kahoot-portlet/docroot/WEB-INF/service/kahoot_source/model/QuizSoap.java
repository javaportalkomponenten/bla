/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author krzysiek
 * @generated
 */
public class QuizSoap implements Serializable {
	public static QuizSoap toSoapModel(Quiz model) {
		QuizSoap soapModel = new QuizSoap();

		soapModel.setQuizid(model.getQuizid());
		soapModel.setTitle(model.getTitle());
		soapModel.setTimestamp(model.getTimestamp());
		soapModel.setRunning(model.getRunning());

		return soapModel;
	}

	public static QuizSoap[] toSoapModels(Quiz[] models) {
		QuizSoap[] soapModels = new QuizSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static QuizSoap[][] toSoapModels(Quiz[][] models) {
		QuizSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new QuizSoap[models.length][models[0].length];
		}
		else {
			soapModels = new QuizSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static QuizSoap[] toSoapModels(List<Quiz> models) {
		List<QuizSoap> soapModels = new ArrayList<QuizSoap>(models.size());

		for (Quiz model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new QuizSoap[soapModels.size()]);
	}

	public QuizSoap() {
	}

	public long getPrimaryKey() {
		return _quizid;
	}

	public void setPrimaryKey(long pk) {
		setQuizid(pk);
	}

	public long getQuizid() {
		return _quizid;
	}

	public void setQuizid(long quizid) {
		_quizid = quizid;
	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		_title = title;
	}

	public long getTimestamp() {
		return _timestamp;
	}

	public void setTimestamp(long timestamp) {
		_timestamp = timestamp;
	}

	public boolean getRunning() {
		return _running;
	}

	public boolean isRunning() {
		return _running;
	}

	public void setRunning(boolean running) {
		_running = running;
	}

	private long _quizid;
	private String _title;
	private long _timestamp;
	private boolean _running;
}