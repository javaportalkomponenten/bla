/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Question}.
 * </p>
 *
 * @author krzysiek
 * @see Question
 * @generated
 */
public class QuestionWrapper implements Question, ModelWrapper<Question> {
	public QuestionWrapper(Question question) {
		_question = question;
	}

	@Override
	public Class<?> getModelClass() {
		return Question.class;
	}

	@Override
	public String getModelClassName() {
		return Question.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("questionid", getQuestionid());
		attributes.put("questiontext", getQuestiontext());
		attributes.put("time", getTime());
		attributes.put("bildurl", getBildurl());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long questionid = (Long)attributes.get("questionid");

		if (questionid != null) {
			setQuestionid(questionid);
		}

		String questiontext = (String)attributes.get("questiontext");

		if (questiontext != null) {
			setQuestiontext(questiontext);
		}

		Long time = (Long)attributes.get("time");

		if (time != null) {
			setTime(time);
		}

		String bildurl = (String)attributes.get("bildurl");

		if (bildurl != null) {
			setBildurl(bildurl);
		}
	}

	/**
	* Returns the primary key of this question.
	*
	* @return the primary key of this question
	*/
	@Override
	public long getPrimaryKey() {
		return _question.getPrimaryKey();
	}

	/**
	* Sets the primary key of this question.
	*
	* @param primaryKey the primary key of this question
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_question.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the questionid of this question.
	*
	* @return the questionid of this question
	*/
	@Override
	public long getQuestionid() {
		return _question.getQuestionid();
	}

	/**
	* Sets the questionid of this question.
	*
	* @param questionid the questionid of this question
	*/
	@Override
	public void setQuestionid(long questionid) {
		_question.setQuestionid(questionid);
	}

	/**
	* Returns the questiontext of this question.
	*
	* @return the questiontext of this question
	*/
	@Override
	public java.lang.String getQuestiontext() {
		return _question.getQuestiontext();
	}

	/**
	* Sets the questiontext of this question.
	*
	* @param questiontext the questiontext of this question
	*/
	@Override
	public void setQuestiontext(java.lang.String questiontext) {
		_question.setQuestiontext(questiontext);
	}

	/**
	* Returns the time of this question.
	*
	* @return the time of this question
	*/
	@Override
	public java.lang.Long getTime() {
		return _question.getTime();
	}

	/**
	* Sets the time of this question.
	*
	* @param time the time of this question
	*/
	@Override
	public void setTime(java.lang.Long time) {
		_question.setTime(time);
	}

	/**
	* Returns the bildurl of this question.
	*
	* @return the bildurl of this question
	*/
	@Override
	public java.lang.String getBildurl() {
		return _question.getBildurl();
	}

	/**
	* Sets the bildurl of this question.
	*
	* @param bildurl the bildurl of this question
	*/
	@Override
	public void setBildurl(java.lang.String bildurl) {
		_question.setBildurl(bildurl);
	}

	@Override
	public boolean isNew() {
		return _question.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_question.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _question.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_question.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _question.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _question.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_question.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _question.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_question.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_question.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_question.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new QuestionWrapper((Question)_question.clone());
	}

	@Override
	public int compareTo(kahoot_source.model.Question question) {
		return _question.compareTo(question);
	}

	@Override
	public int hashCode() {
		return _question.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<kahoot_source.model.Question> toCacheModel() {
		return _question.toCacheModel();
	}

	@Override
	public kahoot_source.model.Question toEscapedModel() {
		return new QuestionWrapper(_question.toEscapedModel());
	}

	@Override
	public kahoot_source.model.Question toUnescapedModel() {
		return new QuestionWrapper(_question.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _question.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _question.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_question.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof QuestionWrapper)) {
			return false;
		}

		QuestionWrapper questionWrapper = (QuestionWrapper)obj;

		if (Validator.equals(_question, questionWrapper._question)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Question getWrappedQuestion() {
		return _question;
	}

	@Override
	public Question getWrappedModel() {
		return _question;
	}

	@Override
	public void resetOriginalValues() {
		_question.resetOriginalValues();
	}

	private Question _question;
}