/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import kahoot_source.service.ClpSerializer;
import kahoot_source.service.QuizLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author krzysiek
 */
public class QuizClp extends BaseModelImpl<Quiz> implements Quiz {
	public QuizClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Quiz.class;
	}

	@Override
	public String getModelClassName() {
		return Quiz.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _quizid;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setQuizid(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _quizid;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("quizid", getQuizid());
		attributes.put("title", getTitle());
		attributes.put("timestamp", getTimestamp());
		attributes.put("running", getRunning());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long quizid = (Long)attributes.get("quizid");

		if (quizid != null) {
			setQuizid(quizid);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		Long timestamp = (Long)attributes.get("timestamp");

		if (timestamp != null) {
			setTimestamp(timestamp);
		}

		Boolean running = (Boolean)attributes.get("running");

		if (running != null) {
			setRunning(running);
		}
	}

	@Override
	public long getQuizid() {
		return _quizid;
	}

	@Override
	public void setQuizid(long quizid) {
		_quizid = quizid;

		if (_quizRemoteModel != null) {
			try {
				Class<?> clazz = _quizRemoteModel.getClass();

				Method method = clazz.getMethod("setQuizid", long.class);

				method.invoke(_quizRemoteModel, quizid);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTitle() {
		return _title;
	}

	@Override
	public void setTitle(String title) {
		_title = title;

		if (_quizRemoteModel != null) {
			try {
				Class<?> clazz = _quizRemoteModel.getClass();

				Method method = clazz.getMethod("setTitle", String.class);

				method.invoke(_quizRemoteModel, title);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getTimestamp() {
		return _timestamp;
	}

	@Override
	public void setTimestamp(long timestamp) {
		_timestamp = timestamp;

		if (_quizRemoteModel != null) {
			try {
				Class<?> clazz = _quizRemoteModel.getClass();

				Method method = clazz.getMethod("setTimestamp", long.class);

				method.invoke(_quizRemoteModel, timestamp);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getRunning() {
		return _running;
	}

	@Override
	public boolean isRunning() {
		return _running;
	}

	@Override
	public void setRunning(boolean running) {
		_running = running;

		if (_quizRemoteModel != null) {
			try {
				Class<?> clazz = _quizRemoteModel.getClass();

				Method method = clazz.getMethod("setRunning", boolean.class);

				method.invoke(_quizRemoteModel, running);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getQuizRemoteModel() {
		return _quizRemoteModel;
	}

	public void setQuizRemoteModel(BaseModel<?> quizRemoteModel) {
		_quizRemoteModel = quizRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _quizRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_quizRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			QuizLocalServiceUtil.addQuiz(this);
		}
		else {
			QuizLocalServiceUtil.updateQuiz(this);
		}
	}

	@Override
	public Quiz toEscapedModel() {
		return (Quiz)ProxyUtil.newProxyInstance(Quiz.class.getClassLoader(),
			new Class[] { Quiz.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		QuizClp clone = new QuizClp();

		clone.setQuizid(getQuizid());
		clone.setTitle(getTitle());
		clone.setTimestamp(getTimestamp());
		clone.setRunning(getRunning());

		return clone;
	}

	@Override
	public int compareTo(Quiz quiz) {
		long primaryKey = quiz.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof QuizClp)) {
			return false;
		}

		QuizClp quiz = (QuizClp)obj;

		long primaryKey = quiz.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{quizid=");
		sb.append(getQuizid());
		sb.append(", title=");
		sb.append(getTitle());
		sb.append(", timestamp=");
		sb.append(getTimestamp());
		sb.append(", running=");
		sb.append(getRunning());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(16);

		sb.append("<model><model-name>");
		sb.append("kahoot_source.model.Quiz");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>quizid</column-name><column-value><![CDATA[");
		sb.append(getQuizid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>title</column-name><column-value><![CDATA[");
		sb.append(getTitle());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>timestamp</column-name><column-value><![CDATA[");
		sb.append(getTimestamp());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>running</column-name><column-value><![CDATA[");
		sb.append(getRunning());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _quizid;
	private String _title;
	private long _timestamp;
	private boolean _running;
	private BaseModel<?> _quizRemoteModel;
	private Class<?> _clpSerializerClass = kahoot_source.service.ClpSerializer.class;
}