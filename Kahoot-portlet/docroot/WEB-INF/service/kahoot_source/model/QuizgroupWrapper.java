/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Quizgroup}.
 * </p>
 *
 * @author krzysiek
 * @see Quizgroup
 * @generated
 */
public class QuizgroupWrapper implements Quizgroup, ModelWrapper<Quizgroup> {
	public QuizgroupWrapper(Quizgroup quizgroup) {
		_quizgroup = quizgroup;
	}

	@Override
	public Class<?> getModelClass() {
		return Quizgroup.class;
	}

	@Override
	public String getModelClassName() {
		return Quizgroup.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("quizgroupid", getQuizgroupid());
		attributes.put("name", getName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long quizgroupid = (Long)attributes.get("quizgroupid");

		if (quizgroupid != null) {
			setQuizgroupid(quizgroupid);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}
	}

	/**
	* Returns the primary key of this quizgroup.
	*
	* @return the primary key of this quizgroup
	*/
	@Override
	public long getPrimaryKey() {
		return _quizgroup.getPrimaryKey();
	}

	/**
	* Sets the primary key of this quizgroup.
	*
	* @param primaryKey the primary key of this quizgroup
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_quizgroup.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the quizgroupid of this quizgroup.
	*
	* @return the quizgroupid of this quizgroup
	*/
	@Override
	public long getQuizgroupid() {
		return _quizgroup.getQuizgroupid();
	}

	/**
	* Sets the quizgroupid of this quizgroup.
	*
	* @param quizgroupid the quizgroupid of this quizgroup
	*/
	@Override
	public void setQuizgroupid(long quizgroupid) {
		_quizgroup.setQuizgroupid(quizgroupid);
	}

	/**
	* Returns the name of this quizgroup.
	*
	* @return the name of this quizgroup
	*/
	@Override
	public java.lang.String getName() {
		return _quizgroup.getName();
	}

	/**
	* Sets the name of this quizgroup.
	*
	* @param name the name of this quizgroup
	*/
	@Override
	public void setName(java.lang.String name) {
		_quizgroup.setName(name);
	}

	@Override
	public boolean isNew() {
		return _quizgroup.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_quizgroup.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _quizgroup.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_quizgroup.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _quizgroup.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _quizgroup.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_quizgroup.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _quizgroup.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_quizgroup.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_quizgroup.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_quizgroup.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new QuizgroupWrapper((Quizgroup)_quizgroup.clone());
	}

	@Override
	public int compareTo(kahoot_source.model.Quizgroup quizgroup) {
		return _quizgroup.compareTo(quizgroup);
	}

	@Override
	public int hashCode() {
		return _quizgroup.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<kahoot_source.model.Quizgroup> toCacheModel() {
		return _quizgroup.toCacheModel();
	}

	@Override
	public kahoot_source.model.Quizgroup toEscapedModel() {
		return new QuizgroupWrapper(_quizgroup.toEscapedModel());
	}

	@Override
	public kahoot_source.model.Quizgroup toUnescapedModel() {
		return new QuizgroupWrapper(_quizgroup.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _quizgroup.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _quizgroup.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizgroup.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof QuizgroupWrapper)) {
			return false;
		}

		QuizgroupWrapper quizgroupWrapper = (QuizgroupWrapper)obj;

		if (Validator.equals(_quizgroup, quizgroupWrapper._quizgroup)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Quizgroup getWrappedQuizgroup() {
		return _quizgroup;
	}

	@Override
	public Quizgroup getWrappedModel() {
		return _quizgroup;
	}

	@Override
	public void resetOriginalValues() {
		_quizgroup.resetOriginalValues();
	}

	private Quizgroup _quizgroup;
}