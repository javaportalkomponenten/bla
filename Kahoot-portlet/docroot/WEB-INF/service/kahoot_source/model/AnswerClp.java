/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import kahoot_source.service.AnswerLocalServiceUtil;
import kahoot_source.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author krzysiek
 */
public class AnswerClp extends BaseModelImpl<Answer> implements Answer {
	public AnswerClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Answer.class;
	}

	@Override
	public String getModelClassName() {
		return Answer.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _answerid;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setAnswerid(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _answerid;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("answerid", getAnswerid());
		attributes.put("answertext", getAnswertext());
		attributes.put("right", getRight());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long answerid = (Long)attributes.get("answerid");

		if (answerid != null) {
			setAnswerid(answerid);
		}

		String answertext = (String)attributes.get("answertext");

		if (answertext != null) {
			setAnswertext(answertext);
		}

		Boolean right = (Boolean)attributes.get("right");

		if (right != null) {
			setRight(right);
		}
	}

	@Override
	public long getAnswerid() {
		return _answerid;
	}

	@Override
	public void setAnswerid(long answerid) {
		_answerid = answerid;

		if (_answerRemoteModel != null) {
			try {
				Class<?> clazz = _answerRemoteModel.getClass();

				Method method = clazz.getMethod("setAnswerid", long.class);

				method.invoke(_answerRemoteModel, answerid);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getAnswertext() {
		return _answertext;
	}

	@Override
	public void setAnswertext(String answertext) {
		_answertext = answertext;

		if (_answerRemoteModel != null) {
			try {
				Class<?> clazz = _answerRemoteModel.getClass();

				Method method = clazz.getMethod("setAnswertext", String.class);

				method.invoke(_answerRemoteModel, answertext);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getRight() {
		return _right;
	}

	@Override
	public boolean isRight() {
		return _right;
	}

	@Override
	public void setRight(boolean right) {
		_right = right;

		if (_answerRemoteModel != null) {
			try {
				Class<?> clazz = _answerRemoteModel.getClass();

				Method method = clazz.getMethod("setRight", boolean.class);

				method.invoke(_answerRemoteModel, right);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getAnswerRemoteModel() {
		return _answerRemoteModel;
	}

	public void setAnswerRemoteModel(BaseModel<?> answerRemoteModel) {
		_answerRemoteModel = answerRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _answerRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_answerRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			AnswerLocalServiceUtil.addAnswer(this);
		}
		else {
			AnswerLocalServiceUtil.updateAnswer(this);
		}
	}

	@Override
	public Answer toEscapedModel() {
		return (Answer)ProxyUtil.newProxyInstance(Answer.class.getClassLoader(),
			new Class[] { Answer.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		AnswerClp clone = new AnswerClp();

		clone.setAnswerid(getAnswerid());
		clone.setAnswertext(getAnswertext());
		clone.setRight(getRight());

		return clone;
	}

	@Override
	public int compareTo(Answer answer) {
		long primaryKey = answer.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AnswerClp)) {
			return false;
		}

		AnswerClp answer = (AnswerClp)obj;

		long primaryKey = answer.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{answerid=");
		sb.append(getAnswerid());
		sb.append(", answertext=");
		sb.append(getAnswertext());
		sb.append(", right=");
		sb.append(getRight());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append("kahoot_source.model.Answer");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>answerid</column-name><column-value><![CDATA[");
		sb.append(getAnswerid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>answertext</column-name><column-value><![CDATA[");
		sb.append(getAnswertext());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>right</column-name><column-value><![CDATA[");
		sb.append(getRight());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _answerid;
	private String _answertext;
	private boolean _right;
	private BaseModel<?> _answerRemoteModel;
	private Class<?> _clpSerializerClass = kahoot_source.service.ClpSerializer.class;
}