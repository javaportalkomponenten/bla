/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Answer}.
 * </p>
 *
 * @author krzysiek
 * @see Answer
 * @generated
 */
public class AnswerWrapper implements Answer, ModelWrapper<Answer> {
	public AnswerWrapper(Answer answer) {
		_answer = answer;
	}

	@Override
	public Class<?> getModelClass() {
		return Answer.class;
	}

	@Override
	public String getModelClassName() {
		return Answer.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("answerid", getAnswerid());
		attributes.put("answertext", getAnswertext());
		attributes.put("right", getRight());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long answerid = (Long)attributes.get("answerid");

		if (answerid != null) {
			setAnswerid(answerid);
		}

		String answertext = (String)attributes.get("answertext");

		if (answertext != null) {
			setAnswertext(answertext);
		}

		Boolean right = (Boolean)attributes.get("right");

		if (right != null) {
			setRight(right);
		}
	}

	/**
	* Returns the primary key of this answer.
	*
	* @return the primary key of this answer
	*/
	@Override
	public long getPrimaryKey() {
		return _answer.getPrimaryKey();
	}

	/**
	* Sets the primary key of this answer.
	*
	* @param primaryKey the primary key of this answer
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_answer.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the answerid of this answer.
	*
	* @return the answerid of this answer
	*/
	@Override
	public long getAnswerid() {
		return _answer.getAnswerid();
	}

	/**
	* Sets the answerid of this answer.
	*
	* @param answerid the answerid of this answer
	*/
	@Override
	public void setAnswerid(long answerid) {
		_answer.setAnswerid(answerid);
	}

	/**
	* Returns the answertext of this answer.
	*
	* @return the answertext of this answer
	*/
	@Override
	public java.lang.String getAnswertext() {
		return _answer.getAnswertext();
	}

	/**
	* Sets the answertext of this answer.
	*
	* @param answertext the answertext of this answer
	*/
	@Override
	public void setAnswertext(java.lang.String answertext) {
		_answer.setAnswertext(answertext);
	}

	/**
	* Returns the right of this answer.
	*
	* @return the right of this answer
	*/
	@Override
	public boolean getRight() {
		return _answer.getRight();
	}

	/**
	* Returns <code>true</code> if this answer is right.
	*
	* @return <code>true</code> if this answer is right; <code>false</code> otherwise
	*/
	@Override
	public boolean isRight() {
		return _answer.isRight();
	}

	/**
	* Sets whether this answer is right.
	*
	* @param right the right of this answer
	*/
	@Override
	public void setRight(boolean right) {
		_answer.setRight(right);
	}

	@Override
	public boolean isNew() {
		return _answer.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_answer.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _answer.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_answer.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _answer.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _answer.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_answer.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _answer.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_answer.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_answer.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_answer.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new AnswerWrapper((Answer)_answer.clone());
	}

	@Override
	public int compareTo(kahoot_source.model.Answer answer) {
		return _answer.compareTo(answer);
	}

	@Override
	public int hashCode() {
		return _answer.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<kahoot_source.model.Answer> toCacheModel() {
		return _answer.toCacheModel();
	}

	@Override
	public kahoot_source.model.Answer toEscapedModel() {
		return new AnswerWrapper(_answer.toEscapedModel());
	}

	@Override
	public kahoot_source.model.Answer toUnescapedModel() {
		return new AnswerWrapper(_answer.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _answer.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _answer.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_answer.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AnswerWrapper)) {
			return false;
		}

		AnswerWrapper answerWrapper = (AnswerWrapper)obj;

		if (Validator.equals(_answer, answerWrapper._answer)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Answer getWrappedAnswer() {
		return _answer;
	}

	@Override
	public Answer getWrappedModel() {
		return _answer;
	}

	@Override
	public void resetOriginalValues() {
		_answer.resetOriginalValues();
	}

	private Answer _answer;
}