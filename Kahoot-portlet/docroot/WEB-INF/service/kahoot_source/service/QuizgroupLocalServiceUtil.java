/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Quizgroup. This utility wraps
 * {@link kahoot_source.service.impl.QuizgroupLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author krzysiek
 * @see QuizgroupLocalService
 * @see kahoot_source.service.base.QuizgroupLocalServiceBaseImpl
 * @see kahoot_source.service.impl.QuizgroupLocalServiceImpl
 * @generated
 */
public class QuizgroupLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link kahoot_source.service.impl.QuizgroupLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the quizgroup to the database. Also notifies the appropriate model listeners.
	*
	* @param quizgroup the quizgroup
	* @return the quizgroup that was added
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quizgroup addQuizgroup(
		kahoot_source.model.Quizgroup quizgroup)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addQuizgroup(quizgroup);
	}

	/**
	* Creates a new quizgroup with the primary key. Does not add the quizgroup to the database.
	*
	* @param quizgroupid the primary key for the new quizgroup
	* @return the new quizgroup
	*/
	public static kahoot_source.model.Quizgroup createQuizgroup(
		long quizgroupid) {
		return getService().createQuizgroup(quizgroupid);
	}

	/**
	* Deletes the quizgroup with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param quizgroupid the primary key of the quizgroup
	* @return the quizgroup that was removed
	* @throws PortalException if a quizgroup with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quizgroup deleteQuizgroup(
		long quizgroupid)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteQuizgroup(quizgroupid);
	}

	/**
	* Deletes the quizgroup from the database. Also notifies the appropriate model listeners.
	*
	* @param quizgroup the quizgroup
	* @return the quizgroup that was removed
	* @throws PortalException
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quizgroup deleteQuizgroup(
		kahoot_source.model.Quizgroup quizgroup)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteQuizgroup(quizgroup);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static kahoot_source.model.Quizgroup fetchQuizgroup(long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchQuizgroup(quizgroupid);
	}

	/**
	* Returns the quizgroup with the primary key.
	*
	* @param quizgroupid the primary key of the quizgroup
	* @return the quizgroup
	* @throws PortalException if a quizgroup with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quizgroup getQuizgroup(long quizgroupid)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getQuizgroup(quizgroupid);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the quizgroups.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of quizgroups
	* @param end the upper bound of the range of quizgroups (not inclusive)
	* @return the range of quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quizgroup> getQuizgroups(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getQuizgroups(start, end);
	}

	/**
	* Returns the number of quizgroups.
	*
	* @return the number of quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public static int getQuizgroupsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getQuizgroupsCount();
	}

	/**
	* Updates the quizgroup in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param quizgroup the quizgroup
	* @return the quizgroup that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quizgroup updateQuizgroup(
		kahoot_source.model.Quizgroup quizgroup)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateQuizgroup(quizgroup);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuizQuizgroup(long quizid, long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().addQuizQuizgroup(quizid, quizgroupid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuizQuizgroup(long quizid,
		kahoot_source.model.Quizgroup quizgroup)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().addQuizQuizgroup(quizid, quizgroup);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuizQuizgroups(long quizid, long[] quizgroupids)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().addQuizQuizgroups(quizid, quizgroupids);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuizQuizgroups(long quizid,
		java.util.List<kahoot_source.model.Quizgroup> Quizgroups)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().addQuizQuizgroups(quizid, Quizgroups);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void clearQuizQuizgroups(long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().clearQuizQuizgroups(quizid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void deleteQuizQuizgroup(long quizid, long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deleteQuizQuizgroup(quizid, quizgroupid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void deleteQuizQuizgroup(long quizid,
		kahoot_source.model.Quizgroup quizgroup)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deleteQuizQuizgroup(quizid, quizgroup);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void deleteQuizQuizgroups(long quizid, long[] quizgroupids)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deleteQuizQuizgroups(quizid, quizgroupids);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void deleteQuizQuizgroups(long quizid,
		java.util.List<kahoot_source.model.Quizgroup> Quizgroups)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deleteQuizQuizgroups(quizid, Quizgroups);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quizgroup> getQuizQuizgroups(
		long quizid) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getQuizQuizgroups(quizid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quizgroup> getQuizQuizgroups(
		long quizid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getQuizQuizgroups(quizid, start, end);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quizgroup> getQuizQuizgroups(
		long quizid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .getQuizQuizgroups(quizid, start, end, orderByComparator);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static int getQuizQuizgroupsCount(long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getQuizQuizgroupsCount(quizid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static boolean hasQuizQuizgroup(long quizid, long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().hasQuizQuizgroup(quizid, quizgroupid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static boolean hasQuizQuizgroups(long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().hasQuizQuizgroups(quizid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void setQuizQuizgroups(long quizid, long[] quizgroupids)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().setQuizQuizgroups(quizid, quizgroupids);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static kahoot_source.model.Quizgroup addQuizgroup(
		java.lang.String name, java.util.List<kahoot_source.model.Quiz> quizes)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addQuizgroup(name, quizes);
	}

	public static kahoot_source.model.Quizgroup updateQuizgroup(
		java.lang.Long quizgroupId, java.lang.String name,
		java.util.List<kahoot_source.model.Quiz> quizes)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateQuizgroup(quizgroupId, name, quizes);
	}

	public static kahoot_source.model.Quizgroup deleteQuizgroup(
		java.lang.Long quizgroupId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteQuizgroup(quizgroupId);
	}

	public static void clearService() {
		_service = null;
	}

	public static QuizgroupLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					QuizgroupLocalService.class.getName());

			if (invokableLocalService instanceof QuizgroupLocalService) {
				_service = (QuizgroupLocalService)invokableLocalService;
			}
			else {
				_service = new QuizgroupLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(QuizgroupLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(QuizgroupLocalService service) {
	}

	private static QuizgroupLocalService _service;
}