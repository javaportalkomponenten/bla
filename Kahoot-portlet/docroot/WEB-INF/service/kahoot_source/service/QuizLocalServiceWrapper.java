/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link QuizLocalService}.
 *
 * @author krzysiek
 * @see QuizLocalService
 * @generated
 */
public class QuizLocalServiceWrapper implements QuizLocalService,
	ServiceWrapper<QuizLocalService> {
	public QuizLocalServiceWrapper(QuizLocalService quizLocalService) {
		_quizLocalService = quizLocalService;
	}

	/**
	* Adds the quiz to the database. Also notifies the appropriate model listeners.
	*
	* @param quiz the quiz
	* @return the quiz that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public kahoot_source.model.Quiz addQuiz(kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.addQuiz(quiz);
	}

	/**
	* Creates a new quiz with the primary key. Does not add the quiz to the database.
	*
	* @param quizid the primary key for the new quiz
	* @return the new quiz
	*/
	@Override
	public kahoot_source.model.Quiz createQuiz(long quizid) {
		return _quizLocalService.createQuiz(quizid);
	}

	/**
	* Deletes the quiz with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param quizid the primary key of the quiz
	* @return the quiz that was removed
	* @throws PortalException if a quiz with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public kahoot_source.model.Quiz deleteQuiz(long quizid)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.deleteQuiz(quizid);
	}

	/**
	* Deletes the quiz from the database. Also notifies the appropriate model listeners.
	*
	* @param quiz the quiz
	* @return the quiz that was removed
	* @throws PortalException
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public kahoot_source.model.Quiz deleteQuiz(kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.deleteQuiz(quiz);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _quizLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public kahoot_source.model.Quiz fetchQuiz(long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.fetchQuiz(quizid);
	}

	/**
	* Returns the quiz with the primary key.
	*
	* @param quizid the primary key of the quiz
	* @return the quiz
	* @throws PortalException if a quiz with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public kahoot_source.model.Quiz getQuiz(long quizid)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.getQuiz(quizid);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the quizs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @return the range of quizs
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<kahoot_source.model.Quiz> getQuizs(int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.getQuizs(start, end);
	}

	/**
	* Returns the number of quizs.
	*
	* @return the number of quizs
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getQuizsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.getQuizsCount();
	}

	/**
	* Updates the quiz in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param quiz the quiz
	* @return the quiz that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public kahoot_source.model.Quiz updateQuiz(kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.updateQuiz(quiz);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void addQuizgroupQuiz(long quizgroupid, long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizLocalService.addQuizgroupQuiz(quizgroupid, quizid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void addQuizgroupQuiz(long quizgroupid, kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizLocalService.addQuizgroupQuiz(quizgroupid, quiz);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void addQuizgroupQuizs(long quizgroupid, long[] quizids)
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizLocalService.addQuizgroupQuizs(quizgroupid, quizids);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void addQuizgroupQuizs(long quizgroupid,
		java.util.List<kahoot_source.model.Quiz> Quizs)
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizLocalService.addQuizgroupQuizs(quizgroupid, Quizs);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void clearQuizgroupQuizs(long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizLocalService.clearQuizgroupQuizs(quizgroupid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void deleteQuizgroupQuiz(long quizgroupid, long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizLocalService.deleteQuizgroupQuiz(quizgroupid, quizid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void deleteQuizgroupQuiz(long quizgroupid,
		kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizLocalService.deleteQuizgroupQuiz(quizgroupid, quiz);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void deleteQuizgroupQuizs(long quizgroupid, long[] quizids)
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizLocalService.deleteQuizgroupQuizs(quizgroupid, quizids);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void deleteQuizgroupQuizs(long quizgroupid,
		java.util.List<kahoot_source.model.Quiz> Quizs)
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizLocalService.deleteQuizgroupQuizs(quizgroupid, Quizs);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<kahoot_source.model.Quiz> getQuizgroupQuizs(
		long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.getQuizgroupQuizs(quizgroupid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<kahoot_source.model.Quiz> getQuizgroupQuizs(
		long quizgroupid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.getQuizgroupQuizs(quizgroupid, start, end);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<kahoot_source.model.Quiz> getQuizgroupQuizs(
		long quizgroupid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.getQuizgroupQuizs(quizgroupid, start, end,
			orderByComparator);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getQuizgroupQuizsCount(long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.getQuizgroupQuizsCount(quizgroupid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public boolean hasQuizgroupQuiz(long quizgroupid, long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.hasQuizgroupQuiz(quizgroupid, quizid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public boolean hasQuizgroupQuizs(long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.hasQuizgroupQuizs(quizgroupid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void setQuizgroupQuizs(long quizgroupid, long[] quizids)
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizLocalService.setQuizgroupQuizs(quizgroupid, quizids);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void addQuestionQuiz(long questionid, long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizLocalService.addQuestionQuiz(questionid, quizid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void addQuestionQuiz(long questionid, kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizLocalService.addQuestionQuiz(questionid, quiz);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void addQuestionQuizs(long questionid, long[] quizids)
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizLocalService.addQuestionQuizs(questionid, quizids);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void addQuestionQuizs(long questionid,
		java.util.List<kahoot_source.model.Quiz> Quizs)
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizLocalService.addQuestionQuizs(questionid, Quizs);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void clearQuestionQuizs(long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizLocalService.clearQuestionQuizs(questionid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void deleteQuestionQuiz(long questionid, long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizLocalService.deleteQuestionQuiz(questionid, quizid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void deleteQuestionQuiz(long questionid,
		kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizLocalService.deleteQuestionQuiz(questionid, quiz);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void deleteQuestionQuizs(long questionid, long[] quizids)
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizLocalService.deleteQuestionQuizs(questionid, quizids);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void deleteQuestionQuizs(long questionid,
		java.util.List<kahoot_source.model.Quiz> Quizs)
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizLocalService.deleteQuestionQuizs(questionid, Quizs);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<kahoot_source.model.Quiz> getQuestionQuizs(
		long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.getQuestionQuizs(questionid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<kahoot_source.model.Quiz> getQuestionQuizs(
		long questionid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.getQuestionQuizs(questionid, start, end);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<kahoot_source.model.Quiz> getQuestionQuizs(
		long questionid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.getQuestionQuizs(questionid, start, end,
			orderByComparator);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getQuestionQuizsCount(long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.getQuestionQuizsCount(questionid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public boolean hasQuestionQuiz(long questionid, long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.hasQuestionQuiz(questionid, quizid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public boolean hasQuestionQuizs(long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.hasQuestionQuizs(questionid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void setQuestionQuizs(long questionid, long[] quizids)
		throws com.liferay.portal.kernel.exception.SystemException {
		_quizLocalService.setQuestionQuizs(questionid, quizids);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _quizLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_quizLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _quizLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	@Override
	public kahoot_source.model.Quiz addQuiz(java.lang.String title,
		java.lang.Long timestamp, boolean running,
		java.util.List<kahoot_source.model.Quizgroup> quizgroups,
		java.util.List<kahoot_source.model.Question> questions)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.addQuiz(title, timestamp, running, quizgroups,
			questions);
	}

	@Override
	public java.util.List<kahoot_source.model.Quiz> getByTitle(
		java.lang.String title)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.getByTitle(title);
	}

	@Override
	public java.util.List<kahoot_source.model.Quiz> getAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.getAll();
	}

	@Override
	public java.util.List<kahoot_source.model.Quiz> getByTitleLike(
		java.lang.String title) {
		return _quizLocalService.getByTitleLike(title);
	}

	@Override
	public java.util.List<kahoot_source.model.Quiz> getByTimestampBefore(
		java.lang.Long timestamp) {
		return _quizLocalService.getByTimestampBefore(timestamp);
	}

	@Override
	public java.util.List<kahoot_source.model.Quiz> getRunningQuizes()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.getRunningQuizes();
	}

	@Override
	public java.util.List<kahoot_source.model.Quiz> getNotRunningQuizes()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.getNotRunningQuizes();
	}

	@Override
	public kahoot_source.model.Quiz updateQuiz(java.lang.Long quizId,
		java.lang.String title, java.lang.Long timestamp,
		java.lang.Boolean running,
		java.util.List<kahoot_source.model.Quizgroup> quizgroups,
		java.util.List<kahoot_source.model.Question> questions)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.updateQuiz(quizId, title, timestamp, running,
			quizgroups, questions);
	}

	@Override
	public kahoot_source.model.Quiz deleteQuiz(java.lang.Long quizId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _quizLocalService.deleteQuiz(quizId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public QuizLocalService getWrappedQuizLocalService() {
		return _quizLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedQuizLocalService(QuizLocalService quizLocalService) {
		_quizLocalService = quizLocalService;
	}

	@Override
	public QuizLocalService getWrappedService() {
		return _quizLocalService;
	}

	@Override
	public void setWrappedService(QuizLocalService quizLocalService) {
		_quizLocalService = quizLocalService;
	}

	private QuizLocalService _quizLocalService;
}