/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Quiz. This utility wraps
 * {@link kahoot_source.service.impl.QuizLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author krzysiek
 * @see QuizLocalService
 * @see kahoot_source.service.base.QuizLocalServiceBaseImpl
 * @see kahoot_source.service.impl.QuizLocalServiceImpl
 * @generated
 */
public class QuizLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link kahoot_source.service.impl.QuizLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the quiz to the database. Also notifies the appropriate model listeners.
	*
	* @param quiz the quiz
	* @return the quiz that was added
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz addQuiz(
		kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addQuiz(quiz);
	}

	/**
	* Creates a new quiz with the primary key. Does not add the quiz to the database.
	*
	* @param quizid the primary key for the new quiz
	* @return the new quiz
	*/
	public static kahoot_source.model.Quiz createQuiz(long quizid) {
		return getService().createQuiz(quizid);
	}

	/**
	* Deletes the quiz with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param quizid the primary key of the quiz
	* @return the quiz that was removed
	* @throws PortalException if a quiz with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz deleteQuiz(long quizid)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteQuiz(quizid);
	}

	/**
	* Deletes the quiz from the database. Also notifies the appropriate model listeners.
	*
	* @param quiz the quiz
	* @return the quiz that was removed
	* @throws PortalException
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz deleteQuiz(
		kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteQuiz(quiz);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static kahoot_source.model.Quiz fetchQuiz(long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchQuiz(quizid);
	}

	/**
	* Returns the quiz with the primary key.
	*
	* @param quizid the primary key of the quiz
	* @return the quiz
	* @throws PortalException if a quiz with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz getQuiz(long quizid)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getQuiz(quizid);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the quizs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @return the range of quizs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> getQuizs(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getQuizs(start, end);
	}

	/**
	* Returns the number of quizs.
	*
	* @return the number of quizs
	* @throws SystemException if a system exception occurred
	*/
	public static int getQuizsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getQuizsCount();
	}

	/**
	* Updates the quiz in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param quiz the quiz
	* @return the quiz that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz updateQuiz(
		kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateQuiz(quiz);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuizgroupQuiz(long quizgroupid, long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().addQuizgroupQuiz(quizgroupid, quizid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuizgroupQuiz(long quizgroupid,
		kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().addQuizgroupQuiz(quizgroupid, quiz);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuizgroupQuizs(long quizgroupid, long[] quizids)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().addQuizgroupQuizs(quizgroupid, quizids);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuizgroupQuizs(long quizgroupid,
		java.util.List<kahoot_source.model.Quiz> Quizs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().addQuizgroupQuizs(quizgroupid, Quizs);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void clearQuizgroupQuizs(long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().clearQuizgroupQuizs(quizgroupid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void deleteQuizgroupQuiz(long quizgroupid, long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deleteQuizgroupQuiz(quizgroupid, quizid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void deleteQuizgroupQuiz(long quizgroupid,
		kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deleteQuizgroupQuiz(quizgroupid, quiz);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void deleteQuizgroupQuizs(long quizgroupid, long[] quizids)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deleteQuizgroupQuizs(quizgroupid, quizids);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void deleteQuizgroupQuizs(long quizgroupid,
		java.util.List<kahoot_source.model.Quiz> Quizs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deleteQuizgroupQuizs(quizgroupid, Quizs);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> getQuizgroupQuizs(
		long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getQuizgroupQuizs(quizgroupid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> getQuizgroupQuizs(
		long quizgroupid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getQuizgroupQuizs(quizgroupid, start, end);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> getQuizgroupQuizs(
		long quizgroupid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .getQuizgroupQuizs(quizgroupid, start, end, orderByComparator);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static int getQuizgroupQuizsCount(long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getQuizgroupQuizsCount(quizgroupid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static boolean hasQuizgroupQuiz(long quizgroupid, long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().hasQuizgroupQuiz(quizgroupid, quizid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static boolean hasQuizgroupQuizs(long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().hasQuizgroupQuizs(quizgroupid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void setQuizgroupQuizs(long quizgroupid, long[] quizids)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().setQuizgroupQuizs(quizgroupid, quizids);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuestionQuiz(long questionid, long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().addQuestionQuiz(questionid, quizid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuestionQuiz(long questionid,
		kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().addQuestionQuiz(questionid, quiz);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuestionQuizs(long questionid, long[] quizids)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().addQuestionQuizs(questionid, quizids);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuestionQuizs(long questionid,
		java.util.List<kahoot_source.model.Quiz> Quizs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().addQuestionQuizs(questionid, Quizs);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void clearQuestionQuizs(long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().clearQuestionQuizs(questionid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void deleteQuestionQuiz(long questionid, long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deleteQuestionQuiz(questionid, quizid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void deleteQuestionQuiz(long questionid,
		kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deleteQuestionQuiz(questionid, quiz);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void deleteQuestionQuizs(long questionid, long[] quizids)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deleteQuestionQuizs(questionid, quizids);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void deleteQuestionQuizs(long questionid,
		java.util.List<kahoot_source.model.Quiz> Quizs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deleteQuestionQuizs(questionid, Quizs);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> getQuestionQuizs(
		long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getQuestionQuizs(questionid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> getQuestionQuizs(
		long questionid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getQuestionQuizs(questionid, start, end);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> getQuestionQuizs(
		long questionid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .getQuestionQuizs(questionid, start, end, orderByComparator);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static int getQuestionQuizsCount(long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getQuestionQuizsCount(questionid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static boolean hasQuestionQuiz(long questionid, long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().hasQuestionQuiz(questionid, quizid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static boolean hasQuestionQuizs(long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().hasQuestionQuizs(questionid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void setQuestionQuizs(long questionid, long[] quizids)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().setQuestionQuizs(questionid, quizids);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static kahoot_source.model.Quiz addQuiz(java.lang.String title,
		java.lang.Long timestamp, boolean running,
		java.util.List<kahoot_source.model.Quizgroup> quizgroups,
		java.util.List<kahoot_source.model.Question> questions)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .addQuiz(title, timestamp, running, quizgroups, questions);
	}

	public static java.util.List<kahoot_source.model.Quiz> getByTitle(
		java.lang.String title)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getByTitle(title);
	}

	public static java.util.List<kahoot_source.model.Quiz> getAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getAll();
	}

	public static java.util.List<kahoot_source.model.Quiz> getByTitleLike(
		java.lang.String title) {
		return getService().getByTitleLike(title);
	}

	public static java.util.List<kahoot_source.model.Quiz> getByTimestampBefore(
		java.lang.Long timestamp) {
		return getService().getByTimestampBefore(timestamp);
	}

	public static java.util.List<kahoot_source.model.Quiz> getRunningQuizes()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getRunningQuizes();
	}

	public static java.util.List<kahoot_source.model.Quiz> getNotRunningQuizes()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getNotRunningQuizes();
	}

	public static kahoot_source.model.Quiz updateQuiz(java.lang.Long quizId,
		java.lang.String title, java.lang.Long timestamp,
		java.lang.Boolean running,
		java.util.List<kahoot_source.model.Quizgroup> quizgroups,
		java.util.List<kahoot_source.model.Question> questions)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .updateQuiz(quizId, title, timestamp, running, quizgroups,
			questions);
	}

	public static kahoot_source.model.Quiz deleteQuiz(java.lang.Long quizId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteQuiz(quizId);
	}

	public static void clearService() {
		_service = null;
	}

	public static QuizLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					QuizLocalService.class.getName());

			if (invokableLocalService instanceof QuizLocalService) {
				_service = (QuizLocalService)invokableLocalService;
			}
			else {
				_service = new QuizLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(QuizLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(QuizLocalService service) {
	}

	private static QuizLocalService _service;
}