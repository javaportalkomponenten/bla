/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AnswerLocalService}.
 *
 * @author krzysiek
 * @see AnswerLocalService
 * @generated
 */
public class AnswerLocalServiceWrapper implements AnswerLocalService,
	ServiceWrapper<AnswerLocalService> {
	public AnswerLocalServiceWrapper(AnswerLocalService answerLocalService) {
		_answerLocalService = answerLocalService;
	}

	/**
	* Adds the answer to the database. Also notifies the appropriate model listeners.
	*
	* @param answer the answer
	* @return the answer that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public kahoot_source.model.Answer addAnswer(
		kahoot_source.model.Answer answer)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.addAnswer(answer);
	}

	/**
	* Creates a new answer with the primary key. Does not add the answer to the database.
	*
	* @param answerid the primary key for the new answer
	* @return the new answer
	*/
	@Override
	public kahoot_source.model.Answer createAnswer(long answerid) {
		return _answerLocalService.createAnswer(answerid);
	}

	/**
	* Deletes the answer with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param answerid the primary key of the answer
	* @return the answer that was removed
	* @throws PortalException if a answer with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public kahoot_source.model.Answer deleteAnswer(long answerid)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.deleteAnswer(answerid);
	}

	/**
	* Deletes the answer from the database. Also notifies the appropriate model listeners.
	*
	* @param answer the answer
	* @return the answer that was removed
	* @throws PortalException
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public kahoot_source.model.Answer deleteAnswer(
		kahoot_source.model.Answer answer)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.deleteAnswer(answer);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _answerLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public kahoot_source.model.Answer fetchAnswer(long answerid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.fetchAnswer(answerid);
	}

	/**
	* Returns the answer with the primary key.
	*
	* @param answerid the primary key of the answer
	* @return the answer
	* @throws PortalException if a answer with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public kahoot_source.model.Answer getAnswer(long answerid)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.getAnswer(answerid);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the answers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of answers
	* @param end the upper bound of the range of answers (not inclusive)
	* @return the range of answers
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<kahoot_source.model.Answer> getAnswers(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.getAnswers(start, end);
	}

	/**
	* Returns the number of answers.
	*
	* @return the number of answers
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getAnswersCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.getAnswersCount();
	}

	/**
	* Updates the answer in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param answer the answer
	* @return the answer that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public kahoot_source.model.Answer updateAnswer(
		kahoot_source.model.Answer answer)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.updateAnswer(answer);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void addQuestionAnswer(long questionid, long answerid)
		throws com.liferay.portal.kernel.exception.SystemException {
		_answerLocalService.addQuestionAnswer(questionid, answerid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void addQuestionAnswer(long questionid,
		kahoot_source.model.Answer answer)
		throws com.liferay.portal.kernel.exception.SystemException {
		_answerLocalService.addQuestionAnswer(questionid, answer);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void addQuestionAnswers(long questionid, long[] answerids)
		throws com.liferay.portal.kernel.exception.SystemException {
		_answerLocalService.addQuestionAnswers(questionid, answerids);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void addQuestionAnswers(long questionid,
		java.util.List<kahoot_source.model.Answer> Answers)
		throws com.liferay.portal.kernel.exception.SystemException {
		_answerLocalService.addQuestionAnswers(questionid, Answers);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void clearQuestionAnswers(long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		_answerLocalService.clearQuestionAnswers(questionid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void deleteQuestionAnswer(long questionid, long answerid)
		throws com.liferay.portal.kernel.exception.SystemException {
		_answerLocalService.deleteQuestionAnswer(questionid, answerid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void deleteQuestionAnswer(long questionid,
		kahoot_source.model.Answer answer)
		throws com.liferay.portal.kernel.exception.SystemException {
		_answerLocalService.deleteQuestionAnswer(questionid, answer);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void deleteQuestionAnswers(long questionid, long[] answerids)
		throws com.liferay.portal.kernel.exception.SystemException {
		_answerLocalService.deleteQuestionAnswers(questionid, answerids);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void deleteQuestionAnswers(long questionid,
		java.util.List<kahoot_source.model.Answer> Answers)
		throws com.liferay.portal.kernel.exception.SystemException {
		_answerLocalService.deleteQuestionAnswers(questionid, Answers);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<kahoot_source.model.Answer> getQuestionAnswers(
		long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.getQuestionAnswers(questionid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<kahoot_source.model.Answer> getQuestionAnswers(
		long questionid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.getQuestionAnswers(questionid, start, end);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<kahoot_source.model.Answer> getQuestionAnswers(
		long questionid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.getQuestionAnswers(questionid, start, end,
			orderByComparator);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getQuestionAnswersCount(long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.getQuestionAnswersCount(questionid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public boolean hasQuestionAnswer(long questionid, long answerid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.hasQuestionAnswer(questionid, answerid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public boolean hasQuestionAnswers(long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.hasQuestionAnswers(questionid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void setQuestionAnswers(long questionid, long[] answerids)
		throws com.liferay.portal.kernel.exception.SystemException {
		_answerLocalService.setQuestionAnswers(questionid, answerids);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _answerLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_answerLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _answerLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	@Override
	public kahoot_source.model.Answer addAnswer(java.lang.String answerText,
		boolean isRight)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.addAnswer(answerText, isRight);
	}

	@Override
	public kahoot_source.model.Answer updateAnswer(java.lang.Long answerId,
		java.lang.String answerText, java.lang.Boolean isRight)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.updateAnswer(answerId, answerText, isRight);
	}

	@Override
	public kahoot_source.model.Answer deleteAnswer(java.lang.Long answerId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _answerLocalService.deleteAnswer(answerId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public AnswerLocalService getWrappedAnswerLocalService() {
		return _answerLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedAnswerLocalService(
		AnswerLocalService answerLocalService) {
		_answerLocalService = answerLocalService;
	}

	@Override
	public AnswerLocalService getWrappedService() {
		return _answerLocalService;
	}

	@Override
	public void setWrappedService(AnswerLocalService answerLocalService) {
		_answerLocalService = answerLocalService;
	}

	private AnswerLocalService _answerLocalService;
}