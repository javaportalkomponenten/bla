/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import kahoot_source.model.Quizgroup;

import java.util.List;

/**
 * The persistence utility for the quizgroup service. This utility wraps {@link QuizgroupPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author krzysiek
 * @see QuizgroupPersistence
 * @see QuizgroupPersistenceImpl
 * @generated
 */
public class QuizgroupUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Quizgroup quizgroup) {
		getPersistence().clearCache(quizgroup);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Quizgroup> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Quizgroup> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Quizgroup> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static Quizgroup update(Quizgroup quizgroup)
		throws SystemException {
		return getPersistence().update(quizgroup);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static Quizgroup update(Quizgroup quizgroup,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(quizgroup, serviceContext);
	}

	/**
	* Returns all the quizgroups where quizgroupid = &#63;.
	*
	* @param quizgroupid the quizgroupid
	* @return the matching quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quizgroup> findByquizgroupid(
		long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByquizgroupid(quizgroupid);
	}

	/**
	* Returns a range of all the quizgroups where quizgroupid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param quizgroupid the quizgroupid
	* @param start the lower bound of the range of quizgroups
	* @param end the upper bound of the range of quizgroups (not inclusive)
	* @return the range of matching quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quizgroup> findByquizgroupid(
		long quizgroupid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByquizgroupid(quizgroupid, start, end);
	}

	/**
	* Returns an ordered range of all the quizgroups where quizgroupid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param quizgroupid the quizgroupid
	* @param start the lower bound of the range of quizgroups
	* @param end the upper bound of the range of quizgroups (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quizgroup> findByquizgroupid(
		long quizgroupid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByquizgroupid(quizgroupid, start, end, orderByComparator);
	}

	/**
	* Returns the first quizgroup in the ordered set where quizgroupid = &#63;.
	*
	* @param quizgroupid the quizgroupid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching quizgroup
	* @throws kahoot_source.NoSuchQuizgroupException if a matching quizgroup could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quizgroup findByquizgroupid_First(
		long quizgroupid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizgroupException {
		return getPersistence()
				   .findByquizgroupid_First(quizgroupid, orderByComparator);
	}

	/**
	* Returns the first quizgroup in the ordered set where quizgroupid = &#63;.
	*
	* @param quizgroupid the quizgroupid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching quizgroup, or <code>null</code> if a matching quizgroup could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quizgroup fetchByquizgroupid_First(
		long quizgroupid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByquizgroupid_First(quizgroupid, orderByComparator);
	}

	/**
	* Returns the last quizgroup in the ordered set where quizgroupid = &#63;.
	*
	* @param quizgroupid the quizgroupid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching quizgroup
	* @throws kahoot_source.NoSuchQuizgroupException if a matching quizgroup could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quizgroup findByquizgroupid_Last(
		long quizgroupid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizgroupException {
		return getPersistence()
				   .findByquizgroupid_Last(quizgroupid, orderByComparator);
	}

	/**
	* Returns the last quizgroup in the ordered set where quizgroupid = &#63;.
	*
	* @param quizgroupid the quizgroupid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching quizgroup, or <code>null</code> if a matching quizgroup could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quizgroup fetchByquizgroupid_Last(
		long quizgroupid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByquizgroupid_Last(quizgroupid, orderByComparator);
	}

	/**
	* Removes all the quizgroups where quizgroupid = &#63; from the database.
	*
	* @param quizgroupid the quizgroupid
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByquizgroupid(long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByquizgroupid(quizgroupid);
	}

	/**
	* Returns the number of quizgroups where quizgroupid = &#63;.
	*
	* @param quizgroupid the quizgroupid
	* @return the number of matching quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public static int countByquizgroupid(long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByquizgroupid(quizgroupid);
	}

	/**
	* Returns all the quizgroups where name = &#63;.
	*
	* @param name the name
	* @return the matching quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quizgroup> findByname(
		java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByname(name);
	}

	/**
	* Returns a range of all the quizgroups where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param name the name
	* @param start the lower bound of the range of quizgroups
	* @param end the upper bound of the range of quizgroups (not inclusive)
	* @return the range of matching quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quizgroup> findByname(
		java.lang.String name, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByname(name, start, end);
	}

	/**
	* Returns an ordered range of all the quizgroups where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param name the name
	* @param start the lower bound of the range of quizgroups
	* @param end the upper bound of the range of quizgroups (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quizgroup> findByname(
		java.lang.String name, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByname(name, start, end, orderByComparator);
	}

	/**
	* Returns the first quizgroup in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching quizgroup
	* @throws kahoot_source.NoSuchQuizgroupException if a matching quizgroup could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quizgroup findByname_First(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizgroupException {
		return getPersistence().findByname_First(name, orderByComparator);
	}

	/**
	* Returns the first quizgroup in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching quizgroup, or <code>null</code> if a matching quizgroup could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quizgroup fetchByname_First(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByname_First(name, orderByComparator);
	}

	/**
	* Returns the last quizgroup in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching quizgroup
	* @throws kahoot_source.NoSuchQuizgroupException if a matching quizgroup could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quizgroup findByname_Last(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizgroupException {
		return getPersistence().findByname_Last(name, orderByComparator);
	}

	/**
	* Returns the last quizgroup in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching quizgroup, or <code>null</code> if a matching quizgroup could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quizgroup fetchByname_Last(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByname_Last(name, orderByComparator);
	}

	/**
	* Returns the quizgroups before and after the current quizgroup in the ordered set where name = &#63;.
	*
	* @param quizgroupid the primary key of the current quizgroup
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next quizgroup
	* @throws kahoot_source.NoSuchQuizgroupException if a quizgroup with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quizgroup[] findByname_PrevAndNext(
		long quizgroupid, java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizgroupException {
		return getPersistence()
				   .findByname_PrevAndNext(quizgroupid, name, orderByComparator);
	}

	/**
	* Removes all the quizgroups where name = &#63; from the database.
	*
	* @param name the name
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByname(java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByname(name);
	}

	/**
	* Returns the number of quizgroups where name = &#63;.
	*
	* @param name the name
	* @return the number of matching quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public static int countByname(java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByname(name);
	}

	/**
	* Caches the quizgroup in the entity cache if it is enabled.
	*
	* @param quizgroup the quizgroup
	*/
	public static void cacheResult(kahoot_source.model.Quizgroup quizgroup) {
		getPersistence().cacheResult(quizgroup);
	}

	/**
	* Caches the quizgroups in the entity cache if it is enabled.
	*
	* @param quizgroups the quizgroups
	*/
	public static void cacheResult(
		java.util.List<kahoot_source.model.Quizgroup> quizgroups) {
		getPersistence().cacheResult(quizgroups);
	}

	/**
	* Creates a new quizgroup with the primary key. Does not add the quizgroup to the database.
	*
	* @param quizgroupid the primary key for the new quizgroup
	* @return the new quizgroup
	*/
	public static kahoot_source.model.Quizgroup create(long quizgroupid) {
		return getPersistence().create(quizgroupid);
	}

	/**
	* Removes the quizgroup with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param quizgroupid the primary key of the quizgroup
	* @return the quizgroup that was removed
	* @throws kahoot_source.NoSuchQuizgroupException if a quizgroup with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quizgroup remove(long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizgroupException {
		return getPersistence().remove(quizgroupid);
	}

	public static kahoot_source.model.Quizgroup updateImpl(
		kahoot_source.model.Quizgroup quizgroup)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(quizgroup);
	}

	/**
	* Returns the quizgroup with the primary key or throws a {@link kahoot_source.NoSuchQuizgroupException} if it could not be found.
	*
	* @param quizgroupid the primary key of the quizgroup
	* @return the quizgroup
	* @throws kahoot_source.NoSuchQuizgroupException if a quizgroup with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quizgroup findByPrimaryKey(
		long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizgroupException {
		return getPersistence().findByPrimaryKey(quizgroupid);
	}

	/**
	* Returns the quizgroup with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param quizgroupid the primary key of the quizgroup
	* @return the quizgroup, or <code>null</code> if a quizgroup with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quizgroup fetchByPrimaryKey(
		long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(quizgroupid);
	}

	/**
	* Returns all the quizgroups.
	*
	* @return the quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quizgroup> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the quizgroups.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of quizgroups
	* @param end the upper bound of the range of quizgroups (not inclusive)
	* @return the range of quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quizgroup> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the quizgroups.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of quizgroups
	* @param end the upper bound of the range of quizgroups (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quizgroup> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the quizgroups from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of quizgroups.
	*
	* @return the number of quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	/**
	* Returns all the quizs associated with the quizgroup.
	*
	* @param pk the primary key of the quizgroup
	* @return the quizs associated with the quizgroup
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> getQuizs(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getQuizs(pk);
	}

	/**
	* Returns a range of all the quizs associated with the quizgroup.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the quizgroup
	* @param start the lower bound of the range of quizgroups
	* @param end the upper bound of the range of quizgroups (not inclusive)
	* @return the range of quizs associated with the quizgroup
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> getQuizs(long pk,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getQuizs(pk, start, end);
	}

	/**
	* Returns an ordered range of all the quizs associated with the quizgroup.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the quizgroup
	* @param start the lower bound of the range of quizgroups
	* @param end the upper bound of the range of quizgroups (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of quizs associated with the quizgroup
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> getQuizs(long pk,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getQuizs(pk, start, end, orderByComparator);
	}

	/**
	* Returns the number of quizs associated with the quizgroup.
	*
	* @param pk the primary key of the quizgroup
	* @return the number of quizs associated with the quizgroup
	* @throws SystemException if a system exception occurred
	*/
	public static int getQuizsSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getQuizsSize(pk);
	}

	/**
	* Returns <code>true</code> if the quiz is associated with the quizgroup.
	*
	* @param pk the primary key of the quizgroup
	* @param quizPK the primary key of the quiz
	* @return <code>true</code> if the quiz is associated with the quizgroup; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsQuiz(long pk, long quizPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsQuiz(pk, quizPK);
	}

	/**
	* Returns <code>true</code> if the quizgroup has any quizs associated with it.
	*
	* @param pk the primary key of the quizgroup to check for associations with quizs
	* @return <code>true</code> if the quizgroup has any quizs associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsQuizs(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsQuizs(pk);
	}

	/**
	* Adds an association between the quizgroup and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup
	* @param quizPK the primary key of the quiz
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuiz(long pk, long quizPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addQuiz(pk, quizPK);
	}

	/**
	* Adds an association between the quizgroup and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup
	* @param quiz the quiz
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuiz(long pk, kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addQuiz(pk, quiz);
	}

	/**
	* Adds an association between the quizgroup and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup
	* @param quizPKs the primary keys of the quizs
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuizs(long pk, long[] quizPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addQuizs(pk, quizPKs);
	}

	/**
	* Adds an association between the quizgroup and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup
	* @param quizs the quizs
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuizs(long pk,
		java.util.List<kahoot_source.model.Quiz> quizs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addQuizs(pk, quizs);
	}

	/**
	* Clears all associations between the quizgroup and its quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup to clear the associated quizs from
	* @throws SystemException if a system exception occurred
	*/
	public static void clearQuizs(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().clearQuizs(pk);
	}

	/**
	* Removes the association between the quizgroup and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup
	* @param quizPK the primary key of the quiz
	* @throws SystemException if a system exception occurred
	*/
	public static void removeQuiz(long pk, long quizPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeQuiz(pk, quizPK);
	}

	/**
	* Removes the association between the quizgroup and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup
	* @param quiz the quiz
	* @throws SystemException if a system exception occurred
	*/
	public static void removeQuiz(long pk, kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeQuiz(pk, quiz);
	}

	/**
	* Removes the association between the quizgroup and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup
	* @param quizPKs the primary keys of the quizs
	* @throws SystemException if a system exception occurred
	*/
	public static void removeQuizs(long pk, long[] quizPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeQuizs(pk, quizPKs);
	}

	/**
	* Removes the association between the quizgroup and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup
	* @param quizs the quizs
	* @throws SystemException if a system exception occurred
	*/
	public static void removeQuizs(long pk,
		java.util.List<kahoot_source.model.Quiz> quizs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeQuizs(pk, quizs);
	}

	/**
	* Sets the quizs associated with the quizgroup, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup
	* @param quizPKs the primary keys of the quizs to be associated with the quizgroup
	* @throws SystemException if a system exception occurred
	*/
	public static void setQuizs(long pk, long[] quizPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().setQuizs(pk, quizPKs);
	}

	/**
	* Sets the quizs associated with the quizgroup, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup
	* @param quizs the quizs to be associated with the quizgroup
	* @throws SystemException if a system exception occurred
	*/
	public static void setQuizs(long pk,
		java.util.List<kahoot_source.model.Quiz> quizs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().setQuizs(pk, quizs);
	}

	public static QuizgroupPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (QuizgroupPersistence)PortletBeanLocatorUtil.locate(kahoot_source.service.ClpSerializer.getServletContextName(),
					QuizgroupPersistence.class.getName());

			ReferenceRegistry.registerReference(QuizgroupUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(QuizgroupPersistence persistence) {
	}

	private static QuizgroupPersistence _persistence;
}