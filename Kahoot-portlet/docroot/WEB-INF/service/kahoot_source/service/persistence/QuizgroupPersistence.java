/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import kahoot_source.model.Quizgroup;

/**
 * The persistence interface for the quizgroup service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author krzysiek
 * @see QuizgroupPersistenceImpl
 * @see QuizgroupUtil
 * @generated
 */
public interface QuizgroupPersistence extends BasePersistence<Quizgroup> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link QuizgroupUtil} to access the quizgroup persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the quizgroups where quizgroupid = &#63;.
	*
	* @param quizgroupid the quizgroupid
	* @return the matching quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quizgroup> findByquizgroupid(
		long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the quizgroups where quizgroupid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param quizgroupid the quizgroupid
	* @param start the lower bound of the range of quizgroups
	* @param end the upper bound of the range of quizgroups (not inclusive)
	* @return the range of matching quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quizgroup> findByquizgroupid(
		long quizgroupid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the quizgroups where quizgroupid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param quizgroupid the quizgroupid
	* @param start the lower bound of the range of quizgroups
	* @param end the upper bound of the range of quizgroups (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quizgroup> findByquizgroupid(
		long quizgroupid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first quizgroup in the ordered set where quizgroupid = &#63;.
	*
	* @param quizgroupid the quizgroupid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching quizgroup
	* @throws kahoot_source.NoSuchQuizgroupException if a matching quizgroup could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quizgroup findByquizgroupid_First(
		long quizgroupid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizgroupException;

	/**
	* Returns the first quizgroup in the ordered set where quizgroupid = &#63;.
	*
	* @param quizgroupid the quizgroupid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching quizgroup, or <code>null</code> if a matching quizgroup could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quizgroup fetchByquizgroupid_First(
		long quizgroupid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last quizgroup in the ordered set where quizgroupid = &#63;.
	*
	* @param quizgroupid the quizgroupid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching quizgroup
	* @throws kahoot_source.NoSuchQuizgroupException if a matching quizgroup could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quizgroup findByquizgroupid_Last(
		long quizgroupid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizgroupException;

	/**
	* Returns the last quizgroup in the ordered set where quizgroupid = &#63;.
	*
	* @param quizgroupid the quizgroupid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching quizgroup, or <code>null</code> if a matching quizgroup could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quizgroup fetchByquizgroupid_Last(
		long quizgroupid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the quizgroups where quizgroupid = &#63; from the database.
	*
	* @param quizgroupid the quizgroupid
	* @throws SystemException if a system exception occurred
	*/
	public void removeByquizgroupid(long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of quizgroups where quizgroupid = &#63;.
	*
	* @param quizgroupid the quizgroupid
	* @return the number of matching quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public int countByquizgroupid(long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the quizgroups where name = &#63;.
	*
	* @param name the name
	* @return the matching quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quizgroup> findByname(
		java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the quizgroups where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param name the name
	* @param start the lower bound of the range of quizgroups
	* @param end the upper bound of the range of quizgroups (not inclusive)
	* @return the range of matching quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quizgroup> findByname(
		java.lang.String name, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the quizgroups where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param name the name
	* @param start the lower bound of the range of quizgroups
	* @param end the upper bound of the range of quizgroups (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quizgroup> findByname(
		java.lang.String name, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first quizgroup in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching quizgroup
	* @throws kahoot_source.NoSuchQuizgroupException if a matching quizgroup could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quizgroup findByname_First(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizgroupException;

	/**
	* Returns the first quizgroup in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching quizgroup, or <code>null</code> if a matching quizgroup could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quizgroup fetchByname_First(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last quizgroup in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching quizgroup
	* @throws kahoot_source.NoSuchQuizgroupException if a matching quizgroup could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quizgroup findByname_Last(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizgroupException;

	/**
	* Returns the last quizgroup in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching quizgroup, or <code>null</code> if a matching quizgroup could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quizgroup fetchByname_Last(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the quizgroups before and after the current quizgroup in the ordered set where name = &#63;.
	*
	* @param quizgroupid the primary key of the current quizgroup
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next quizgroup
	* @throws kahoot_source.NoSuchQuizgroupException if a quizgroup with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quizgroup[] findByname_PrevAndNext(
		long quizgroupid, java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizgroupException;

	/**
	* Removes all the quizgroups where name = &#63; from the database.
	*
	* @param name the name
	* @throws SystemException if a system exception occurred
	*/
	public void removeByname(java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of quizgroups where name = &#63;.
	*
	* @param name the name
	* @return the number of matching quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public int countByname(java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the quizgroup in the entity cache if it is enabled.
	*
	* @param quizgroup the quizgroup
	*/
	public void cacheResult(kahoot_source.model.Quizgroup quizgroup);

	/**
	* Caches the quizgroups in the entity cache if it is enabled.
	*
	* @param quizgroups the quizgroups
	*/
	public void cacheResult(
		java.util.List<kahoot_source.model.Quizgroup> quizgroups);

	/**
	* Creates a new quizgroup with the primary key. Does not add the quizgroup to the database.
	*
	* @param quizgroupid the primary key for the new quizgroup
	* @return the new quizgroup
	*/
	public kahoot_source.model.Quizgroup create(long quizgroupid);

	/**
	* Removes the quizgroup with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param quizgroupid the primary key of the quizgroup
	* @return the quizgroup that was removed
	* @throws kahoot_source.NoSuchQuizgroupException if a quizgroup with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quizgroup remove(long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizgroupException;

	public kahoot_source.model.Quizgroup updateImpl(
		kahoot_source.model.Quizgroup quizgroup)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the quizgroup with the primary key or throws a {@link kahoot_source.NoSuchQuizgroupException} if it could not be found.
	*
	* @param quizgroupid the primary key of the quizgroup
	* @return the quizgroup
	* @throws kahoot_source.NoSuchQuizgroupException if a quizgroup with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quizgroup findByPrimaryKey(long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizgroupException;

	/**
	* Returns the quizgroup with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param quizgroupid the primary key of the quizgroup
	* @return the quizgroup, or <code>null</code> if a quizgroup with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quizgroup fetchByPrimaryKey(long quizgroupid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the quizgroups.
	*
	* @return the quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quizgroup> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the quizgroups.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of quizgroups
	* @param end the upper bound of the range of quizgroups (not inclusive)
	* @return the range of quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quizgroup> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the quizgroups.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of quizgroups
	* @param end the upper bound of the range of quizgroups (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quizgroup> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the quizgroups from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of quizgroups.
	*
	* @return the number of quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the quizs associated with the quizgroup.
	*
	* @param pk the primary key of the quizgroup
	* @return the quizs associated with the quizgroup
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quiz> getQuizs(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the quizs associated with the quizgroup.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the quizgroup
	* @param start the lower bound of the range of quizgroups
	* @param end the upper bound of the range of quizgroups (not inclusive)
	* @return the range of quizs associated with the quizgroup
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quiz> getQuizs(long pk,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the quizs associated with the quizgroup.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the quizgroup
	* @param start the lower bound of the range of quizgroups
	* @param end the upper bound of the range of quizgroups (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of quizs associated with the quizgroup
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quiz> getQuizs(long pk,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of quizs associated with the quizgroup.
	*
	* @param pk the primary key of the quizgroup
	* @return the number of quizs associated with the quizgroup
	* @throws SystemException if a system exception occurred
	*/
	public int getQuizsSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the quiz is associated with the quizgroup.
	*
	* @param pk the primary key of the quizgroup
	* @param quizPK the primary key of the quiz
	* @return <code>true</code> if the quiz is associated with the quizgroup; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsQuiz(long pk, long quizPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the quizgroup has any quizs associated with it.
	*
	* @param pk the primary key of the quizgroup to check for associations with quizs
	* @return <code>true</code> if the quizgroup has any quizs associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsQuizs(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the quizgroup and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup
	* @param quizPK the primary key of the quiz
	* @throws SystemException if a system exception occurred
	*/
	public void addQuiz(long pk, long quizPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the quizgroup and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup
	* @param quiz the quiz
	* @throws SystemException if a system exception occurred
	*/
	public void addQuiz(long pk, kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the quizgroup and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup
	* @param quizPKs the primary keys of the quizs
	* @throws SystemException if a system exception occurred
	*/
	public void addQuizs(long pk, long[] quizPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the quizgroup and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup
	* @param quizs the quizs
	* @throws SystemException if a system exception occurred
	*/
	public void addQuizs(long pk, java.util.List<kahoot_source.model.Quiz> quizs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Clears all associations between the quizgroup and its quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup to clear the associated quizs from
	* @throws SystemException if a system exception occurred
	*/
	public void clearQuizs(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the quizgroup and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup
	* @param quizPK the primary key of the quiz
	* @throws SystemException if a system exception occurred
	*/
	public void removeQuiz(long pk, long quizPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the quizgroup and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup
	* @param quiz the quiz
	* @throws SystemException if a system exception occurred
	*/
	public void removeQuiz(long pk, kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the quizgroup and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup
	* @param quizPKs the primary keys of the quizs
	* @throws SystemException if a system exception occurred
	*/
	public void removeQuizs(long pk, long[] quizPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the quizgroup and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup
	* @param quizs the quizs
	* @throws SystemException if a system exception occurred
	*/
	public void removeQuizs(long pk,
		java.util.List<kahoot_source.model.Quiz> quizs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Sets the quizs associated with the quizgroup, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup
	* @param quizPKs the primary keys of the quizs to be associated with the quizgroup
	* @throws SystemException if a system exception occurred
	*/
	public void setQuizs(long pk, long[] quizPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Sets the quizs associated with the quizgroup, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quizgroup
	* @param quizs the quizs to be associated with the quizgroup
	* @throws SystemException if a system exception occurred
	*/
	public void setQuizs(long pk, java.util.List<kahoot_source.model.Quiz> quizs)
		throws com.liferay.portal.kernel.exception.SystemException;
}