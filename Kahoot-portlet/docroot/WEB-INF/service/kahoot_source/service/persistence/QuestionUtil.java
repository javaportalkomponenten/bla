/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import kahoot_source.model.Question;

import java.util.List;

/**
 * The persistence utility for the question service. This utility wraps {@link QuestionPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author krzysiek
 * @see QuestionPersistence
 * @see QuestionPersistenceImpl
 * @generated
 */
public class QuestionUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Question question) {
		getPersistence().clearCache(question);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Question> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Question> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Question> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static Question update(Question question) throws SystemException {
		return getPersistence().update(question);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static Question update(Question question,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(question, serviceContext);
	}

	/**
	* Returns all the questions where questionid = &#63;.
	*
	* @param questionid the questionid
	* @return the matching questions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Question> findByquestionid(
		long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByquestionid(questionid);
	}

	/**
	* Returns a range of all the questions where questionid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionid the questionid
	* @param start the lower bound of the range of questions
	* @param end the upper bound of the range of questions (not inclusive)
	* @return the range of matching questions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Question> findByquestionid(
		long questionid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByquestionid(questionid, start, end);
	}

	/**
	* Returns an ordered range of all the questions where questionid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionid the questionid
	* @param start the lower bound of the range of questions
	* @param end the upper bound of the range of questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching questions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Question> findByquestionid(
		long questionid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByquestionid(questionid, start, end, orderByComparator);
	}

	/**
	* Returns the first question in the ordered set where questionid = &#63;.
	*
	* @param questionid the questionid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching question
	* @throws kahoot_source.NoSuchQuestionException if a matching question could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Question findByquestionid_First(
		long questionid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuestionException {
		return getPersistence()
				   .findByquestionid_First(questionid, orderByComparator);
	}

	/**
	* Returns the first question in the ordered set where questionid = &#63;.
	*
	* @param questionid the questionid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching question, or <code>null</code> if a matching question could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Question fetchByquestionid_First(
		long questionid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByquestionid_First(questionid, orderByComparator);
	}

	/**
	* Returns the last question in the ordered set where questionid = &#63;.
	*
	* @param questionid the questionid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching question
	* @throws kahoot_source.NoSuchQuestionException if a matching question could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Question findByquestionid_Last(
		long questionid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuestionException {
		return getPersistence()
				   .findByquestionid_Last(questionid, orderByComparator);
	}

	/**
	* Returns the last question in the ordered set where questionid = &#63;.
	*
	* @param questionid the questionid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching question, or <code>null</code> if a matching question could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Question fetchByquestionid_Last(
		long questionid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByquestionid_Last(questionid, orderByComparator);
	}

	/**
	* Removes all the questions where questionid = &#63; from the database.
	*
	* @param questionid the questionid
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByquestionid(long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByquestionid(questionid);
	}

	/**
	* Returns the number of questions where questionid = &#63;.
	*
	* @param questionid the questionid
	* @return the number of matching questions
	* @throws SystemException if a system exception occurred
	*/
	public static int countByquestionid(long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByquestionid(questionid);
	}

	/**
	* Caches the question in the entity cache if it is enabled.
	*
	* @param question the question
	*/
	public static void cacheResult(kahoot_source.model.Question question) {
		getPersistence().cacheResult(question);
	}

	/**
	* Caches the questions in the entity cache if it is enabled.
	*
	* @param questions the questions
	*/
	public static void cacheResult(
		java.util.List<kahoot_source.model.Question> questions) {
		getPersistence().cacheResult(questions);
	}

	/**
	* Creates a new question with the primary key. Does not add the question to the database.
	*
	* @param questionid the primary key for the new question
	* @return the new question
	*/
	public static kahoot_source.model.Question create(long questionid) {
		return getPersistence().create(questionid);
	}

	/**
	* Removes the question with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param questionid the primary key of the question
	* @return the question that was removed
	* @throws kahoot_source.NoSuchQuestionException if a question with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Question remove(long questionid)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuestionException {
		return getPersistence().remove(questionid);
	}

	public static kahoot_source.model.Question updateImpl(
		kahoot_source.model.Question question)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(question);
	}

	/**
	* Returns the question with the primary key or throws a {@link kahoot_source.NoSuchQuestionException} if it could not be found.
	*
	* @param questionid the primary key of the question
	* @return the question
	* @throws kahoot_source.NoSuchQuestionException if a question with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Question findByPrimaryKey(long questionid)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuestionException {
		return getPersistence().findByPrimaryKey(questionid);
	}

	/**
	* Returns the question with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param questionid the primary key of the question
	* @return the question, or <code>null</code> if a question with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Question fetchByPrimaryKey(
		long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(questionid);
	}

	/**
	* Returns all the questions.
	*
	* @return the questions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Question> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the questions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of questions
	* @param end the upper bound of the range of questions (not inclusive)
	* @return the range of questions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Question> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the questions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of questions
	* @param end the upper bound of the range of questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of questions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Question> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the questions from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of questions.
	*
	* @return the number of questions
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	/**
	* Returns all the quizs associated with the question.
	*
	* @param pk the primary key of the question
	* @return the quizs associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> getQuizs(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getQuizs(pk);
	}

	/**
	* Returns a range of all the quizs associated with the question.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the question
	* @param start the lower bound of the range of questions
	* @param end the upper bound of the range of questions (not inclusive)
	* @return the range of quizs associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> getQuizs(long pk,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getQuizs(pk, start, end);
	}

	/**
	* Returns an ordered range of all the quizs associated with the question.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the question
	* @param start the lower bound of the range of questions
	* @param end the upper bound of the range of questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of quizs associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> getQuizs(long pk,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getQuizs(pk, start, end, orderByComparator);
	}

	/**
	* Returns the number of quizs associated with the question.
	*
	* @param pk the primary key of the question
	* @return the number of quizs associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public static int getQuizsSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getQuizsSize(pk);
	}

	/**
	* Returns <code>true</code> if the quiz is associated with the question.
	*
	* @param pk the primary key of the question
	* @param quizPK the primary key of the quiz
	* @return <code>true</code> if the quiz is associated with the question; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsQuiz(long pk, long quizPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsQuiz(pk, quizPK);
	}

	/**
	* Returns <code>true</code> if the question has any quizs associated with it.
	*
	* @param pk the primary key of the question to check for associations with quizs
	* @return <code>true</code> if the question has any quizs associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsQuizs(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsQuizs(pk);
	}

	/**
	* Adds an association between the question and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param quizPK the primary key of the quiz
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuiz(long pk, long quizPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addQuiz(pk, quizPK);
	}

	/**
	* Adds an association between the question and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param quiz the quiz
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuiz(long pk, kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addQuiz(pk, quiz);
	}

	/**
	* Adds an association between the question and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param quizPKs the primary keys of the quizs
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuizs(long pk, long[] quizPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addQuizs(pk, quizPKs);
	}

	/**
	* Adds an association between the question and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param quizs the quizs
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuizs(long pk,
		java.util.List<kahoot_source.model.Quiz> quizs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addQuizs(pk, quizs);
	}

	/**
	* Clears all associations between the question and its quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question to clear the associated quizs from
	* @throws SystemException if a system exception occurred
	*/
	public static void clearQuizs(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().clearQuizs(pk);
	}

	/**
	* Removes the association between the question and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param quizPK the primary key of the quiz
	* @throws SystemException if a system exception occurred
	*/
	public static void removeQuiz(long pk, long quizPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeQuiz(pk, quizPK);
	}

	/**
	* Removes the association between the question and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param quiz the quiz
	* @throws SystemException if a system exception occurred
	*/
	public static void removeQuiz(long pk, kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeQuiz(pk, quiz);
	}

	/**
	* Removes the association between the question and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param quizPKs the primary keys of the quizs
	* @throws SystemException if a system exception occurred
	*/
	public static void removeQuizs(long pk, long[] quizPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeQuizs(pk, quizPKs);
	}

	/**
	* Removes the association between the question and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param quizs the quizs
	* @throws SystemException if a system exception occurred
	*/
	public static void removeQuizs(long pk,
		java.util.List<kahoot_source.model.Quiz> quizs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeQuizs(pk, quizs);
	}

	/**
	* Sets the quizs associated with the question, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param quizPKs the primary keys of the quizs to be associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public static void setQuizs(long pk, long[] quizPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().setQuizs(pk, quizPKs);
	}

	/**
	* Sets the quizs associated with the question, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param quizs the quizs to be associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public static void setQuizs(long pk,
		java.util.List<kahoot_source.model.Quiz> quizs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().setQuizs(pk, quizs);
	}

	/**
	* Returns all the answers associated with the question.
	*
	* @param pk the primary key of the question
	* @return the answers associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Answer> getAnswers(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getAnswers(pk);
	}

	/**
	* Returns a range of all the answers associated with the question.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the question
	* @param start the lower bound of the range of questions
	* @param end the upper bound of the range of questions (not inclusive)
	* @return the range of answers associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Answer> getAnswers(
		long pk, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getAnswers(pk, start, end);
	}

	/**
	* Returns an ordered range of all the answers associated with the question.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the question
	* @param start the lower bound of the range of questions
	* @param end the upper bound of the range of questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of answers associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Answer> getAnswers(
		long pk, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getAnswers(pk, start, end, orderByComparator);
	}

	/**
	* Returns the number of answers associated with the question.
	*
	* @param pk the primary key of the question
	* @return the number of answers associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public static int getAnswersSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getAnswersSize(pk);
	}

	/**
	* Returns <code>true</code> if the answer is associated with the question.
	*
	* @param pk the primary key of the question
	* @param answerPK the primary key of the answer
	* @return <code>true</code> if the answer is associated with the question; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsAnswer(long pk, long answerPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsAnswer(pk, answerPK);
	}

	/**
	* Returns <code>true</code> if the question has any answers associated with it.
	*
	* @param pk the primary key of the question to check for associations with answers
	* @return <code>true</code> if the question has any answers associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsAnswers(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsAnswers(pk);
	}

	/**
	* Adds an association between the question and the answer. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param answerPK the primary key of the answer
	* @throws SystemException if a system exception occurred
	*/
	public static void addAnswer(long pk, long answerPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addAnswer(pk, answerPK);
	}

	/**
	* Adds an association between the question and the answer. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param answer the answer
	* @throws SystemException if a system exception occurred
	*/
	public static void addAnswer(long pk, kahoot_source.model.Answer answer)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addAnswer(pk, answer);
	}

	/**
	* Adds an association between the question and the answers. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param answerPKs the primary keys of the answers
	* @throws SystemException if a system exception occurred
	*/
	public static void addAnswers(long pk, long[] answerPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addAnswers(pk, answerPKs);
	}

	/**
	* Adds an association between the question and the answers. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param answers the answers
	* @throws SystemException if a system exception occurred
	*/
	public static void addAnswers(long pk,
		java.util.List<kahoot_source.model.Answer> answers)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addAnswers(pk, answers);
	}

	/**
	* Clears all associations between the question and its answers. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question to clear the associated answers from
	* @throws SystemException if a system exception occurred
	*/
	public static void clearAnswers(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().clearAnswers(pk);
	}

	/**
	* Removes the association between the question and the answer. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param answerPK the primary key of the answer
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAnswer(long pk, long answerPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAnswer(pk, answerPK);
	}

	/**
	* Removes the association between the question and the answer. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param answer the answer
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAnswer(long pk, kahoot_source.model.Answer answer)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAnswer(pk, answer);
	}

	/**
	* Removes the association between the question and the answers. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param answerPKs the primary keys of the answers
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAnswers(long pk, long[] answerPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAnswers(pk, answerPKs);
	}

	/**
	* Removes the association between the question and the answers. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param answers the answers
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAnswers(long pk,
		java.util.List<kahoot_source.model.Answer> answers)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAnswers(pk, answers);
	}

	/**
	* Sets the answers associated with the question, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param answerPKs the primary keys of the answers to be associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public static void setAnswers(long pk, long[] answerPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().setAnswers(pk, answerPKs);
	}

	/**
	* Sets the answers associated with the question, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param answers the answers to be associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public static void setAnswers(long pk,
		java.util.List<kahoot_source.model.Answer> answers)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().setAnswers(pk, answers);
	}

	public static QuestionPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (QuestionPersistence)PortletBeanLocatorUtil.locate(kahoot_source.service.ClpSerializer.getServletContextName(),
					QuestionPersistence.class.getName());

			ReferenceRegistry.registerReference(QuestionUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(QuestionPersistence persistence) {
	}

	private static QuestionPersistence _persistence;
}