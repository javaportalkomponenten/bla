/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import kahoot_source.model.Quiz;

import java.util.List;

/**
 * The persistence utility for the quiz service. This utility wraps {@link QuizPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author krzysiek
 * @see QuizPersistence
 * @see QuizPersistenceImpl
 * @generated
 */
public class QuizUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Quiz quiz) {
		getPersistence().clearCache(quiz);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Quiz> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Quiz> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Quiz> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static Quiz update(Quiz quiz) throws SystemException {
		return getPersistence().update(quiz);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static Quiz update(Quiz quiz, ServiceContext serviceContext)
		throws SystemException {
		return getPersistence().update(quiz, serviceContext);
	}

	/**
	* Returns all the quizs where quizid = &#63;.
	*
	* @param quizid the quizid
	* @return the matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> findByquizid(
		long quizid) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByquizid(quizid);
	}

	/**
	* Returns a range of all the quizs where quizid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param quizid the quizid
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @return the range of matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> findByquizid(
		long quizid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByquizid(quizid, start, end);
	}

	/**
	* Returns an ordered range of all the quizs where quizid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param quizid the quizid
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> findByquizid(
		long quizid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByquizid(quizid, start, end, orderByComparator);
	}

	/**
	* Returns the first quiz in the ordered set where quizid = &#63;.
	*
	* @param quizid the quizid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching quiz
	* @throws kahoot_source.NoSuchQuizException if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz findByquizid_First(long quizid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizException {
		return getPersistence().findByquizid_First(quizid, orderByComparator);
	}

	/**
	* Returns the first quiz in the ordered set where quizid = &#63;.
	*
	* @param quizid the quizid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching quiz, or <code>null</code> if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz fetchByquizid_First(long quizid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByquizid_First(quizid, orderByComparator);
	}

	/**
	* Returns the last quiz in the ordered set where quizid = &#63;.
	*
	* @param quizid the quizid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching quiz
	* @throws kahoot_source.NoSuchQuizException if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz findByquizid_Last(long quizid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizException {
		return getPersistence().findByquizid_Last(quizid, orderByComparator);
	}

	/**
	* Returns the last quiz in the ordered set where quizid = &#63;.
	*
	* @param quizid the quizid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching quiz, or <code>null</code> if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz fetchByquizid_Last(long quizid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByquizid_Last(quizid, orderByComparator);
	}

	/**
	* Removes all the quizs where quizid = &#63; from the database.
	*
	* @param quizid the quizid
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByquizid(long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByquizid(quizid);
	}

	/**
	* Returns the number of quizs where quizid = &#63;.
	*
	* @param quizid the quizid
	* @return the number of matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByquizid(long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByquizid(quizid);
	}

	/**
	* Returns all the quizs where title = &#63;.
	*
	* @param title the title
	* @return the matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> findBytitle(
		java.lang.String title)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBytitle(title);
	}

	/**
	* Returns a range of all the quizs where title = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param title the title
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @return the range of matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> findBytitle(
		java.lang.String title, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBytitle(title, start, end);
	}

	/**
	* Returns an ordered range of all the quizs where title = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param title the title
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> findBytitle(
		java.lang.String title, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBytitle(title, start, end, orderByComparator);
	}

	/**
	* Returns the first quiz in the ordered set where title = &#63;.
	*
	* @param title the title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching quiz
	* @throws kahoot_source.NoSuchQuizException if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz findBytitle_First(
		java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizException {
		return getPersistence().findBytitle_First(title, orderByComparator);
	}

	/**
	* Returns the first quiz in the ordered set where title = &#63;.
	*
	* @param title the title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching quiz, or <code>null</code> if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz fetchBytitle_First(
		java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBytitle_First(title, orderByComparator);
	}

	/**
	* Returns the last quiz in the ordered set where title = &#63;.
	*
	* @param title the title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching quiz
	* @throws kahoot_source.NoSuchQuizException if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz findBytitle_Last(
		java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizException {
		return getPersistence().findBytitle_Last(title, orderByComparator);
	}

	/**
	* Returns the last quiz in the ordered set where title = &#63;.
	*
	* @param title the title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching quiz, or <code>null</code> if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz fetchBytitle_Last(
		java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBytitle_Last(title, orderByComparator);
	}

	/**
	* Returns the quizs before and after the current quiz in the ordered set where title = &#63;.
	*
	* @param quizid the primary key of the current quiz
	* @param title the title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next quiz
	* @throws kahoot_source.NoSuchQuizException if a quiz with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz[] findBytitle_PrevAndNext(
		long quizid, java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizException {
		return getPersistence()
				   .findBytitle_PrevAndNext(quizid, title, orderByComparator);
	}

	/**
	* Removes all the quizs where title = &#63; from the database.
	*
	* @param title the title
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBytitle(java.lang.String title)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBytitle(title);
	}

	/**
	* Returns the number of quizs where title = &#63;.
	*
	* @param title the title
	* @return the number of matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public static int countBytitle(java.lang.String title)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBytitle(title);
	}

	/**
	* Returns all the quizs where running = &#63;.
	*
	* @param running the running
	* @return the matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> findByrunning(
		boolean running)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByrunning(running);
	}

	/**
	* Returns a range of all the quizs where running = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param running the running
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @return the range of matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> findByrunning(
		boolean running, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByrunning(running, start, end);
	}

	/**
	* Returns an ordered range of all the quizs where running = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param running the running
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> findByrunning(
		boolean running, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByrunning(running, start, end, orderByComparator);
	}

	/**
	* Returns the first quiz in the ordered set where running = &#63;.
	*
	* @param running the running
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching quiz
	* @throws kahoot_source.NoSuchQuizException if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz findByrunning_First(
		boolean running,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizException {
		return getPersistence().findByrunning_First(running, orderByComparator);
	}

	/**
	* Returns the first quiz in the ordered set where running = &#63;.
	*
	* @param running the running
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching quiz, or <code>null</code> if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz fetchByrunning_First(
		boolean running,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByrunning_First(running, orderByComparator);
	}

	/**
	* Returns the last quiz in the ordered set where running = &#63;.
	*
	* @param running the running
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching quiz
	* @throws kahoot_source.NoSuchQuizException if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz findByrunning_Last(boolean running,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizException {
		return getPersistence().findByrunning_Last(running, orderByComparator);
	}

	/**
	* Returns the last quiz in the ordered set where running = &#63;.
	*
	* @param running the running
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching quiz, or <code>null</code> if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz fetchByrunning_Last(
		boolean running,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByrunning_Last(running, orderByComparator);
	}

	/**
	* Returns the quizs before and after the current quiz in the ordered set where running = &#63;.
	*
	* @param quizid the primary key of the current quiz
	* @param running the running
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next quiz
	* @throws kahoot_source.NoSuchQuizException if a quiz with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz[] findByrunning_PrevAndNext(
		long quizid, boolean running,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizException {
		return getPersistence()
				   .findByrunning_PrevAndNext(quizid, running, orderByComparator);
	}

	/**
	* Removes all the quizs where running = &#63; from the database.
	*
	* @param running the running
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByrunning(boolean running)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByrunning(running);
	}

	/**
	* Returns the number of quizs where running = &#63;.
	*
	* @param running the running
	* @return the number of matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByrunning(boolean running)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByrunning(running);
	}

	/**
	* Caches the quiz in the entity cache if it is enabled.
	*
	* @param quiz the quiz
	*/
	public static void cacheResult(kahoot_source.model.Quiz quiz) {
		getPersistence().cacheResult(quiz);
	}

	/**
	* Caches the quizs in the entity cache if it is enabled.
	*
	* @param quizs the quizs
	*/
	public static void cacheResult(
		java.util.List<kahoot_source.model.Quiz> quizs) {
		getPersistence().cacheResult(quizs);
	}

	/**
	* Creates a new quiz with the primary key. Does not add the quiz to the database.
	*
	* @param quizid the primary key for the new quiz
	* @return the new quiz
	*/
	public static kahoot_source.model.Quiz create(long quizid) {
		return getPersistence().create(quizid);
	}

	/**
	* Removes the quiz with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param quizid the primary key of the quiz
	* @return the quiz that was removed
	* @throws kahoot_source.NoSuchQuizException if a quiz with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz remove(long quizid)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizException {
		return getPersistence().remove(quizid);
	}

	public static kahoot_source.model.Quiz updateImpl(
		kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(quiz);
	}

	/**
	* Returns the quiz with the primary key or throws a {@link kahoot_source.NoSuchQuizException} if it could not be found.
	*
	* @param quizid the primary key of the quiz
	* @return the quiz
	* @throws kahoot_source.NoSuchQuizException if a quiz with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz findByPrimaryKey(long quizid)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizException {
		return getPersistence().findByPrimaryKey(quizid);
	}

	/**
	* Returns the quiz with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param quizid the primary key of the quiz
	* @return the quiz, or <code>null</code> if a quiz with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Quiz fetchByPrimaryKey(long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(quizid);
	}

	/**
	* Returns all the quizs.
	*
	* @return the quizs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the quizs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @return the range of quizs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the quizs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of quizs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quiz> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the quizs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of quizs.
	*
	* @return the number of quizs
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	/**
	* Returns all the quizgroups associated with the quiz.
	*
	* @param pk the primary key of the quiz
	* @return the quizgroups associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quizgroup> getQuizgroups(
		long pk) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getQuizgroups(pk);
	}

	/**
	* Returns a range of all the quizgroups associated with the quiz.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the quiz
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @return the range of quizgroups associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quizgroup> getQuizgroups(
		long pk, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getQuizgroups(pk, start, end);
	}

	/**
	* Returns an ordered range of all the quizgroups associated with the quiz.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the quiz
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of quizgroups associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Quizgroup> getQuizgroups(
		long pk, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getQuizgroups(pk, start, end, orderByComparator);
	}

	/**
	* Returns the number of quizgroups associated with the quiz.
	*
	* @param pk the primary key of the quiz
	* @return the number of quizgroups associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public static int getQuizgroupsSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getQuizgroupsSize(pk);
	}

	/**
	* Returns <code>true</code> if the quizgroup is associated with the quiz.
	*
	* @param pk the primary key of the quiz
	* @param quizgroupPK the primary key of the quizgroup
	* @return <code>true</code> if the quizgroup is associated with the quiz; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsQuizgroup(long pk, long quizgroupPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsQuizgroup(pk, quizgroupPK);
	}

	/**
	* Returns <code>true</code> if the quiz has any quizgroups associated with it.
	*
	* @param pk the primary key of the quiz to check for associations with quizgroups
	* @return <code>true</code> if the quiz has any quizgroups associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsQuizgroups(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsQuizgroups(pk);
	}

	/**
	* Adds an association between the quiz and the quizgroup. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param quizgroupPK the primary key of the quizgroup
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuizgroup(long pk, long quizgroupPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addQuizgroup(pk, quizgroupPK);
	}

	/**
	* Adds an association between the quiz and the quizgroup. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param quizgroup the quizgroup
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuizgroup(long pk,
		kahoot_source.model.Quizgroup quizgroup)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addQuizgroup(pk, quizgroup);
	}

	/**
	* Adds an association between the quiz and the quizgroups. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param quizgroupPKs the primary keys of the quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuizgroups(long pk, long[] quizgroupPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addQuizgroups(pk, quizgroupPKs);
	}

	/**
	* Adds an association between the quiz and the quizgroups. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param quizgroups the quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuizgroups(long pk,
		java.util.List<kahoot_source.model.Quizgroup> quizgroups)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addQuizgroups(pk, quizgroups);
	}

	/**
	* Clears all associations between the quiz and its quizgroups. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz to clear the associated quizgroups from
	* @throws SystemException if a system exception occurred
	*/
	public static void clearQuizgroups(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().clearQuizgroups(pk);
	}

	/**
	* Removes the association between the quiz and the quizgroup. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param quizgroupPK the primary key of the quizgroup
	* @throws SystemException if a system exception occurred
	*/
	public static void removeQuizgroup(long pk, long quizgroupPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeQuizgroup(pk, quizgroupPK);
	}

	/**
	* Removes the association between the quiz and the quizgroup. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param quizgroup the quizgroup
	* @throws SystemException if a system exception occurred
	*/
	public static void removeQuizgroup(long pk,
		kahoot_source.model.Quizgroup quizgroup)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeQuizgroup(pk, quizgroup);
	}

	/**
	* Removes the association between the quiz and the quizgroups. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param quizgroupPKs the primary keys of the quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public static void removeQuizgroups(long pk, long[] quizgroupPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeQuizgroups(pk, quizgroupPKs);
	}

	/**
	* Removes the association between the quiz and the quizgroups. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param quizgroups the quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public static void removeQuizgroups(long pk,
		java.util.List<kahoot_source.model.Quizgroup> quizgroups)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeQuizgroups(pk, quizgroups);
	}

	/**
	* Sets the quizgroups associated with the quiz, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param quizgroupPKs the primary keys of the quizgroups to be associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public static void setQuizgroups(long pk, long[] quizgroupPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().setQuizgroups(pk, quizgroupPKs);
	}

	/**
	* Sets the quizgroups associated with the quiz, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param quizgroups the quizgroups to be associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public static void setQuizgroups(long pk,
		java.util.List<kahoot_source.model.Quizgroup> quizgroups)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().setQuizgroups(pk, quizgroups);
	}

	/**
	* Returns all the questions associated with the quiz.
	*
	* @param pk the primary key of the quiz
	* @return the questions associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Question> getQuestions(
		long pk) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getQuestions(pk);
	}

	/**
	* Returns a range of all the questions associated with the quiz.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the quiz
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @return the range of questions associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Question> getQuestions(
		long pk, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getQuestions(pk, start, end);
	}

	/**
	* Returns an ordered range of all the questions associated with the quiz.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the quiz
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of questions associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Question> getQuestions(
		long pk, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getQuestions(pk, start, end, orderByComparator);
	}

	/**
	* Returns the number of questions associated with the quiz.
	*
	* @param pk the primary key of the quiz
	* @return the number of questions associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public static int getQuestionsSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getQuestionsSize(pk);
	}

	/**
	* Returns <code>true</code> if the question is associated with the quiz.
	*
	* @param pk the primary key of the quiz
	* @param questionPK the primary key of the question
	* @return <code>true</code> if the question is associated with the quiz; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsQuestion(long pk, long questionPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsQuestion(pk, questionPK);
	}

	/**
	* Returns <code>true</code> if the quiz has any questions associated with it.
	*
	* @param pk the primary key of the quiz to check for associations with questions
	* @return <code>true</code> if the quiz has any questions associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsQuestions(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsQuestions(pk);
	}

	/**
	* Adds an association between the quiz and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param questionPK the primary key of the question
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuestion(long pk, long questionPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addQuestion(pk, questionPK);
	}

	/**
	* Adds an association between the quiz and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param question the question
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuestion(long pk,
		kahoot_source.model.Question question)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addQuestion(pk, question);
	}

	/**
	* Adds an association between the quiz and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param questionPKs the primary keys of the questions
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuestions(long pk, long[] questionPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addQuestions(pk, questionPKs);
	}

	/**
	* Adds an association between the quiz and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param questions the questions
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuestions(long pk,
		java.util.List<kahoot_source.model.Question> questions)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addQuestions(pk, questions);
	}

	/**
	* Clears all associations between the quiz and its questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz to clear the associated questions from
	* @throws SystemException if a system exception occurred
	*/
	public static void clearQuestions(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().clearQuestions(pk);
	}

	/**
	* Removes the association between the quiz and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param questionPK the primary key of the question
	* @throws SystemException if a system exception occurred
	*/
	public static void removeQuestion(long pk, long questionPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeQuestion(pk, questionPK);
	}

	/**
	* Removes the association between the quiz and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param question the question
	* @throws SystemException if a system exception occurred
	*/
	public static void removeQuestion(long pk,
		kahoot_source.model.Question question)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeQuestion(pk, question);
	}

	/**
	* Removes the association between the quiz and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param questionPKs the primary keys of the questions
	* @throws SystemException if a system exception occurred
	*/
	public static void removeQuestions(long pk, long[] questionPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeQuestions(pk, questionPKs);
	}

	/**
	* Removes the association between the quiz and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param questions the questions
	* @throws SystemException if a system exception occurred
	*/
	public static void removeQuestions(long pk,
		java.util.List<kahoot_source.model.Question> questions)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeQuestions(pk, questions);
	}

	/**
	* Sets the questions associated with the quiz, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param questionPKs the primary keys of the questions to be associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public static void setQuestions(long pk, long[] questionPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().setQuestions(pk, questionPKs);
	}

	/**
	* Sets the questions associated with the quiz, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param questions the questions to be associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public static void setQuestions(long pk,
		java.util.List<kahoot_source.model.Question> questions)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().setQuestions(pk, questions);
	}

	public static QuizPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (QuizPersistence)PortletBeanLocatorUtil.locate(kahoot_source.service.ClpSerializer.getServletContextName(),
					QuizPersistence.class.getName());

			ReferenceRegistry.registerReference(QuizUtil.class, "_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(QuizPersistence persistence) {
	}

	private static QuizPersistence _persistence;
}