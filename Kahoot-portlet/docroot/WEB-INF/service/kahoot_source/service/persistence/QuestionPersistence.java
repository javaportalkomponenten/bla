/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import kahoot_source.model.Question;

/**
 * The persistence interface for the question service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author krzysiek
 * @see QuestionPersistenceImpl
 * @see QuestionUtil
 * @generated
 */
public interface QuestionPersistence extends BasePersistence<Question> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link QuestionUtil} to access the question persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the questions where questionid = &#63;.
	*
	* @param questionid the questionid
	* @return the matching questions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Question> findByquestionid(
		long questionid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the questions where questionid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionid the questionid
	* @param start the lower bound of the range of questions
	* @param end the upper bound of the range of questions (not inclusive)
	* @return the range of matching questions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Question> findByquestionid(
		long questionid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the questions where questionid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionid the questionid
	* @param start the lower bound of the range of questions
	* @param end the upper bound of the range of questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching questions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Question> findByquestionid(
		long questionid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first question in the ordered set where questionid = &#63;.
	*
	* @param questionid the questionid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching question
	* @throws kahoot_source.NoSuchQuestionException if a matching question could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Question findByquestionid_First(
		long questionid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuestionException;

	/**
	* Returns the first question in the ordered set where questionid = &#63;.
	*
	* @param questionid the questionid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching question, or <code>null</code> if a matching question could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Question fetchByquestionid_First(
		long questionid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last question in the ordered set where questionid = &#63;.
	*
	* @param questionid the questionid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching question
	* @throws kahoot_source.NoSuchQuestionException if a matching question could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Question findByquestionid_Last(long questionid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuestionException;

	/**
	* Returns the last question in the ordered set where questionid = &#63;.
	*
	* @param questionid the questionid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching question, or <code>null</code> if a matching question could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Question fetchByquestionid_Last(
		long questionid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the questions where questionid = &#63; from the database.
	*
	* @param questionid the questionid
	* @throws SystemException if a system exception occurred
	*/
	public void removeByquestionid(long questionid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of questions where questionid = &#63;.
	*
	* @param questionid the questionid
	* @return the number of matching questions
	* @throws SystemException if a system exception occurred
	*/
	public int countByquestionid(long questionid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the question in the entity cache if it is enabled.
	*
	* @param question the question
	*/
	public void cacheResult(kahoot_source.model.Question question);

	/**
	* Caches the questions in the entity cache if it is enabled.
	*
	* @param questions the questions
	*/
	public void cacheResult(
		java.util.List<kahoot_source.model.Question> questions);

	/**
	* Creates a new question with the primary key. Does not add the question to the database.
	*
	* @param questionid the primary key for the new question
	* @return the new question
	*/
	public kahoot_source.model.Question create(long questionid);

	/**
	* Removes the question with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param questionid the primary key of the question
	* @return the question that was removed
	* @throws kahoot_source.NoSuchQuestionException if a question with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Question remove(long questionid)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuestionException;

	public kahoot_source.model.Question updateImpl(
		kahoot_source.model.Question question)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the question with the primary key or throws a {@link kahoot_source.NoSuchQuestionException} if it could not be found.
	*
	* @param questionid the primary key of the question
	* @return the question
	* @throws kahoot_source.NoSuchQuestionException if a question with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Question findByPrimaryKey(long questionid)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuestionException;

	/**
	* Returns the question with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param questionid the primary key of the question
	* @return the question, or <code>null</code> if a question with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Question fetchByPrimaryKey(long questionid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the questions.
	*
	* @return the questions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Question> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the questions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of questions
	* @param end the upper bound of the range of questions (not inclusive)
	* @return the range of questions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Question> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the questions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of questions
	* @param end the upper bound of the range of questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of questions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Question> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the questions from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of questions.
	*
	* @return the number of questions
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the quizs associated with the question.
	*
	* @param pk the primary key of the question
	* @return the quizs associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quiz> getQuizs(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the quizs associated with the question.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the question
	* @param start the lower bound of the range of questions
	* @param end the upper bound of the range of questions (not inclusive)
	* @return the range of quizs associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quiz> getQuizs(long pk,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the quizs associated with the question.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the question
	* @param start the lower bound of the range of questions
	* @param end the upper bound of the range of questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of quizs associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quiz> getQuizs(long pk,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of quizs associated with the question.
	*
	* @param pk the primary key of the question
	* @return the number of quizs associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public int getQuizsSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the quiz is associated with the question.
	*
	* @param pk the primary key of the question
	* @param quizPK the primary key of the quiz
	* @return <code>true</code> if the quiz is associated with the question; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsQuiz(long pk, long quizPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the question has any quizs associated with it.
	*
	* @param pk the primary key of the question to check for associations with quizs
	* @return <code>true</code> if the question has any quizs associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsQuizs(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the question and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param quizPK the primary key of the quiz
	* @throws SystemException if a system exception occurred
	*/
	public void addQuiz(long pk, long quizPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the question and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param quiz the quiz
	* @throws SystemException if a system exception occurred
	*/
	public void addQuiz(long pk, kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the question and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param quizPKs the primary keys of the quizs
	* @throws SystemException if a system exception occurred
	*/
	public void addQuizs(long pk, long[] quizPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the question and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param quizs the quizs
	* @throws SystemException if a system exception occurred
	*/
	public void addQuizs(long pk, java.util.List<kahoot_source.model.Quiz> quizs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Clears all associations between the question and its quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question to clear the associated quizs from
	* @throws SystemException if a system exception occurred
	*/
	public void clearQuizs(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the question and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param quizPK the primary key of the quiz
	* @throws SystemException if a system exception occurred
	*/
	public void removeQuiz(long pk, long quizPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the question and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param quiz the quiz
	* @throws SystemException if a system exception occurred
	*/
	public void removeQuiz(long pk, kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the question and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param quizPKs the primary keys of the quizs
	* @throws SystemException if a system exception occurred
	*/
	public void removeQuizs(long pk, long[] quizPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the question and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param quizs the quizs
	* @throws SystemException if a system exception occurred
	*/
	public void removeQuizs(long pk,
		java.util.List<kahoot_source.model.Quiz> quizs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Sets the quizs associated with the question, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param quizPKs the primary keys of the quizs to be associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public void setQuizs(long pk, long[] quizPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Sets the quizs associated with the question, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param quizs the quizs to be associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public void setQuizs(long pk, java.util.List<kahoot_source.model.Quiz> quizs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the answers associated with the question.
	*
	* @param pk the primary key of the question
	* @return the answers associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Answer> getAnswers(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the answers associated with the question.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the question
	* @param start the lower bound of the range of questions
	* @param end the upper bound of the range of questions (not inclusive)
	* @return the range of answers associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Answer> getAnswers(long pk,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the answers associated with the question.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the question
	* @param start the lower bound of the range of questions
	* @param end the upper bound of the range of questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of answers associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Answer> getAnswers(long pk,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of answers associated with the question.
	*
	* @param pk the primary key of the question
	* @return the number of answers associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public int getAnswersSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the answer is associated with the question.
	*
	* @param pk the primary key of the question
	* @param answerPK the primary key of the answer
	* @return <code>true</code> if the answer is associated with the question; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsAnswer(long pk, long answerPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the question has any answers associated with it.
	*
	* @param pk the primary key of the question to check for associations with answers
	* @return <code>true</code> if the question has any answers associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsAnswers(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the question and the answer. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param answerPK the primary key of the answer
	* @throws SystemException if a system exception occurred
	*/
	public void addAnswer(long pk, long answerPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the question and the answer. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param answer the answer
	* @throws SystemException if a system exception occurred
	*/
	public void addAnswer(long pk, kahoot_source.model.Answer answer)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the question and the answers. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param answerPKs the primary keys of the answers
	* @throws SystemException if a system exception occurred
	*/
	public void addAnswers(long pk, long[] answerPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the question and the answers. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param answers the answers
	* @throws SystemException if a system exception occurred
	*/
	public void addAnswers(long pk,
		java.util.List<kahoot_source.model.Answer> answers)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Clears all associations between the question and its answers. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question to clear the associated answers from
	* @throws SystemException if a system exception occurred
	*/
	public void clearAnswers(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the question and the answer. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param answerPK the primary key of the answer
	* @throws SystemException if a system exception occurred
	*/
	public void removeAnswer(long pk, long answerPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the question and the answer. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param answer the answer
	* @throws SystemException if a system exception occurred
	*/
	public void removeAnswer(long pk, kahoot_source.model.Answer answer)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the question and the answers. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param answerPKs the primary keys of the answers
	* @throws SystemException if a system exception occurred
	*/
	public void removeAnswers(long pk, long[] answerPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the question and the answers. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param answers the answers
	* @throws SystemException if a system exception occurred
	*/
	public void removeAnswers(long pk,
		java.util.List<kahoot_source.model.Answer> answers)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Sets the answers associated with the question, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param answerPKs the primary keys of the answers to be associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public void setAnswers(long pk, long[] answerPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Sets the answers associated with the question, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the question
	* @param answers the answers to be associated with the question
	* @throws SystemException if a system exception occurred
	*/
	public void setAnswers(long pk,
		java.util.List<kahoot_source.model.Answer> answers)
		throws com.liferay.portal.kernel.exception.SystemException;
}