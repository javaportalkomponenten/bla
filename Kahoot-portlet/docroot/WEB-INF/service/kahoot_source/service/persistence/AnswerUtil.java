/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import kahoot_source.model.Answer;

import java.util.List;

/**
 * The persistence utility for the answer service. This utility wraps {@link AnswerPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author krzysiek
 * @see AnswerPersistence
 * @see AnswerPersistenceImpl
 * @generated
 */
public class AnswerUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Answer answer) {
		getPersistence().clearCache(answer);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Answer> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Answer> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Answer> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static Answer update(Answer answer) throws SystemException {
		return getPersistence().update(answer);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static Answer update(Answer answer, ServiceContext serviceContext)
		throws SystemException {
		return getPersistence().update(answer, serviceContext);
	}

	/**
	* Returns all the answers where answerid = &#63;.
	*
	* @param answerid the answerid
	* @return the matching answers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Answer> findByanswerid(
		long answerid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByanswerid(answerid);
	}

	/**
	* Returns a range of all the answers where answerid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param answerid the answerid
	* @param start the lower bound of the range of answers
	* @param end the upper bound of the range of answers (not inclusive)
	* @return the range of matching answers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Answer> findByanswerid(
		long answerid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByanswerid(answerid, start, end);
	}

	/**
	* Returns an ordered range of all the answers where answerid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param answerid the answerid
	* @param start the lower bound of the range of answers
	* @param end the upper bound of the range of answers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching answers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Answer> findByanswerid(
		long answerid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByanswerid(answerid, start, end, orderByComparator);
	}

	/**
	* Returns the first answer in the ordered set where answerid = &#63;.
	*
	* @param answerid the answerid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching answer
	* @throws kahoot_source.NoSuchAnswerException if a matching answer could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Answer findByanswerid_First(
		long answerid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchAnswerException {
		return getPersistence().findByanswerid_First(answerid, orderByComparator);
	}

	/**
	* Returns the first answer in the ordered set where answerid = &#63;.
	*
	* @param answerid the answerid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching answer, or <code>null</code> if a matching answer could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Answer fetchByanswerid_First(
		long answerid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByanswerid_First(answerid, orderByComparator);
	}

	/**
	* Returns the last answer in the ordered set where answerid = &#63;.
	*
	* @param answerid the answerid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching answer
	* @throws kahoot_source.NoSuchAnswerException if a matching answer could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Answer findByanswerid_Last(
		long answerid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchAnswerException {
		return getPersistence().findByanswerid_Last(answerid, orderByComparator);
	}

	/**
	* Returns the last answer in the ordered set where answerid = &#63;.
	*
	* @param answerid the answerid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching answer, or <code>null</code> if a matching answer could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Answer fetchByanswerid_Last(
		long answerid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByanswerid_Last(answerid, orderByComparator);
	}

	/**
	* Removes all the answers where answerid = &#63; from the database.
	*
	* @param answerid the answerid
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByanswerid(long answerid)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByanswerid(answerid);
	}

	/**
	* Returns the number of answers where answerid = &#63;.
	*
	* @param answerid the answerid
	* @return the number of matching answers
	* @throws SystemException if a system exception occurred
	*/
	public static int countByanswerid(long answerid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByanswerid(answerid);
	}

	/**
	* Returns all the answers where right = &#63;.
	*
	* @param right the right
	* @return the matching answers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Answer> findByright(
		boolean right)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByright(right);
	}

	/**
	* Returns a range of all the answers where right = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param right the right
	* @param start the lower bound of the range of answers
	* @param end the upper bound of the range of answers (not inclusive)
	* @return the range of matching answers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Answer> findByright(
		boolean right, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByright(right, start, end);
	}

	/**
	* Returns an ordered range of all the answers where right = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param right the right
	* @param start the lower bound of the range of answers
	* @param end the upper bound of the range of answers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching answers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Answer> findByright(
		boolean right, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByright(right, start, end, orderByComparator);
	}

	/**
	* Returns the first answer in the ordered set where right = &#63;.
	*
	* @param right the right
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching answer
	* @throws kahoot_source.NoSuchAnswerException if a matching answer could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Answer findByright_First(boolean right,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchAnswerException {
		return getPersistence().findByright_First(right, orderByComparator);
	}

	/**
	* Returns the first answer in the ordered set where right = &#63;.
	*
	* @param right the right
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching answer, or <code>null</code> if a matching answer could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Answer fetchByright_First(boolean right,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByright_First(right, orderByComparator);
	}

	/**
	* Returns the last answer in the ordered set where right = &#63;.
	*
	* @param right the right
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching answer
	* @throws kahoot_source.NoSuchAnswerException if a matching answer could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Answer findByright_Last(boolean right,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchAnswerException {
		return getPersistence().findByright_Last(right, orderByComparator);
	}

	/**
	* Returns the last answer in the ordered set where right = &#63;.
	*
	* @param right the right
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching answer, or <code>null</code> if a matching answer could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Answer fetchByright_Last(boolean right,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByright_Last(right, orderByComparator);
	}

	/**
	* Returns the answers before and after the current answer in the ordered set where right = &#63;.
	*
	* @param answerid the primary key of the current answer
	* @param right the right
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next answer
	* @throws kahoot_source.NoSuchAnswerException if a answer with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Answer[] findByright_PrevAndNext(
		long answerid, boolean right,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchAnswerException {
		return getPersistence()
				   .findByright_PrevAndNext(answerid, right, orderByComparator);
	}

	/**
	* Removes all the answers where right = &#63; from the database.
	*
	* @param right the right
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByright(boolean right)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByright(right);
	}

	/**
	* Returns the number of answers where right = &#63;.
	*
	* @param right the right
	* @return the number of matching answers
	* @throws SystemException if a system exception occurred
	*/
	public static int countByright(boolean right)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByright(right);
	}

	/**
	* Caches the answer in the entity cache if it is enabled.
	*
	* @param answer the answer
	*/
	public static void cacheResult(kahoot_source.model.Answer answer) {
		getPersistence().cacheResult(answer);
	}

	/**
	* Caches the answers in the entity cache if it is enabled.
	*
	* @param answers the answers
	*/
	public static void cacheResult(
		java.util.List<kahoot_source.model.Answer> answers) {
		getPersistence().cacheResult(answers);
	}

	/**
	* Creates a new answer with the primary key. Does not add the answer to the database.
	*
	* @param answerid the primary key for the new answer
	* @return the new answer
	*/
	public static kahoot_source.model.Answer create(long answerid) {
		return getPersistence().create(answerid);
	}

	/**
	* Removes the answer with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param answerid the primary key of the answer
	* @return the answer that was removed
	* @throws kahoot_source.NoSuchAnswerException if a answer with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Answer remove(long answerid)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchAnswerException {
		return getPersistence().remove(answerid);
	}

	public static kahoot_source.model.Answer updateImpl(
		kahoot_source.model.Answer answer)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(answer);
	}

	/**
	* Returns the answer with the primary key or throws a {@link kahoot_source.NoSuchAnswerException} if it could not be found.
	*
	* @param answerid the primary key of the answer
	* @return the answer
	* @throws kahoot_source.NoSuchAnswerException if a answer with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Answer findByPrimaryKey(long answerid)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchAnswerException {
		return getPersistence().findByPrimaryKey(answerid);
	}

	/**
	* Returns the answer with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param answerid the primary key of the answer
	* @return the answer, or <code>null</code> if a answer with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static kahoot_source.model.Answer fetchByPrimaryKey(long answerid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(answerid);
	}

	/**
	* Returns all the answers.
	*
	* @return the answers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Answer> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the answers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of answers
	* @param end the upper bound of the range of answers (not inclusive)
	* @return the range of answers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Answer> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the answers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of answers
	* @param end the upper bound of the range of answers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of answers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Answer> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the answers from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of answers.
	*
	* @return the number of answers
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	/**
	* Returns all the questions associated with the answer.
	*
	* @param pk the primary key of the answer
	* @return the questions associated with the answer
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Question> getQuestions(
		long pk) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getQuestions(pk);
	}

	/**
	* Returns a range of all the questions associated with the answer.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the answer
	* @param start the lower bound of the range of answers
	* @param end the upper bound of the range of answers (not inclusive)
	* @return the range of questions associated with the answer
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Question> getQuestions(
		long pk, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getQuestions(pk, start, end);
	}

	/**
	* Returns an ordered range of all the questions associated with the answer.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the answer
	* @param start the lower bound of the range of answers
	* @param end the upper bound of the range of answers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of questions associated with the answer
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<kahoot_source.model.Question> getQuestions(
		long pk, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getQuestions(pk, start, end, orderByComparator);
	}

	/**
	* Returns the number of questions associated with the answer.
	*
	* @param pk the primary key of the answer
	* @return the number of questions associated with the answer
	* @throws SystemException if a system exception occurred
	*/
	public static int getQuestionsSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getQuestionsSize(pk);
	}

	/**
	* Returns <code>true</code> if the question is associated with the answer.
	*
	* @param pk the primary key of the answer
	* @param questionPK the primary key of the question
	* @return <code>true</code> if the question is associated with the answer; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsQuestion(long pk, long questionPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsQuestion(pk, questionPK);
	}

	/**
	* Returns <code>true</code> if the answer has any questions associated with it.
	*
	* @param pk the primary key of the answer to check for associations with questions
	* @return <code>true</code> if the answer has any questions associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsQuestions(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsQuestions(pk);
	}

	/**
	* Adds an association between the answer and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer
	* @param questionPK the primary key of the question
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuestion(long pk, long questionPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addQuestion(pk, questionPK);
	}

	/**
	* Adds an association between the answer and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer
	* @param question the question
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuestion(long pk,
		kahoot_source.model.Question question)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addQuestion(pk, question);
	}

	/**
	* Adds an association between the answer and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer
	* @param questionPKs the primary keys of the questions
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuestions(long pk, long[] questionPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addQuestions(pk, questionPKs);
	}

	/**
	* Adds an association between the answer and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer
	* @param questions the questions
	* @throws SystemException if a system exception occurred
	*/
	public static void addQuestions(long pk,
		java.util.List<kahoot_source.model.Question> questions)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addQuestions(pk, questions);
	}

	/**
	* Clears all associations between the answer and its questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer to clear the associated questions from
	* @throws SystemException if a system exception occurred
	*/
	public static void clearQuestions(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().clearQuestions(pk);
	}

	/**
	* Removes the association between the answer and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer
	* @param questionPK the primary key of the question
	* @throws SystemException if a system exception occurred
	*/
	public static void removeQuestion(long pk, long questionPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeQuestion(pk, questionPK);
	}

	/**
	* Removes the association between the answer and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer
	* @param question the question
	* @throws SystemException if a system exception occurred
	*/
	public static void removeQuestion(long pk,
		kahoot_source.model.Question question)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeQuestion(pk, question);
	}

	/**
	* Removes the association between the answer and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer
	* @param questionPKs the primary keys of the questions
	* @throws SystemException if a system exception occurred
	*/
	public static void removeQuestions(long pk, long[] questionPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeQuestions(pk, questionPKs);
	}

	/**
	* Removes the association between the answer and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer
	* @param questions the questions
	* @throws SystemException if a system exception occurred
	*/
	public static void removeQuestions(long pk,
		java.util.List<kahoot_source.model.Question> questions)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeQuestions(pk, questions);
	}

	/**
	* Sets the questions associated with the answer, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer
	* @param questionPKs the primary keys of the questions to be associated with the answer
	* @throws SystemException if a system exception occurred
	*/
	public static void setQuestions(long pk, long[] questionPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().setQuestions(pk, questionPKs);
	}

	/**
	* Sets the questions associated with the answer, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer
	* @param questions the questions to be associated with the answer
	* @throws SystemException if a system exception occurred
	*/
	public static void setQuestions(long pk,
		java.util.List<kahoot_source.model.Question> questions)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().setQuestions(pk, questions);
	}

	public static AnswerPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (AnswerPersistence)PortletBeanLocatorUtil.locate(kahoot_source.service.ClpSerializer.getServletContextName(),
					AnswerPersistence.class.getName());

			ReferenceRegistry.registerReference(AnswerUtil.class, "_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(AnswerPersistence persistence) {
	}

	private static AnswerPersistence _persistence;
}