/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import kahoot_source.model.Quiz;

/**
 * The persistence interface for the quiz service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author krzysiek
 * @see QuizPersistenceImpl
 * @see QuizUtil
 * @generated
 */
public interface QuizPersistence extends BasePersistence<Quiz> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link QuizUtil} to access the quiz persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the quizs where quizid = &#63;.
	*
	* @param quizid the quizid
	* @return the matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quiz> findByquizid(long quizid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the quizs where quizid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param quizid the quizid
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @return the range of matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quiz> findByquizid(long quizid,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the quizs where quizid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param quizid the quizid
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quiz> findByquizid(long quizid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first quiz in the ordered set where quizid = &#63;.
	*
	* @param quizid the quizid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching quiz
	* @throws kahoot_source.NoSuchQuizException if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quiz findByquizid_First(long quizid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizException;

	/**
	* Returns the first quiz in the ordered set where quizid = &#63;.
	*
	* @param quizid the quizid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching quiz, or <code>null</code> if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quiz fetchByquizid_First(long quizid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last quiz in the ordered set where quizid = &#63;.
	*
	* @param quizid the quizid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching quiz
	* @throws kahoot_source.NoSuchQuizException if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quiz findByquizid_Last(long quizid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizException;

	/**
	* Returns the last quiz in the ordered set where quizid = &#63;.
	*
	* @param quizid the quizid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching quiz, or <code>null</code> if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quiz fetchByquizid_Last(long quizid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the quizs where quizid = &#63; from the database.
	*
	* @param quizid the quizid
	* @throws SystemException if a system exception occurred
	*/
	public void removeByquizid(long quizid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of quizs where quizid = &#63;.
	*
	* @param quizid the quizid
	* @return the number of matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public int countByquizid(long quizid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the quizs where title = &#63;.
	*
	* @param title the title
	* @return the matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quiz> findBytitle(
		java.lang.String title)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the quizs where title = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param title the title
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @return the range of matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quiz> findBytitle(
		java.lang.String title, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the quizs where title = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param title the title
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quiz> findBytitle(
		java.lang.String title, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first quiz in the ordered set where title = &#63;.
	*
	* @param title the title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching quiz
	* @throws kahoot_source.NoSuchQuizException if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quiz findBytitle_First(java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizException;

	/**
	* Returns the first quiz in the ordered set where title = &#63;.
	*
	* @param title the title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching quiz, or <code>null</code> if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quiz fetchBytitle_First(java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last quiz in the ordered set where title = &#63;.
	*
	* @param title the title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching quiz
	* @throws kahoot_source.NoSuchQuizException if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quiz findBytitle_Last(java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizException;

	/**
	* Returns the last quiz in the ordered set where title = &#63;.
	*
	* @param title the title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching quiz, or <code>null</code> if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quiz fetchBytitle_Last(java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the quizs before and after the current quiz in the ordered set where title = &#63;.
	*
	* @param quizid the primary key of the current quiz
	* @param title the title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next quiz
	* @throws kahoot_source.NoSuchQuizException if a quiz with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quiz[] findBytitle_PrevAndNext(long quizid,
		java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizException;

	/**
	* Removes all the quizs where title = &#63; from the database.
	*
	* @param title the title
	* @throws SystemException if a system exception occurred
	*/
	public void removeBytitle(java.lang.String title)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of quizs where title = &#63;.
	*
	* @param title the title
	* @return the number of matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public int countBytitle(java.lang.String title)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the quizs where running = &#63;.
	*
	* @param running the running
	* @return the matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quiz> findByrunning(
		boolean running)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the quizs where running = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param running the running
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @return the range of matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quiz> findByrunning(
		boolean running, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the quizs where running = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param running the running
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quiz> findByrunning(
		boolean running, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first quiz in the ordered set where running = &#63;.
	*
	* @param running the running
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching quiz
	* @throws kahoot_source.NoSuchQuizException if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quiz findByrunning_First(boolean running,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizException;

	/**
	* Returns the first quiz in the ordered set where running = &#63;.
	*
	* @param running the running
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching quiz, or <code>null</code> if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quiz fetchByrunning_First(boolean running,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last quiz in the ordered set where running = &#63;.
	*
	* @param running the running
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching quiz
	* @throws kahoot_source.NoSuchQuizException if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quiz findByrunning_Last(boolean running,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizException;

	/**
	* Returns the last quiz in the ordered set where running = &#63;.
	*
	* @param running the running
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching quiz, or <code>null</code> if a matching quiz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quiz fetchByrunning_Last(boolean running,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the quizs before and after the current quiz in the ordered set where running = &#63;.
	*
	* @param quizid the primary key of the current quiz
	* @param running the running
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next quiz
	* @throws kahoot_source.NoSuchQuizException if a quiz with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quiz[] findByrunning_PrevAndNext(long quizid,
		boolean running,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizException;

	/**
	* Removes all the quizs where running = &#63; from the database.
	*
	* @param running the running
	* @throws SystemException if a system exception occurred
	*/
	public void removeByrunning(boolean running)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of quizs where running = &#63;.
	*
	* @param running the running
	* @return the number of matching quizs
	* @throws SystemException if a system exception occurred
	*/
	public int countByrunning(boolean running)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the quiz in the entity cache if it is enabled.
	*
	* @param quiz the quiz
	*/
	public void cacheResult(kahoot_source.model.Quiz quiz);

	/**
	* Caches the quizs in the entity cache if it is enabled.
	*
	* @param quizs the quizs
	*/
	public void cacheResult(java.util.List<kahoot_source.model.Quiz> quizs);

	/**
	* Creates a new quiz with the primary key. Does not add the quiz to the database.
	*
	* @param quizid the primary key for the new quiz
	* @return the new quiz
	*/
	public kahoot_source.model.Quiz create(long quizid);

	/**
	* Removes the quiz with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param quizid the primary key of the quiz
	* @return the quiz that was removed
	* @throws kahoot_source.NoSuchQuizException if a quiz with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quiz remove(long quizid)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizException;

	public kahoot_source.model.Quiz updateImpl(kahoot_source.model.Quiz quiz)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the quiz with the primary key or throws a {@link kahoot_source.NoSuchQuizException} if it could not be found.
	*
	* @param quizid the primary key of the quiz
	* @return the quiz
	* @throws kahoot_source.NoSuchQuizException if a quiz with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quiz findByPrimaryKey(long quizid)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchQuizException;

	/**
	* Returns the quiz with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param quizid the primary key of the quiz
	* @return the quiz, or <code>null</code> if a quiz with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Quiz fetchByPrimaryKey(long quizid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the quizs.
	*
	* @return the quizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quiz> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the quizs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @return the range of quizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quiz> findAll(int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the quizs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of quizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quiz> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the quizs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of quizs.
	*
	* @return the number of quizs
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the quizgroups associated with the quiz.
	*
	* @param pk the primary key of the quiz
	* @return the quizgroups associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quizgroup> getQuizgroups(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the quizgroups associated with the quiz.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the quiz
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @return the range of quizgroups associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quizgroup> getQuizgroups(
		long pk, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the quizgroups associated with the quiz.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the quiz
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of quizgroups associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Quizgroup> getQuizgroups(
		long pk, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of quizgroups associated with the quiz.
	*
	* @param pk the primary key of the quiz
	* @return the number of quizgroups associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public int getQuizgroupsSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the quizgroup is associated with the quiz.
	*
	* @param pk the primary key of the quiz
	* @param quizgroupPK the primary key of the quizgroup
	* @return <code>true</code> if the quizgroup is associated with the quiz; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsQuizgroup(long pk, long quizgroupPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the quiz has any quizgroups associated with it.
	*
	* @param pk the primary key of the quiz to check for associations with quizgroups
	* @return <code>true</code> if the quiz has any quizgroups associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsQuizgroups(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the quiz and the quizgroup. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param quizgroupPK the primary key of the quizgroup
	* @throws SystemException if a system exception occurred
	*/
	public void addQuizgroup(long pk, long quizgroupPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the quiz and the quizgroup. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param quizgroup the quizgroup
	* @throws SystemException if a system exception occurred
	*/
	public void addQuizgroup(long pk, kahoot_source.model.Quizgroup quizgroup)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the quiz and the quizgroups. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param quizgroupPKs the primary keys of the quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public void addQuizgroups(long pk, long[] quizgroupPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the quiz and the quizgroups. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param quizgroups the quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public void addQuizgroups(long pk,
		java.util.List<kahoot_source.model.Quizgroup> quizgroups)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Clears all associations between the quiz and its quizgroups. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz to clear the associated quizgroups from
	* @throws SystemException if a system exception occurred
	*/
	public void clearQuizgroups(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the quiz and the quizgroup. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param quizgroupPK the primary key of the quizgroup
	* @throws SystemException if a system exception occurred
	*/
	public void removeQuizgroup(long pk, long quizgroupPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the quiz and the quizgroup. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param quizgroup the quizgroup
	* @throws SystemException if a system exception occurred
	*/
	public void removeQuizgroup(long pk, kahoot_source.model.Quizgroup quizgroup)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the quiz and the quizgroups. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param quizgroupPKs the primary keys of the quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public void removeQuizgroups(long pk, long[] quizgroupPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the quiz and the quizgroups. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param quizgroups the quizgroups
	* @throws SystemException if a system exception occurred
	*/
	public void removeQuizgroups(long pk,
		java.util.List<kahoot_source.model.Quizgroup> quizgroups)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Sets the quizgroups associated with the quiz, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param quizgroupPKs the primary keys of the quizgroups to be associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public void setQuizgroups(long pk, long[] quizgroupPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Sets the quizgroups associated with the quiz, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param quizgroups the quizgroups to be associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public void setQuizgroups(long pk,
		java.util.List<kahoot_source.model.Quizgroup> quizgroups)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the questions associated with the quiz.
	*
	* @param pk the primary key of the quiz
	* @return the questions associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Question> getQuestions(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the questions associated with the quiz.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the quiz
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @return the range of questions associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Question> getQuestions(long pk,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the questions associated with the quiz.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the quiz
	* @param start the lower bound of the range of quizs
	* @param end the upper bound of the range of quizs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of questions associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Question> getQuestions(long pk,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of questions associated with the quiz.
	*
	* @param pk the primary key of the quiz
	* @return the number of questions associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public int getQuestionsSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the question is associated with the quiz.
	*
	* @param pk the primary key of the quiz
	* @param questionPK the primary key of the question
	* @return <code>true</code> if the question is associated with the quiz; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsQuestion(long pk, long questionPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the quiz has any questions associated with it.
	*
	* @param pk the primary key of the quiz to check for associations with questions
	* @return <code>true</code> if the quiz has any questions associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsQuestions(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the quiz and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param questionPK the primary key of the question
	* @throws SystemException if a system exception occurred
	*/
	public void addQuestion(long pk, long questionPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the quiz and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param question the question
	* @throws SystemException if a system exception occurred
	*/
	public void addQuestion(long pk, kahoot_source.model.Question question)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the quiz and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param questionPKs the primary keys of the questions
	* @throws SystemException if a system exception occurred
	*/
	public void addQuestions(long pk, long[] questionPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the quiz and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param questions the questions
	* @throws SystemException if a system exception occurred
	*/
	public void addQuestions(long pk,
		java.util.List<kahoot_source.model.Question> questions)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Clears all associations between the quiz and its questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz to clear the associated questions from
	* @throws SystemException if a system exception occurred
	*/
	public void clearQuestions(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the quiz and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param questionPK the primary key of the question
	* @throws SystemException if a system exception occurred
	*/
	public void removeQuestion(long pk, long questionPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the quiz and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param question the question
	* @throws SystemException if a system exception occurred
	*/
	public void removeQuestion(long pk, kahoot_source.model.Question question)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the quiz and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param questionPKs the primary keys of the questions
	* @throws SystemException if a system exception occurred
	*/
	public void removeQuestions(long pk, long[] questionPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the quiz and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param questions the questions
	* @throws SystemException if a system exception occurred
	*/
	public void removeQuestions(long pk,
		java.util.List<kahoot_source.model.Question> questions)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Sets the questions associated with the quiz, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param questionPKs the primary keys of the questions to be associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public void setQuestions(long pk, long[] questionPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Sets the questions associated with the quiz, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the quiz
	* @param questions the questions to be associated with the quiz
	* @throws SystemException if a system exception occurred
	*/
	public void setQuestions(long pk,
		java.util.List<kahoot_source.model.Question> questions)
		throws com.liferay.portal.kernel.exception.SystemException;
}