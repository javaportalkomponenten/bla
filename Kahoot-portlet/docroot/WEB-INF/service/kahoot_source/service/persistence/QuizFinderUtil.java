/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;

/**
 * @author krzysiek
 */
public class QuizFinderUtil {
	public static java.util.List<kahoot_source.model.Quiz> findByTitleLike(
		java.lang.String title) {
		return getFinder().findByTitleLike(title);
	}

	public static java.util.List<kahoot_source.model.Quiz> findByTimestampBefore(
		java.lang.Long timestamp) {
		return getFinder().findByTimestampBefore(timestamp);
	}

	public static java.util.List<kahoot_source.model.Quiz> findByTimestampAfter(
		java.lang.Long timestamp) {
		return getFinder().findByTimestampAfter(timestamp);
	}

	public static QuizFinder getFinder() {
		if (_finder == null) {
			_finder = (QuizFinder)PortletBeanLocatorUtil.locate(kahoot_source.service.ClpSerializer.getServletContextName(),
					QuizFinder.class.getName());

			ReferenceRegistry.registerReference(QuizFinderUtil.class, "_finder");
		}

		return _finder;
	}

	public void setFinder(QuizFinder finder) {
		_finder = finder;

		ReferenceRegistry.registerReference(QuizFinderUtil.class, "_finder");
	}

	private static QuizFinder _finder;
}