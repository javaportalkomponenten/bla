/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import kahoot_source.model.Answer;

import kahoot_source.service.AnswerLocalServiceUtil;

/**
 * @author krzysiek
 * @generated
 */
public abstract class AnswerActionableDynamicQuery
	extends BaseActionableDynamicQuery {
	public AnswerActionableDynamicQuery() throws SystemException {
		setBaseLocalService(AnswerLocalServiceUtil.getService());
		setClass(Answer.class);

		setClassLoader(kahoot_source.service.ClpSerializer.class.getClassLoader());

		setPrimaryKeyPropertyName("answerid");
	}
}