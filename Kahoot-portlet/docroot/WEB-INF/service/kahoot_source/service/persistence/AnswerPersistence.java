/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import kahoot_source.model.Answer;

/**
 * The persistence interface for the answer service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author krzysiek
 * @see AnswerPersistenceImpl
 * @see AnswerUtil
 * @generated
 */
public interface AnswerPersistence extends BasePersistence<Answer> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AnswerUtil} to access the answer persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the answers where answerid = &#63;.
	*
	* @param answerid the answerid
	* @return the matching answers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Answer> findByanswerid(
		long answerid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the answers where answerid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param answerid the answerid
	* @param start the lower bound of the range of answers
	* @param end the upper bound of the range of answers (not inclusive)
	* @return the range of matching answers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Answer> findByanswerid(
		long answerid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the answers where answerid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param answerid the answerid
	* @param start the lower bound of the range of answers
	* @param end the upper bound of the range of answers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching answers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Answer> findByanswerid(
		long answerid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first answer in the ordered set where answerid = &#63;.
	*
	* @param answerid the answerid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching answer
	* @throws kahoot_source.NoSuchAnswerException if a matching answer could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Answer findByanswerid_First(long answerid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchAnswerException;

	/**
	* Returns the first answer in the ordered set where answerid = &#63;.
	*
	* @param answerid the answerid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching answer, or <code>null</code> if a matching answer could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Answer fetchByanswerid_First(long answerid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last answer in the ordered set where answerid = &#63;.
	*
	* @param answerid the answerid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching answer
	* @throws kahoot_source.NoSuchAnswerException if a matching answer could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Answer findByanswerid_Last(long answerid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchAnswerException;

	/**
	* Returns the last answer in the ordered set where answerid = &#63;.
	*
	* @param answerid the answerid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching answer, or <code>null</code> if a matching answer could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Answer fetchByanswerid_Last(long answerid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the answers where answerid = &#63; from the database.
	*
	* @param answerid the answerid
	* @throws SystemException if a system exception occurred
	*/
	public void removeByanswerid(long answerid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of answers where answerid = &#63;.
	*
	* @param answerid the answerid
	* @return the number of matching answers
	* @throws SystemException if a system exception occurred
	*/
	public int countByanswerid(long answerid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the answers where right = &#63;.
	*
	* @param right the right
	* @return the matching answers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Answer> findByright(boolean right)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the answers where right = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param right the right
	* @param start the lower bound of the range of answers
	* @param end the upper bound of the range of answers (not inclusive)
	* @return the range of matching answers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Answer> findByright(
		boolean right, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the answers where right = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param right the right
	* @param start the lower bound of the range of answers
	* @param end the upper bound of the range of answers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching answers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Answer> findByright(
		boolean right, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first answer in the ordered set where right = &#63;.
	*
	* @param right the right
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching answer
	* @throws kahoot_source.NoSuchAnswerException if a matching answer could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Answer findByright_First(boolean right,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchAnswerException;

	/**
	* Returns the first answer in the ordered set where right = &#63;.
	*
	* @param right the right
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching answer, or <code>null</code> if a matching answer could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Answer fetchByright_First(boolean right,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last answer in the ordered set where right = &#63;.
	*
	* @param right the right
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching answer
	* @throws kahoot_source.NoSuchAnswerException if a matching answer could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Answer findByright_Last(boolean right,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchAnswerException;

	/**
	* Returns the last answer in the ordered set where right = &#63;.
	*
	* @param right the right
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching answer, or <code>null</code> if a matching answer could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Answer fetchByright_Last(boolean right,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the answers before and after the current answer in the ordered set where right = &#63;.
	*
	* @param answerid the primary key of the current answer
	* @param right the right
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next answer
	* @throws kahoot_source.NoSuchAnswerException if a answer with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Answer[] findByright_PrevAndNext(long answerid,
		boolean right,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchAnswerException;

	/**
	* Removes all the answers where right = &#63; from the database.
	*
	* @param right the right
	* @throws SystemException if a system exception occurred
	*/
	public void removeByright(boolean right)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of answers where right = &#63;.
	*
	* @param right the right
	* @return the number of matching answers
	* @throws SystemException if a system exception occurred
	*/
	public int countByright(boolean right)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the answer in the entity cache if it is enabled.
	*
	* @param answer the answer
	*/
	public void cacheResult(kahoot_source.model.Answer answer);

	/**
	* Caches the answers in the entity cache if it is enabled.
	*
	* @param answers the answers
	*/
	public void cacheResult(java.util.List<kahoot_source.model.Answer> answers);

	/**
	* Creates a new answer with the primary key. Does not add the answer to the database.
	*
	* @param answerid the primary key for the new answer
	* @return the new answer
	*/
	public kahoot_source.model.Answer create(long answerid);

	/**
	* Removes the answer with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param answerid the primary key of the answer
	* @return the answer that was removed
	* @throws kahoot_source.NoSuchAnswerException if a answer with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Answer remove(long answerid)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchAnswerException;

	public kahoot_source.model.Answer updateImpl(
		kahoot_source.model.Answer answer)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the answer with the primary key or throws a {@link kahoot_source.NoSuchAnswerException} if it could not be found.
	*
	* @param answerid the primary key of the answer
	* @return the answer
	* @throws kahoot_source.NoSuchAnswerException if a answer with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Answer findByPrimaryKey(long answerid)
		throws com.liferay.portal.kernel.exception.SystemException,
			kahoot_source.NoSuchAnswerException;

	/**
	* Returns the answer with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param answerid the primary key of the answer
	* @return the answer, or <code>null</code> if a answer with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public kahoot_source.model.Answer fetchByPrimaryKey(long answerid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the answers.
	*
	* @return the answers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Answer> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the answers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of answers
	* @param end the upper bound of the range of answers (not inclusive)
	* @return the range of answers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Answer> findAll(int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the answers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of answers
	* @param end the upper bound of the range of answers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of answers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Answer> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the answers from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of answers.
	*
	* @return the number of answers
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the questions associated with the answer.
	*
	* @param pk the primary key of the answer
	* @return the questions associated with the answer
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Question> getQuestions(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the questions associated with the answer.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the answer
	* @param start the lower bound of the range of answers
	* @param end the upper bound of the range of answers (not inclusive)
	* @return the range of questions associated with the answer
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Question> getQuestions(long pk,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the questions associated with the answer.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the answer
	* @param start the lower bound of the range of answers
	* @param end the upper bound of the range of answers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of questions associated with the answer
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<kahoot_source.model.Question> getQuestions(long pk,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of questions associated with the answer.
	*
	* @param pk the primary key of the answer
	* @return the number of questions associated with the answer
	* @throws SystemException if a system exception occurred
	*/
	public int getQuestionsSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the question is associated with the answer.
	*
	* @param pk the primary key of the answer
	* @param questionPK the primary key of the question
	* @return <code>true</code> if the question is associated with the answer; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsQuestion(long pk, long questionPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the answer has any questions associated with it.
	*
	* @param pk the primary key of the answer to check for associations with questions
	* @return <code>true</code> if the answer has any questions associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsQuestions(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the answer and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer
	* @param questionPK the primary key of the question
	* @throws SystemException if a system exception occurred
	*/
	public void addQuestion(long pk, long questionPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the answer and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer
	* @param question the question
	* @throws SystemException if a system exception occurred
	*/
	public void addQuestion(long pk, kahoot_source.model.Question question)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the answer and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer
	* @param questionPKs the primary keys of the questions
	* @throws SystemException if a system exception occurred
	*/
	public void addQuestions(long pk, long[] questionPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the answer and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer
	* @param questions the questions
	* @throws SystemException if a system exception occurred
	*/
	public void addQuestions(long pk,
		java.util.List<kahoot_source.model.Question> questions)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Clears all associations between the answer and its questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer to clear the associated questions from
	* @throws SystemException if a system exception occurred
	*/
	public void clearQuestions(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the answer and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer
	* @param questionPK the primary key of the question
	* @throws SystemException if a system exception occurred
	*/
	public void removeQuestion(long pk, long questionPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the answer and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer
	* @param question the question
	* @throws SystemException if a system exception occurred
	*/
	public void removeQuestion(long pk, kahoot_source.model.Question question)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the answer and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer
	* @param questionPKs the primary keys of the questions
	* @throws SystemException if a system exception occurred
	*/
	public void removeQuestions(long pk, long[] questionPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the answer and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer
	* @param questions the questions
	* @throws SystemException if a system exception occurred
	*/
	public void removeQuestions(long pk,
		java.util.List<kahoot_source.model.Question> questions)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Sets the questions associated with the answer, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer
	* @param questionPKs the primary keys of the questions to be associated with the answer
	* @throws SystemException if a system exception occurred
	*/
	public void setQuestions(long pk, long[] questionPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Sets the questions associated with the answer, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the answer
	* @param questions the questions to be associated with the answer
	* @throws SystemException if a system exception occurred
	*/
	public void setQuestions(long pk,
		java.util.List<kahoot_source.model.Question> questions)
		throws com.liferay.portal.kernel.exception.SystemException;
}