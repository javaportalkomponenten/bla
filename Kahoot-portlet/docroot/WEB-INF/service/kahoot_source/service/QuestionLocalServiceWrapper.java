/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link QuestionLocalService}.
 *
 * @author krzysiek
 * @see QuestionLocalService
 * @generated
 */
public class QuestionLocalServiceWrapper implements QuestionLocalService,
	ServiceWrapper<QuestionLocalService> {
	public QuestionLocalServiceWrapper(
		QuestionLocalService questionLocalService) {
		_questionLocalService = questionLocalService;
	}

	/**
	* Adds the question to the database. Also notifies the appropriate model listeners.
	*
	* @param question the question
	* @return the question that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public kahoot_source.model.Question addQuestion(
		kahoot_source.model.Question question)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.addQuestion(question);
	}

	/**
	* Creates a new question with the primary key. Does not add the question to the database.
	*
	* @param questionid the primary key for the new question
	* @return the new question
	*/
	@Override
	public kahoot_source.model.Question createQuestion(long questionid) {
		return _questionLocalService.createQuestion(questionid);
	}

	/**
	* Deletes the question with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param questionid the primary key of the question
	* @return the question that was removed
	* @throws PortalException if a question with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public kahoot_source.model.Question deleteQuestion(long questionid)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.deleteQuestion(questionid);
	}

	/**
	* Deletes the question from the database. Also notifies the appropriate model listeners.
	*
	* @param question the question
	* @return the question that was removed
	* @throws PortalException
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public kahoot_source.model.Question deleteQuestion(
		kahoot_source.model.Question question)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.deleteQuestion(question);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _questionLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public kahoot_source.model.Question fetchQuestion(long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.fetchQuestion(questionid);
	}

	/**
	* Returns the question with the primary key.
	*
	* @param questionid the primary key of the question
	* @return the question
	* @throws PortalException if a question with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public kahoot_source.model.Question getQuestion(long questionid)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.getQuestion(questionid);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the questions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of questions
	* @param end the upper bound of the range of questions (not inclusive)
	* @return the range of questions
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<kahoot_source.model.Question> getQuestions(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.getQuestions(start, end);
	}

	/**
	* Returns the number of questions.
	*
	* @return the number of questions
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getQuestionsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.getQuestionsCount();
	}

	/**
	* Updates the question in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param question the question
	* @return the question that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public kahoot_source.model.Question updateQuestion(
		kahoot_source.model.Question question)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.updateQuestion(question);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void addQuizQuestion(long quizid, long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		_questionLocalService.addQuizQuestion(quizid, questionid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void addQuizQuestion(long quizid,
		kahoot_source.model.Question question)
		throws com.liferay.portal.kernel.exception.SystemException {
		_questionLocalService.addQuizQuestion(quizid, question);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void addQuizQuestions(long quizid, long[] questionids)
		throws com.liferay.portal.kernel.exception.SystemException {
		_questionLocalService.addQuizQuestions(quizid, questionids);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void addQuizQuestions(long quizid,
		java.util.List<kahoot_source.model.Question> Questions)
		throws com.liferay.portal.kernel.exception.SystemException {
		_questionLocalService.addQuizQuestions(quizid, Questions);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void clearQuizQuestions(long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		_questionLocalService.clearQuizQuestions(quizid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void deleteQuizQuestion(long quizid, long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		_questionLocalService.deleteQuizQuestion(quizid, questionid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void deleteQuizQuestion(long quizid,
		kahoot_source.model.Question question)
		throws com.liferay.portal.kernel.exception.SystemException {
		_questionLocalService.deleteQuizQuestion(quizid, question);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void deleteQuizQuestions(long quizid, long[] questionids)
		throws com.liferay.portal.kernel.exception.SystemException {
		_questionLocalService.deleteQuizQuestions(quizid, questionids);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void deleteQuizQuestions(long quizid,
		java.util.List<kahoot_source.model.Question> Questions)
		throws com.liferay.portal.kernel.exception.SystemException {
		_questionLocalService.deleteQuizQuestions(quizid, Questions);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<kahoot_source.model.Question> getQuizQuestions(
		long quizid) throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.getQuizQuestions(quizid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<kahoot_source.model.Question> getQuizQuestions(
		long quizid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.getQuizQuestions(quizid, start, end);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<kahoot_source.model.Question> getQuizQuestions(
		long quizid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.getQuizQuestions(quizid, start, end,
			orderByComparator);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getQuizQuestionsCount(long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.getQuizQuestionsCount(quizid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public boolean hasQuizQuestion(long quizid, long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.hasQuizQuestion(quizid, questionid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public boolean hasQuizQuestions(long quizid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.hasQuizQuestions(quizid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void setQuizQuestions(long quizid, long[] questionids)
		throws com.liferay.portal.kernel.exception.SystemException {
		_questionLocalService.setQuizQuestions(quizid, questionids);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void addAnswerQuestion(long answerid, long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		_questionLocalService.addAnswerQuestion(answerid, questionid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void addAnswerQuestion(long answerid,
		kahoot_source.model.Question question)
		throws com.liferay.portal.kernel.exception.SystemException {
		_questionLocalService.addAnswerQuestion(answerid, question);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void addAnswerQuestions(long answerid, long[] questionids)
		throws com.liferay.portal.kernel.exception.SystemException {
		_questionLocalService.addAnswerQuestions(answerid, questionids);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void addAnswerQuestions(long answerid,
		java.util.List<kahoot_source.model.Question> Questions)
		throws com.liferay.portal.kernel.exception.SystemException {
		_questionLocalService.addAnswerQuestions(answerid, Questions);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void clearAnswerQuestions(long answerid)
		throws com.liferay.portal.kernel.exception.SystemException {
		_questionLocalService.clearAnswerQuestions(answerid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void deleteAnswerQuestion(long answerid, long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		_questionLocalService.deleteAnswerQuestion(answerid, questionid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void deleteAnswerQuestion(long answerid,
		kahoot_source.model.Question question)
		throws com.liferay.portal.kernel.exception.SystemException {
		_questionLocalService.deleteAnswerQuestion(answerid, question);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void deleteAnswerQuestions(long answerid, long[] questionids)
		throws com.liferay.portal.kernel.exception.SystemException {
		_questionLocalService.deleteAnswerQuestions(answerid, questionids);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void deleteAnswerQuestions(long answerid,
		java.util.List<kahoot_source.model.Question> Questions)
		throws com.liferay.portal.kernel.exception.SystemException {
		_questionLocalService.deleteAnswerQuestions(answerid, Questions);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<kahoot_source.model.Question> getAnswerQuestions(
		long answerid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.getAnswerQuestions(answerid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<kahoot_source.model.Question> getAnswerQuestions(
		long answerid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.getAnswerQuestions(answerid, start, end);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<kahoot_source.model.Question> getAnswerQuestions(
		long answerid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.getAnswerQuestions(answerid, start, end,
			orderByComparator);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getAnswerQuestionsCount(long answerid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.getAnswerQuestionsCount(answerid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public boolean hasAnswerQuestion(long answerid, long questionid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.hasAnswerQuestion(answerid, questionid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public boolean hasAnswerQuestions(long answerid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.hasAnswerQuestions(answerid);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public void setAnswerQuestions(long answerid, long[] questionids)
		throws com.liferay.portal.kernel.exception.SystemException {
		_questionLocalService.setAnswerQuestions(answerid, questionids);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _questionLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_questionLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _questionLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public kahoot_source.model.Question addQuestion(
		java.lang.String questionText, java.lang.Long time, int questionType,
		java.lang.String url, java.util.List<kahoot_source.model.Quiz> quizes,
		java.util.List<kahoot_source.model.Answer> answers)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.addQuestion(questionText, time,
			questionType, url, quizes, answers);
	}

	@Override
	public kahoot_source.model.Question updateQuestion(
		java.lang.Long questionId, java.lang.String questionText,
		java.lang.Long time, java.lang.String url,
		java.util.List<kahoot_source.model.Quiz> quizes,
		java.util.List<kahoot_source.model.Answer> answers)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.updateQuestion(questionId, questionText,
			time, url, quizes, answers);
	}

	@Override
	public kahoot_source.model.Question deleteQuestion(
		java.lang.Long questionId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _questionLocalService.deleteQuestion(questionId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public QuestionLocalService getWrappedQuestionLocalService() {
		return _questionLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedQuestionLocalService(
		QuestionLocalService questionLocalService) {
		_questionLocalService = questionLocalService;
	}

	@Override
	public QuestionLocalService getWrappedService() {
		return _questionLocalService;
	}

	@Override
	public void setWrappedService(QuestionLocalService questionLocalService) {
		_questionLocalService = questionLocalService;
	}

	private QuestionLocalService _questionLocalService;
}