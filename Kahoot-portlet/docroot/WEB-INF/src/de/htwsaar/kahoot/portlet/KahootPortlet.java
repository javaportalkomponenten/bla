package de.htwsaar.kahoot.portlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletContext;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;


import kahoot_source.model.Question;
import kahoot_source.model.Quiz;
import kahoot_source.model.Quizgroup;
import kahoot_source.model.impl.QuizImpl;
import kahoot_source.service.QuestionLocalServiceUtil;
import kahoot_source.service.QuizLocalServiceUtil;
import kahoot_source.service.persistence.QuestionUtil;

public class KahootPortlet extends MVCPortlet{
	
	private Quiz currentQuiz;
	
	
	/**
	 * Zeigt den Wert aus dem Ereignis an.
	 */
	/*
	public void doView(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {

		response.setContentType("text/html");
		PortletContext portletContext = this.getPortletContext();
		String mvcPath;
		
		if (request.getParameter("mvcPath") != null) {
			mvcPath = request.getParameter("mvcPath");
		} else {
			mvcPath = "/html/jsps/view.jsp";
		}
	
		PortletRequestDispatcher portletRequestDispatcher = portletContext.getRequestDispatcher(mvcPath);
		portletRequestDispatcher.include(request, response);
		
	}
	*/
	
	public void addQuiz(ActionRequest actionRequest, ActionResponse actionResponse)throws IOException, PortletException{
		System.out.println("addQuiz()");
		try {
			String title = ParamUtil.getString(actionRequest, "title");
			System.out.println("TITLE: " + title);
			
			ArrayList<Quizgroup> alquizgroup = new ArrayList<Quizgroup>();
			ArrayList<Question> alQuestion = new ArrayList<Question>();
			Quiz quiz = QuizLocalServiceUtil.addQuiz(title, 0L, true, alquizgroup,alQuestion);
			currentQuiz = quiz;
			
		
			
			// navigate to add student jsp page
			//actionResponse.setRenderParameter("mvcPath",
			//		"/html/jsps/add_question.jsp");
			//actionResponse.sendRedirect("/html/jsps/add_question.jsp");
			
			
			actionResponse.setRenderParameter("jspPage", "/html/jsps/add_question.jsp");
			
		} catch (Exception e) {
			SessionErrors.add(actionRequest.getPortletSession(),
					"student-add-error");
			e.printStackTrace();
			System.out.println("******************************* ");
			System.out.println(e.getMessage());
		}
		
	}

}
