/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import de.htwsaar.kahoot.exceptions.UpdateException;
import kahoot_source.model.Quiz;
import kahoot_source.model.Quizgroup;
import kahoot_source.service.QuizLocalServiceUtil;
import kahoot_source.service.QuizgroupLocalServiceUtil;
import kahoot_source.service.base.QuizgroupLocalServiceBaseImpl;

/**
 * The implementation of the quizgroup local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link kahoot_source.service.QuizgroupLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author krzysiek
 * @see kahoot_source.service.base.QuizgroupLocalServiceBaseImpl
 * @see kahoot_source.service.QuizgroupLocalServiceUtil
 */
public class QuizgroupLocalServiceImpl extends QuizgroupLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link kahoot_source.service.QuizgroupLocalServiceUtil} to access the quizgroup local service.
	 */
	public Quizgroup addQuizgroup(String name, List<Quiz> quizes) throws SystemException {
		Long quizgroupId = counterLocalService.increment(Quizgroup.class.getName());
		Quizgroup tempQuizgroup = QuizgroupLocalServiceUtil.createQuizgroup(quizgroupId);
		tempQuizgroup.setName(name);
		QuizgroupLocalServiceUtil.addQuizgroup(tempQuizgroup);
		
		if(!quizes.isEmpty()) {
			QuizLocalServiceUtil.addQuizgroupQuizs(quizgroupId, quizes);
		}
		return tempQuizgroup;
	}
	
	public Quizgroup updateQuizgroup(Long quizgroupId, String name, List<Quiz> quizes) 
			throws SystemException {
				
		Quizgroup updateQuizgroup = QuizgroupLocalServiceUtil.fetchQuizgroup(quizgroupId);
		
		if(name != null) updateQuizgroup.setName(name);
		
		QuizgroupLocalServiceUtil.updateQuizgroup(updateQuizgroup);
		
		if(quizes != null) this.updateQuizes(quizgroupId, quizes);
		
		return updateQuizgroup;
	}
	
	private void updateQuizes(Long quizgroupId, List<Quiz> quizes) throws SystemException {
		QuizLocalServiceUtil.clearQuizgroupQuizs(quizgroupId);
		QuizLocalServiceUtil.addQuizgroupQuizs(quizgroupId, quizes);
		
	}

	public Quizgroup deleteQuizgroup(Long quizgroupId) throws SystemException, PortalException {
		Quizgroup deleteQuizgroup = QuizgroupLocalServiceUtil.deleteQuizgroup(quizgroupId);
		QuizLocalServiceUtil.deleteQuizgroupQuizs(quizgroupId, QuizLocalServiceUtil.getQuizgroupQuizs(quizgroupId));
		return deleteQuizgroup;
	}
	
	public Quizgroup deleteQuizgroup(Quizgroup deleteQuizgroup) throws SystemException, PortalException {
		return this.deleteQuizgroup(deleteQuizgroup.getQuizgroupid());
	}
}