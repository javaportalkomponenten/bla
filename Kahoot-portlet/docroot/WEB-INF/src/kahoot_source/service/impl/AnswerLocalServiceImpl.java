/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service.impl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import de.htwsaar.kahoot.exceptions.UpdateException;
import kahoot_source.model.Answer;
import kahoot_source.service.AnswerLocalServiceUtil;
import kahoot_source.service.QuestionLocalServiceUtil;
import kahoot_source.service.base.AnswerLocalServiceBaseImpl;

/**
 * The implementation of the answer local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link kahoot_source.service.AnswerLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author krzysiek
 * @see kahoot_source.service.base.AnswerLocalServiceBaseImpl
 * @see kahoot_source.service.AnswerLocalServiceUtil
 */
public class AnswerLocalServiceImpl extends AnswerLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link kahoot_source.service.AnswerLocalServiceUtil} to access the answer local service.
	 */
	
	public Answer addAnswer(String answerText, boolean isRight) throws SystemException {
		Long answerId = counterLocalService.increment(Answer.class.getName());
		Answer tempAnswer = AnswerLocalServiceUtil.createAnswer(answerId);
		
		tempAnswer.setAnswertext(answerText);
		tempAnswer.setRight(isRight);
		
		AnswerLocalServiceUtil.addAnswer(tempAnswer);
		
		return tempAnswer;
		
	}
	
	public Answer updateAnswer(Long answerId, String answerText, Boolean isRight) 
			throws SystemException {
				
		Answer updateAnswer = AnswerLocalServiceUtil.fetchAnswer(answerId);
		
		if(answerText != null) updateAnswer.setAnswertext(answerText);
		
		if(isRight != null) updateAnswer.setRight(isRight);
		
		AnswerLocalServiceUtil.updateAnswer(updateAnswer);
		
		return updateAnswer;
	}
	public Answer deleteAnswer(Long answerId) throws PortalException, SystemException {
		Answer deleteAnswer = AnswerLocalServiceUtil.deleteAnswer(answerId);
		QuestionLocalServiceUtil.deleteAnswerQuestions(answerId, QuestionLocalServiceUtil.getAnswerQuestions(answerId));
		return deleteAnswer;
	}
	
	public Answer deleteAnswer(Answer deleteAnswer) throws PortalException, SystemException {
		return this.deleteAnswer(deleteAnswer.getAnswerid());
	}
}