/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import de.htwsaar.kahoot.exceptions.UpdateException;
import kahoot_source.model.Answer;
import kahoot_source.model.Question;
import kahoot_source.model.Quiz;
import kahoot_source.service.AnswerLocalServiceUtil;
import kahoot_source.service.QuestionLocalServiceUtil;
import kahoot_source.service.QuizLocalServiceUtil;
import kahoot_source.service.base.QuestionLocalServiceBaseImpl;

/**
 * The implementation of the question local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link kahoot_source.service.QuestionLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author krzysiek
 * @see kahoot_source.service.base.QuestionLocalServiceBaseImpl
 * @see kahoot_source.service.QuestionLocalServiceUtil
 */
public class QuestionLocalServiceImpl extends QuestionLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link kahoot_source.service.QuestionLocalServiceUtil} to access the question local service.
	 */
	public Question addQuestion(String questionText, Long time, int questionType, String url, List<Quiz> quizes, List<Answer> answers) throws SystemException {
		Long questionId = counterLocalService.increment(Question.class.getName());
		Question tempQuestion = QuestionLocalServiceUtil.createQuestion(questionId);
		
		tempQuestion.setQuestiontext(questionText);
		tempQuestion.setTime(time);
		tempQuestion.setBildurl(url);
		
		
		
		QuestionLocalServiceUtil.addQuestion(tempQuestion);
		
		if(!answers.isEmpty()) {
			AnswerLocalServiceUtil.addQuestionAnswers(questionId, answers);
		}
		
		if(!quizes.isEmpty()) {
			QuizLocalServiceUtil.addQuestionQuizs(questionId, quizes);
		}
		
		return tempQuestion;
	}
	
	public Question updateQuestion(Long questionId, String questionText, Long time, String url, List<Quiz> quizes, List<Answer> answers) throws SystemException {
		
		Question updateQuestion = QuestionLocalServiceUtil.fetchQuestion(questionId);
		
		if(questionText != null) updateQuestion.setQuestiontext(questionText); 
		
		if(time != null) updateQuestion.setTime(time);
		
		if(quizes != null) this.updateQuizes(questionId, quizes);
		
		if(answers != null) this.updateAnswers(questionId, answers);
		
		return updateQuestion;
	}
	private void updateAnswers(Long questionId, List<Answer> answers) throws SystemException {
		AnswerLocalServiceUtil.clearQuestionAnswers(questionId);
		AnswerLocalServiceUtil.addQuestionAnswers(questionId, answers);
		
	}

	private void updateQuizes(Long questionId, List<Quiz> quizes) throws SystemException {
		QuizLocalServiceUtil.clearQuestionQuizs(questionId);
		QuizLocalServiceUtil.addQuestionQuizs(questionId, quizes);
		
	}

	public Question deleteQuestion(Long questionId) throws SystemException, PortalException {
		Question deleteQuestion = QuestionLocalServiceUtil.deleteQuestion(questionId);
		QuizLocalServiceUtil.deleteQuestionQuizs(questionId, QuizLocalServiceUtil.getQuestionQuizs(questionId));
		return deleteQuestion;
	}
	
	public Question deleteQuestion(Question deleteQuestion) throws SystemException, PortalException {
		return this.deleteQuestion(deleteQuestion.getQuestionid());
	}
}