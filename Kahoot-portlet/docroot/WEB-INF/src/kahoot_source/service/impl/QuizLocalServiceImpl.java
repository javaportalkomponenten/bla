/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import de.htwsaar.kahoot.exceptions.UpdateException;
import kahoot_source.model.Question;
import kahoot_source.model.Quiz;
import kahoot_source.model.Quizgroup;
import kahoot_source.service.QuestionLocalServiceUtil;
import kahoot_source.service.QuizLocalServiceUtil;
import kahoot_source.service.QuizgroupLocalServiceUtil;
import kahoot_source.service.base.QuizLocalServiceBaseImpl;
import kahoot_source.service.persistence.QuizFinder;
import kahoot_source.service.persistence.QuizFinderImpl;
import kahoot_source.service.persistence.QuizUtil;

/**
 * The implementation of the quiz local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link kahoot_source.service.QuizLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author krzysiek
 * @see kahoot_source.service.base.QuizLocalServiceBaseImpl
 * @see kahoot_source.service.QuizLocalServiceUtil
 */
public class QuizLocalServiceImpl extends QuizLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link kahoot_source.service.QuizLocalServiceUtil} to access the quiz local service.
	 */
	public Quiz addQuiz(String title, Long timestamp, boolean running, List<Quizgroup> quizgroups, List<Question> questions) throws SystemException {
		Long quizId = counterLocalService.increment(Quiz.class.getName());
		Quiz tempQuiz = QuizLocalServiceUtil.createQuiz(quizId);
		
		tempQuiz.setTitle(title);
		tempQuiz.setTimestamp(timestamp);
		tempQuiz.setRunning(running);
		QuizLocalServiceUtil.addQuiz(tempQuiz);
		
		if(!quizgroups.isEmpty()) {
			QuizgroupLocalServiceUtil.addQuizQuizgroups(quizId, quizgroups);
		}
		
		if(!questions.isEmpty()) {
			QuestionLocalServiceUtil.addQuizQuestions(quizId, questions);
		}
		return tempQuiz;
		
	}
	
	public List<Quiz> getByTitle(String title) throws SystemException {
		return QuizUtil.findBytitle(title);
	}
	
	public List<Quiz> getAll() throws SystemException {
		return QuizUtil.findAll();
	}
	
	public List<Quiz> getByTitleLike(String title) {
		QuizFinder quizFinder = new QuizFinderImpl();
		return quizFinder.findByTitleLike(title);
	}
	
	public List<Quiz> getByTimestampBefore(Long timestamp) {
		QuizFinder quizFinder = new QuizFinderImpl();
		return quizFinder.findByTimestampBefore(timestamp);
	}
	public List<Quiz> getRunningQuizes() throws SystemException {
		return QuizUtil.findByrunning(true);
	}
	
	public List<Quiz> getNotRunningQuizes() throws SystemException {
		return QuizUtil.findByrunning(false);
	}
	public Quiz updateQuiz(Long quizId, String title, Long timestamp, Boolean running, List<Quizgroup> quizgroups, List<Question> questions) 
			throws SystemException {
		
		
		Quiz updateQuiz = QuizLocalServiceUtil.fetchQuiz(quizId);
		
		if(title != null) updateQuiz.setTitle(title);
		
		if(timestamp != null) updateQuiz.setTimestamp(timestamp);
		
		if(running != null) updateQuiz.setRunning(running);
		
		QuizLocalServiceUtil.updateQuiz(updateQuiz);
		
		if(quizgroups != null) this.updateQuizgroups(quizId, quizgroups);
		
		if(questions != null) this.updateQuestions(quizId, questions);
		
		return updateQuiz;
		
	}
	
	private void updateQuestions(Long quizId, List<Question> questions) throws SystemException {
		QuestionLocalServiceUtil.clearQuizQuestions(quizId);
		QuestionLocalServiceUtil.addQuizQuestions(quizId, questions);
	}

	private void updateQuizgroups(Long quizId, List<Quizgroup> quizgroups) throws SystemException {
		QuizgroupLocalServiceUtil.clearQuizQuizgroups(quizId);
		QuizgroupLocalServiceUtil.addQuizQuizgroups(quizId, quizgroups);
		
	}

	public Quiz deleteQuiz(Long quizId) throws PortalException, SystemException {
		Quiz deleteQuiz = QuizLocalServiceUtil.deleteQuiz(quizId);
		QuizgroupLocalServiceUtil.deleteQuizQuizgroups(quizId, QuizgroupLocalServiceUtil.getQuizQuizgroups(quizId));
		QuestionLocalServiceUtil.deleteQuizQuestions(quizId, QuestionLocalServiceUtil.getQuizQuestions(quizId));
		return deleteQuiz;
	}
	
	public Quiz deleteQuiz(Quiz deleteQuiz) throws PortalException, SystemException {
		return this.deleteQuiz(deleteQuiz.getQuizid());
	}
	
	
}