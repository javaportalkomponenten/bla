/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service.persistence;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.service.persistence.impl.TableMapper;
import com.liferay.portal.service.persistence.impl.TableMapperFactory;

import kahoot_source.NoSuchQuizgroupException;

import kahoot_source.model.Quizgroup;

import kahoot_source.model.impl.QuizgroupImpl;
import kahoot_source.model.impl.QuizgroupModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the quizgroup service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author krzysiek
 * @see QuizgroupPersistence
 * @see QuizgroupUtil
 * @generated
 */
public class QuizgroupPersistenceImpl extends BasePersistenceImpl<Quizgroup>
	implements QuizgroupPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link QuizgroupUtil} to access the quizgroup persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = QuizgroupImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(QuizgroupModelImpl.ENTITY_CACHE_ENABLED,
			QuizgroupModelImpl.FINDER_CACHE_ENABLED, QuizgroupImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(QuizgroupModelImpl.ENTITY_CACHE_ENABLED,
			QuizgroupModelImpl.FINDER_CACHE_ENABLED, QuizgroupImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(QuizgroupModelImpl.ENTITY_CACHE_ENABLED,
			QuizgroupModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_QUIZGROUPID =
		new FinderPath(QuizgroupModelImpl.ENTITY_CACHE_ENABLED,
			QuizgroupModelImpl.FINDER_CACHE_ENABLED, QuizgroupImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByquizgroupid",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUIZGROUPID =
		new FinderPath(QuizgroupModelImpl.ENTITY_CACHE_ENABLED,
			QuizgroupModelImpl.FINDER_CACHE_ENABLED, QuizgroupImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByquizgroupid",
			new String[] { Long.class.getName() },
			QuizgroupModelImpl.QUIZGROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_QUIZGROUPID = new FinderPath(QuizgroupModelImpl.ENTITY_CACHE_ENABLED,
			QuizgroupModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByquizgroupid",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the quizgroups where quizgroupid = &#63;.
	 *
	 * @param quizgroupid the quizgroupid
	 * @return the matching quizgroups
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quizgroup> findByquizgroupid(long quizgroupid)
		throws SystemException {
		return findByquizgroupid(quizgroupid, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the quizgroups where quizgroupid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param quizgroupid the quizgroupid
	 * @param start the lower bound of the range of quizgroups
	 * @param end the upper bound of the range of quizgroups (not inclusive)
	 * @return the range of matching quizgroups
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quizgroup> findByquizgroupid(long quizgroupid, int start,
		int end) throws SystemException {
		return findByquizgroupid(quizgroupid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the quizgroups where quizgroupid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param quizgroupid the quizgroupid
	 * @param start the lower bound of the range of quizgroups
	 * @param end the upper bound of the range of quizgroups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching quizgroups
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quizgroup> findByquizgroupid(long quizgroupid, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUIZGROUPID;
			finderArgs = new Object[] { quizgroupid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_QUIZGROUPID;
			finderArgs = new Object[] { quizgroupid, start, end, orderByComparator };
		}

		List<Quizgroup> list = (List<Quizgroup>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Quizgroup quizgroup : list) {
				if ((quizgroupid != quizgroup.getQuizgroupid())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_QUIZGROUP_WHERE);

			query.append(_FINDER_COLUMN_QUIZGROUPID_QUIZGROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(QuizgroupModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(quizgroupid);

				if (!pagination) {
					list = (List<Quizgroup>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Quizgroup>(list);
				}
				else {
					list = (List<Quizgroup>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first quizgroup in the ordered set where quizgroupid = &#63;.
	 *
	 * @param quizgroupid the quizgroupid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching quizgroup
	 * @throws kahoot_source.NoSuchQuizgroupException if a matching quizgroup could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quizgroup findByquizgroupid_First(long quizgroupid,
		OrderByComparator orderByComparator)
		throws NoSuchQuizgroupException, SystemException {
		Quizgroup quizgroup = fetchByquizgroupid_First(quizgroupid,
				orderByComparator);

		if (quizgroup != null) {
			return quizgroup;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("quizgroupid=");
		msg.append(quizgroupid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuizgroupException(msg.toString());
	}

	/**
	 * Returns the first quizgroup in the ordered set where quizgroupid = &#63;.
	 *
	 * @param quizgroupid the quizgroupid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching quizgroup, or <code>null</code> if a matching quizgroup could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quizgroup fetchByquizgroupid_First(long quizgroupid,
		OrderByComparator orderByComparator) throws SystemException {
		List<Quizgroup> list = findByquizgroupid(quizgroupid, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last quizgroup in the ordered set where quizgroupid = &#63;.
	 *
	 * @param quizgroupid the quizgroupid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching quizgroup
	 * @throws kahoot_source.NoSuchQuizgroupException if a matching quizgroup could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quizgroup findByquizgroupid_Last(long quizgroupid,
		OrderByComparator orderByComparator)
		throws NoSuchQuizgroupException, SystemException {
		Quizgroup quizgroup = fetchByquizgroupid_Last(quizgroupid,
				orderByComparator);

		if (quizgroup != null) {
			return quizgroup;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("quizgroupid=");
		msg.append(quizgroupid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuizgroupException(msg.toString());
	}

	/**
	 * Returns the last quizgroup in the ordered set where quizgroupid = &#63;.
	 *
	 * @param quizgroupid the quizgroupid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching quizgroup, or <code>null</code> if a matching quizgroup could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quizgroup fetchByquizgroupid_Last(long quizgroupid,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByquizgroupid(quizgroupid);

		if (count == 0) {
			return null;
		}

		List<Quizgroup> list = findByquizgroupid(quizgroupid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Removes all the quizgroups where quizgroupid = &#63; from the database.
	 *
	 * @param quizgroupid the quizgroupid
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByquizgroupid(long quizgroupid) throws SystemException {
		for (Quizgroup quizgroup : findByquizgroupid(quizgroupid,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(quizgroup);
		}
	}

	/**
	 * Returns the number of quizgroups where quizgroupid = &#63;.
	 *
	 * @param quizgroupid the quizgroupid
	 * @return the number of matching quizgroups
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByquizgroupid(long quizgroupid) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_QUIZGROUPID;

		Object[] finderArgs = new Object[] { quizgroupid };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_QUIZGROUP_WHERE);

			query.append(_FINDER_COLUMN_QUIZGROUPID_QUIZGROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(quizgroupid);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_QUIZGROUPID_QUIZGROUPID_2 = "quizgroup.quizgroupid = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NAME = new FinderPath(QuizgroupModelImpl.ENTITY_CACHE_ENABLED,
			QuizgroupModelImpl.FINDER_CACHE_ENABLED, QuizgroupImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByname",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME = new FinderPath(QuizgroupModelImpl.ENTITY_CACHE_ENABLED,
			QuizgroupModelImpl.FINDER_CACHE_ENABLED, QuizgroupImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByname",
			new String[] { String.class.getName() },
			QuizgroupModelImpl.NAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_NAME = new FinderPath(QuizgroupModelImpl.ENTITY_CACHE_ENABLED,
			QuizgroupModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByname",
			new String[] { String.class.getName() });

	/**
	 * Returns all the quizgroups where name = &#63;.
	 *
	 * @param name the name
	 * @return the matching quizgroups
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quizgroup> findByname(String name) throws SystemException {
		return findByname(name, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the quizgroups where name = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param name the name
	 * @param start the lower bound of the range of quizgroups
	 * @param end the upper bound of the range of quizgroups (not inclusive)
	 * @return the range of matching quizgroups
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quizgroup> findByname(String name, int start, int end)
		throws SystemException {
		return findByname(name, start, end, null);
	}

	/**
	 * Returns an ordered range of all the quizgroups where name = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param name the name
	 * @param start the lower bound of the range of quizgroups
	 * @param end the upper bound of the range of quizgroups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching quizgroups
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quizgroup> findByname(String name, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME;
			finderArgs = new Object[] { name };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NAME;
			finderArgs = new Object[] { name, start, end, orderByComparator };
		}

		List<Quizgroup> list = (List<Quizgroup>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Quizgroup quizgroup : list) {
				if (!Validator.equals(name, quizgroup.getName())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_QUIZGROUP_WHERE);

			boolean bindName = false;

			if (name == null) {
				query.append(_FINDER_COLUMN_NAME_NAME_1);
			}
			else if (name.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NAME_NAME_3);
			}
			else {
				bindName = true;

				query.append(_FINDER_COLUMN_NAME_NAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(QuizgroupModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindName) {
					qPos.add(name);
				}

				if (!pagination) {
					list = (List<Quizgroup>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Quizgroup>(list);
				}
				else {
					list = (List<Quizgroup>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first quizgroup in the ordered set where name = &#63;.
	 *
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching quizgroup
	 * @throws kahoot_source.NoSuchQuizgroupException if a matching quizgroup could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quizgroup findByname_First(String name,
		OrderByComparator orderByComparator)
		throws NoSuchQuizgroupException, SystemException {
		Quizgroup quizgroup = fetchByname_First(name, orderByComparator);

		if (quizgroup != null) {
			return quizgroup;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("name=");
		msg.append(name);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuizgroupException(msg.toString());
	}

	/**
	 * Returns the first quizgroup in the ordered set where name = &#63;.
	 *
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching quizgroup, or <code>null</code> if a matching quizgroup could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quizgroup fetchByname_First(String name,
		OrderByComparator orderByComparator) throws SystemException {
		List<Quizgroup> list = findByname(name, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last quizgroup in the ordered set where name = &#63;.
	 *
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching quizgroup
	 * @throws kahoot_source.NoSuchQuizgroupException if a matching quizgroup could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quizgroup findByname_Last(String name,
		OrderByComparator orderByComparator)
		throws NoSuchQuizgroupException, SystemException {
		Quizgroup quizgroup = fetchByname_Last(name, orderByComparator);

		if (quizgroup != null) {
			return quizgroup;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("name=");
		msg.append(name);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuizgroupException(msg.toString());
	}

	/**
	 * Returns the last quizgroup in the ordered set where name = &#63;.
	 *
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching quizgroup, or <code>null</code> if a matching quizgroup could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quizgroup fetchByname_Last(String name,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByname(name);

		if (count == 0) {
			return null;
		}

		List<Quizgroup> list = findByname(name, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the quizgroups before and after the current quizgroup in the ordered set where name = &#63;.
	 *
	 * @param quizgroupid the primary key of the current quizgroup
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next quizgroup
	 * @throws kahoot_source.NoSuchQuizgroupException if a quizgroup with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quizgroup[] findByname_PrevAndNext(long quizgroupid, String name,
		OrderByComparator orderByComparator)
		throws NoSuchQuizgroupException, SystemException {
		Quizgroup quizgroup = findByPrimaryKey(quizgroupid);

		Session session = null;

		try {
			session = openSession();

			Quizgroup[] array = new QuizgroupImpl[3];

			array[0] = getByname_PrevAndNext(session, quizgroup, name,
					orderByComparator, true);

			array[1] = quizgroup;

			array[2] = getByname_PrevAndNext(session, quizgroup, name,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Quizgroup getByname_PrevAndNext(Session session,
		Quizgroup quizgroup, String name, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_QUIZGROUP_WHERE);

		boolean bindName = false;

		if (name == null) {
			query.append(_FINDER_COLUMN_NAME_NAME_1);
		}
		else if (name.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_NAME_NAME_3);
		}
		else {
			bindName = true;

			query.append(_FINDER_COLUMN_NAME_NAME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(QuizgroupModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindName) {
			qPos.add(name);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(quizgroup);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Quizgroup> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the quizgroups where name = &#63; from the database.
	 *
	 * @param name the name
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByname(String name) throws SystemException {
		for (Quizgroup quizgroup : findByname(name, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(quizgroup);
		}
	}

	/**
	 * Returns the number of quizgroups where name = &#63;.
	 *
	 * @param name the name
	 * @return the number of matching quizgroups
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByname(String name) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_NAME;

		Object[] finderArgs = new Object[] { name };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_QUIZGROUP_WHERE);

			boolean bindName = false;

			if (name == null) {
				query.append(_FINDER_COLUMN_NAME_NAME_1);
			}
			else if (name.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NAME_NAME_3);
			}
			else {
				bindName = true;

				query.append(_FINDER_COLUMN_NAME_NAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindName) {
					qPos.add(name);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_NAME_NAME_1 = "quizgroup.name IS NULL";
	private static final String _FINDER_COLUMN_NAME_NAME_2 = "quizgroup.name = ?";
	private static final String _FINDER_COLUMN_NAME_NAME_3 = "(quizgroup.name IS NULL OR quizgroup.name = '')";

	public QuizgroupPersistenceImpl() {
		setModelClass(Quizgroup.class);
	}

	/**
	 * Caches the quizgroup in the entity cache if it is enabled.
	 *
	 * @param quizgroup the quizgroup
	 */
	@Override
	public void cacheResult(Quizgroup quizgroup) {
		EntityCacheUtil.putResult(QuizgroupModelImpl.ENTITY_CACHE_ENABLED,
			QuizgroupImpl.class, quizgroup.getPrimaryKey(), quizgroup);

		quizgroup.resetOriginalValues();
	}

	/**
	 * Caches the quizgroups in the entity cache if it is enabled.
	 *
	 * @param quizgroups the quizgroups
	 */
	@Override
	public void cacheResult(List<Quizgroup> quizgroups) {
		for (Quizgroup quizgroup : quizgroups) {
			if (EntityCacheUtil.getResult(
						QuizgroupModelImpl.ENTITY_CACHE_ENABLED,
						QuizgroupImpl.class, quizgroup.getPrimaryKey()) == null) {
				cacheResult(quizgroup);
			}
			else {
				quizgroup.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all quizgroups.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(QuizgroupImpl.class.getName());
		}

		EntityCacheUtil.clearCache(QuizgroupImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the quizgroup.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Quizgroup quizgroup) {
		EntityCacheUtil.removeResult(QuizgroupModelImpl.ENTITY_CACHE_ENABLED,
			QuizgroupImpl.class, quizgroup.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Quizgroup> quizgroups) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Quizgroup quizgroup : quizgroups) {
			EntityCacheUtil.removeResult(QuizgroupModelImpl.ENTITY_CACHE_ENABLED,
				QuizgroupImpl.class, quizgroup.getPrimaryKey());
		}
	}

	/**
	 * Creates a new quizgroup with the primary key. Does not add the quizgroup to the database.
	 *
	 * @param quizgroupid the primary key for the new quizgroup
	 * @return the new quizgroup
	 */
	@Override
	public Quizgroup create(long quizgroupid) {
		Quizgroup quizgroup = new QuizgroupImpl();

		quizgroup.setNew(true);
		quizgroup.setPrimaryKey(quizgroupid);

		return quizgroup;
	}

	/**
	 * Removes the quizgroup with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param quizgroupid the primary key of the quizgroup
	 * @return the quizgroup that was removed
	 * @throws kahoot_source.NoSuchQuizgroupException if a quizgroup with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quizgroup remove(long quizgroupid)
		throws NoSuchQuizgroupException, SystemException {
		return remove((Serializable)quizgroupid);
	}

	/**
	 * Removes the quizgroup with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the quizgroup
	 * @return the quizgroup that was removed
	 * @throws kahoot_source.NoSuchQuizgroupException if a quizgroup with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quizgroup remove(Serializable primaryKey)
		throws NoSuchQuizgroupException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Quizgroup quizgroup = (Quizgroup)session.get(QuizgroupImpl.class,
					primaryKey);

			if (quizgroup == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchQuizgroupException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(quizgroup);
		}
		catch (NoSuchQuizgroupException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Quizgroup removeImpl(Quizgroup quizgroup)
		throws SystemException {
		quizgroup = toUnwrappedModel(quizgroup);

		quizgroupToQuizTableMapper.deleteLeftPrimaryKeyTableMappings(quizgroup.getPrimaryKey());

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(quizgroup)) {
				quizgroup = (Quizgroup)session.get(QuizgroupImpl.class,
						quizgroup.getPrimaryKeyObj());
			}

			if (quizgroup != null) {
				session.delete(quizgroup);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (quizgroup != null) {
			clearCache(quizgroup);
		}

		return quizgroup;
	}

	@Override
	public Quizgroup updateImpl(kahoot_source.model.Quizgroup quizgroup)
		throws SystemException {
		quizgroup = toUnwrappedModel(quizgroup);

		boolean isNew = quizgroup.isNew();

		QuizgroupModelImpl quizgroupModelImpl = (QuizgroupModelImpl)quizgroup;

		Session session = null;

		try {
			session = openSession();

			if (quizgroup.isNew()) {
				session.save(quizgroup);

				quizgroup.setNew(false);
			}
			else {
				session.merge(quizgroup);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !QuizgroupModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((quizgroupModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUIZGROUPID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						quizgroupModelImpl.getOriginalQuizgroupid()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUIZGROUPID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUIZGROUPID,
					args);

				args = new Object[] { quizgroupModelImpl.getQuizgroupid() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUIZGROUPID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUIZGROUPID,
					args);
			}

			if ((quizgroupModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						quizgroupModelImpl.getOriginalName()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME,
					args);

				args = new Object[] { quizgroupModelImpl.getName() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME,
					args);
			}
		}

		EntityCacheUtil.putResult(QuizgroupModelImpl.ENTITY_CACHE_ENABLED,
			QuizgroupImpl.class, quizgroup.getPrimaryKey(), quizgroup);

		return quizgroup;
	}

	protected Quizgroup toUnwrappedModel(Quizgroup quizgroup) {
		if (quizgroup instanceof QuizgroupImpl) {
			return quizgroup;
		}

		QuizgroupImpl quizgroupImpl = new QuizgroupImpl();

		quizgroupImpl.setNew(quizgroup.isNew());
		quizgroupImpl.setPrimaryKey(quizgroup.getPrimaryKey());

		quizgroupImpl.setQuizgroupid(quizgroup.getQuizgroupid());
		quizgroupImpl.setName(quizgroup.getName());

		return quizgroupImpl;
	}

	/**
	 * Returns the quizgroup with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the quizgroup
	 * @return the quizgroup
	 * @throws kahoot_source.NoSuchQuizgroupException if a quizgroup with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quizgroup findByPrimaryKey(Serializable primaryKey)
		throws NoSuchQuizgroupException, SystemException {
		Quizgroup quizgroup = fetchByPrimaryKey(primaryKey);

		if (quizgroup == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchQuizgroupException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return quizgroup;
	}

	/**
	 * Returns the quizgroup with the primary key or throws a {@link kahoot_source.NoSuchQuizgroupException} if it could not be found.
	 *
	 * @param quizgroupid the primary key of the quizgroup
	 * @return the quizgroup
	 * @throws kahoot_source.NoSuchQuizgroupException if a quizgroup with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quizgroup findByPrimaryKey(long quizgroupid)
		throws NoSuchQuizgroupException, SystemException {
		return findByPrimaryKey((Serializable)quizgroupid);
	}

	/**
	 * Returns the quizgroup with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the quizgroup
	 * @return the quizgroup, or <code>null</code> if a quizgroup with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quizgroup fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Quizgroup quizgroup = (Quizgroup)EntityCacheUtil.getResult(QuizgroupModelImpl.ENTITY_CACHE_ENABLED,
				QuizgroupImpl.class, primaryKey);

		if (quizgroup == _nullQuizgroup) {
			return null;
		}

		if (quizgroup == null) {
			Session session = null;

			try {
				session = openSession();

				quizgroup = (Quizgroup)session.get(QuizgroupImpl.class,
						primaryKey);

				if (quizgroup != null) {
					cacheResult(quizgroup);
				}
				else {
					EntityCacheUtil.putResult(QuizgroupModelImpl.ENTITY_CACHE_ENABLED,
						QuizgroupImpl.class, primaryKey, _nullQuizgroup);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(QuizgroupModelImpl.ENTITY_CACHE_ENABLED,
					QuizgroupImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return quizgroup;
	}

	/**
	 * Returns the quizgroup with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param quizgroupid the primary key of the quizgroup
	 * @return the quizgroup, or <code>null</code> if a quizgroup with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quizgroup fetchByPrimaryKey(long quizgroupid)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)quizgroupid);
	}

	/**
	 * Returns all the quizgroups.
	 *
	 * @return the quizgroups
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quizgroup> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the quizgroups.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of quizgroups
	 * @param end the upper bound of the range of quizgroups (not inclusive)
	 * @return the range of quizgroups
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quizgroup> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the quizgroups.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of quizgroups
	 * @param end the upper bound of the range of quizgroups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of quizgroups
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quizgroup> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Quizgroup> list = (List<Quizgroup>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_QUIZGROUP);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_QUIZGROUP;

				if (pagination) {
					sql = sql.concat(QuizgroupModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Quizgroup>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Quizgroup>(list);
				}
				else {
					list = (List<Quizgroup>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the quizgroups from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Quizgroup quizgroup : findAll()) {
			remove(quizgroup);
		}
	}

	/**
	 * Returns the number of quizgroups.
	 *
	 * @return the number of quizgroups
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_QUIZGROUP);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns all the quizs associated with the quizgroup.
	 *
	 * @param pk the primary key of the quizgroup
	 * @return the quizs associated with the quizgroup
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<kahoot_source.model.Quiz> getQuizs(long pk)
		throws SystemException {
		return getQuizs(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the quizs associated with the quizgroup.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the quizgroup
	 * @param start the lower bound of the range of quizgroups
	 * @param end the upper bound of the range of quizgroups (not inclusive)
	 * @return the range of quizs associated with the quizgroup
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<kahoot_source.model.Quiz> getQuizs(long pk, int start, int end)
		throws SystemException {
		return getQuizs(pk, start, end, null);
	}

	/**
	 * Returns an ordered range of all the quizs associated with the quizgroup.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizgroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the quizgroup
	 * @param start the lower bound of the range of quizgroups
	 * @param end the upper bound of the range of quizgroups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of quizs associated with the quizgroup
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<kahoot_source.model.Quiz> getQuizs(long pk, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return quizgroupToQuizTableMapper.getRightBaseModels(pk, start, end,
			orderByComparator);
	}

	/**
	 * Returns the number of quizs associated with the quizgroup.
	 *
	 * @param pk the primary key of the quizgroup
	 * @return the number of quizs associated with the quizgroup
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int getQuizsSize(long pk) throws SystemException {
		long[] pks = quizgroupToQuizTableMapper.getRightPrimaryKeys(pk);

		return pks.length;
	}

	/**
	 * Returns <code>true</code> if the quiz is associated with the quizgroup.
	 *
	 * @param pk the primary key of the quizgroup
	 * @param quizPK the primary key of the quiz
	 * @return <code>true</code> if the quiz is associated with the quizgroup; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public boolean containsQuiz(long pk, long quizPK) throws SystemException {
		return quizgroupToQuizTableMapper.containsTableMapping(pk, quizPK);
	}

	/**
	 * Returns <code>true</code> if the quizgroup has any quizs associated with it.
	 *
	 * @param pk the primary key of the quizgroup to check for associations with quizs
	 * @return <code>true</code> if the quizgroup has any quizs associated with it; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public boolean containsQuizs(long pk) throws SystemException {
		if (getQuizsSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Adds an association between the quizgroup and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quizgroup
	 * @param quizPK the primary key of the quiz
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addQuiz(long pk, long quizPK) throws SystemException {
		quizgroupToQuizTableMapper.addTableMapping(pk, quizPK);
	}

	/**
	 * Adds an association between the quizgroup and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quizgroup
	 * @param quiz the quiz
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addQuiz(long pk, kahoot_source.model.Quiz quiz)
		throws SystemException {
		quizgroupToQuizTableMapper.addTableMapping(pk, quiz.getPrimaryKey());
	}

	/**
	 * Adds an association between the quizgroup and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quizgroup
	 * @param quizPKs the primary keys of the quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addQuizs(long pk, long[] quizPKs) throws SystemException {
		for (long quizPK : quizPKs) {
			quizgroupToQuizTableMapper.addTableMapping(pk, quizPK);
		}
	}

	/**
	 * Adds an association between the quizgroup and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quizgroup
	 * @param quizs the quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addQuizs(long pk, List<kahoot_source.model.Quiz> quizs)
		throws SystemException {
		for (kahoot_source.model.Quiz quiz : quizs) {
			quizgroupToQuizTableMapper.addTableMapping(pk, quiz.getPrimaryKey());
		}
	}

	/**
	 * Clears all associations between the quizgroup and its quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quizgroup to clear the associated quizs from
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void clearQuizs(long pk) throws SystemException {
		quizgroupToQuizTableMapper.deleteLeftPrimaryKeyTableMappings(pk);
	}

	/**
	 * Removes the association between the quizgroup and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quizgroup
	 * @param quizPK the primary key of the quiz
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeQuiz(long pk, long quizPK) throws SystemException {
		quizgroupToQuizTableMapper.deleteTableMapping(pk, quizPK);
	}

	/**
	 * Removes the association between the quizgroup and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quizgroup
	 * @param quiz the quiz
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeQuiz(long pk, kahoot_source.model.Quiz quiz)
		throws SystemException {
		quizgroupToQuizTableMapper.deleteTableMapping(pk, quiz.getPrimaryKey());
	}

	/**
	 * Removes the association between the quizgroup and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quizgroup
	 * @param quizPKs the primary keys of the quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeQuizs(long pk, long[] quizPKs) throws SystemException {
		for (long quizPK : quizPKs) {
			quizgroupToQuizTableMapper.deleteTableMapping(pk, quizPK);
		}
	}

	/**
	 * Removes the association between the quizgroup and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quizgroup
	 * @param quizs the quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeQuizs(long pk, List<kahoot_source.model.Quiz> quizs)
		throws SystemException {
		for (kahoot_source.model.Quiz quiz : quizs) {
			quizgroupToQuizTableMapper.deleteTableMapping(pk,
				quiz.getPrimaryKey());
		}
	}

	/**
	 * Sets the quizs associated with the quizgroup, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quizgroup
	 * @param quizPKs the primary keys of the quizs to be associated with the quizgroup
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void setQuizs(long pk, long[] quizPKs) throws SystemException {
		Set<Long> newQuizPKsSet = SetUtil.fromArray(quizPKs);
		Set<Long> oldQuizPKsSet = SetUtil.fromArray(quizgroupToQuizTableMapper.getRightPrimaryKeys(
					pk));

		Set<Long> removeQuizPKsSet = new HashSet<Long>(oldQuizPKsSet);

		removeQuizPKsSet.removeAll(newQuizPKsSet);

		for (long removeQuizPK : removeQuizPKsSet) {
			quizgroupToQuizTableMapper.deleteTableMapping(pk, removeQuizPK);
		}

		newQuizPKsSet.removeAll(oldQuizPKsSet);

		for (long newQuizPK : newQuizPKsSet) {
			quizgroupToQuizTableMapper.addTableMapping(pk, newQuizPK);
		}
	}

	/**
	 * Sets the quizs associated with the quizgroup, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quizgroup
	 * @param quizs the quizs to be associated with the quizgroup
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void setQuizs(long pk, List<kahoot_source.model.Quiz> quizs)
		throws SystemException {
		try {
			long[] quizPKs = new long[quizs.size()];

			for (int i = 0; i < quizs.size(); i++) {
				kahoot_source.model.Quiz quiz = quizs.get(i);

				quizPKs[i] = quiz.getPrimaryKey();
			}

			setQuizs(pk, quizPKs);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(QuizgroupModelImpl.MAPPING_TABLE_KAHOOT_QUIZ_QUIZGROUP_NAME);
		}
	}

	/**
	 * Initializes the quizgroup persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.kahoot_source.model.Quizgroup")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Quizgroup>> listenersList = new ArrayList<ModelListener<Quizgroup>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Quizgroup>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}

		quizgroupToQuizTableMapper = TableMapperFactory.getTableMapper("kahoot_Quiz_Quizgroup",
				"quizgroupid", "quizid", this, quizPersistence);
	}

	public void destroy() {
		EntityCacheUtil.removeCache(QuizgroupImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		TableMapperFactory.removeTableMapper("kahoot_Quiz_Quizgroup");
	}

	@BeanReference(type = QuizPersistence.class)
	protected QuizPersistence quizPersistence;
	protected TableMapper<Quizgroup, kahoot_source.model.Quiz> quizgroupToQuizTableMapper;
	private static final String _SQL_SELECT_QUIZGROUP = "SELECT quizgroup FROM Quizgroup quizgroup";
	private static final String _SQL_SELECT_QUIZGROUP_WHERE = "SELECT quizgroup FROM Quizgroup quizgroup WHERE ";
	private static final String _SQL_COUNT_QUIZGROUP = "SELECT COUNT(quizgroup) FROM Quizgroup quizgroup";
	private static final String _SQL_COUNT_QUIZGROUP_WHERE = "SELECT COUNT(quizgroup) FROM Quizgroup quizgroup WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "quizgroup.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Quizgroup exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Quizgroup exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(QuizgroupPersistenceImpl.class);
	private static Quizgroup _nullQuizgroup = new QuizgroupImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Quizgroup> toCacheModel() {
				return _nullQuizgroupCacheModel;
			}
		};

	private static CacheModel<Quizgroup> _nullQuizgroupCacheModel = new CacheModel<Quizgroup>() {
			@Override
			public Quizgroup toEntityModel() {
				return _nullQuizgroup;
			}
		};
}