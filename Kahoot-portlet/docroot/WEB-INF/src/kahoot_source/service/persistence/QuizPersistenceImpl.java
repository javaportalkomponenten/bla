/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service.persistence;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.service.persistence.impl.TableMapper;
import com.liferay.portal.service.persistence.impl.TableMapperFactory;

import kahoot_source.NoSuchQuizException;

import kahoot_source.model.Quiz;

import kahoot_source.model.impl.QuizImpl;
import kahoot_source.model.impl.QuizModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the quiz service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author krzysiek
 * @see QuizPersistence
 * @see QuizUtil
 * @generated
 */
public class QuizPersistenceImpl extends BasePersistenceImpl<Quiz>
	implements QuizPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link QuizUtil} to access the quiz persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = QuizImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(QuizModelImpl.ENTITY_CACHE_ENABLED,
			QuizModelImpl.FINDER_CACHE_ENABLED, QuizImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(QuizModelImpl.ENTITY_CACHE_ENABLED,
			QuizModelImpl.FINDER_CACHE_ENABLED, QuizImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(QuizModelImpl.ENTITY_CACHE_ENABLED,
			QuizModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_QUIZID = new FinderPath(QuizModelImpl.ENTITY_CACHE_ENABLED,
			QuizModelImpl.FINDER_CACHE_ENABLED, QuizImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByquizid",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUIZID =
		new FinderPath(QuizModelImpl.ENTITY_CACHE_ENABLED,
			QuizModelImpl.FINDER_CACHE_ENABLED, QuizImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByquizid",
			new String[] { Long.class.getName() },
			QuizModelImpl.QUIZID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_QUIZID = new FinderPath(QuizModelImpl.ENTITY_CACHE_ENABLED,
			QuizModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByquizid",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the quizs where quizid = &#63;.
	 *
	 * @param quizid the quizid
	 * @return the matching quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quiz> findByquizid(long quizid) throws SystemException {
		return findByquizid(quizid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the quizs where quizid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param quizid the quizid
	 * @param start the lower bound of the range of quizs
	 * @param end the upper bound of the range of quizs (not inclusive)
	 * @return the range of matching quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quiz> findByquizid(long quizid, int start, int end)
		throws SystemException {
		return findByquizid(quizid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the quizs where quizid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param quizid the quizid
	 * @param start the lower bound of the range of quizs
	 * @param end the upper bound of the range of quizs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quiz> findByquizid(long quizid, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUIZID;
			finderArgs = new Object[] { quizid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_QUIZID;
			finderArgs = new Object[] { quizid, start, end, orderByComparator };
		}

		List<Quiz> list = (List<Quiz>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Quiz quiz : list) {
				if ((quizid != quiz.getQuizid())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_QUIZ_WHERE);

			query.append(_FINDER_COLUMN_QUIZID_QUIZID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(QuizModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(quizid);

				if (!pagination) {
					list = (List<Quiz>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Quiz>(list);
				}
				else {
					list = (List<Quiz>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first quiz in the ordered set where quizid = &#63;.
	 *
	 * @param quizid the quizid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching quiz
	 * @throws kahoot_source.NoSuchQuizException if a matching quiz could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quiz findByquizid_First(long quizid,
		OrderByComparator orderByComparator)
		throws NoSuchQuizException, SystemException {
		Quiz quiz = fetchByquizid_First(quizid, orderByComparator);

		if (quiz != null) {
			return quiz;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("quizid=");
		msg.append(quizid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuizException(msg.toString());
	}

	/**
	 * Returns the first quiz in the ordered set where quizid = &#63;.
	 *
	 * @param quizid the quizid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching quiz, or <code>null</code> if a matching quiz could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quiz fetchByquizid_First(long quizid,
		OrderByComparator orderByComparator) throws SystemException {
		List<Quiz> list = findByquizid(quizid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last quiz in the ordered set where quizid = &#63;.
	 *
	 * @param quizid the quizid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching quiz
	 * @throws kahoot_source.NoSuchQuizException if a matching quiz could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quiz findByquizid_Last(long quizid,
		OrderByComparator orderByComparator)
		throws NoSuchQuizException, SystemException {
		Quiz quiz = fetchByquizid_Last(quizid, orderByComparator);

		if (quiz != null) {
			return quiz;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("quizid=");
		msg.append(quizid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuizException(msg.toString());
	}

	/**
	 * Returns the last quiz in the ordered set where quizid = &#63;.
	 *
	 * @param quizid the quizid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching quiz, or <code>null</code> if a matching quiz could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quiz fetchByquizid_Last(long quizid,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByquizid(quizid);

		if (count == 0) {
			return null;
		}

		List<Quiz> list = findByquizid(quizid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Removes all the quizs where quizid = &#63; from the database.
	 *
	 * @param quizid the quizid
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByquizid(long quizid) throws SystemException {
		for (Quiz quiz : findByquizid(quizid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(quiz);
		}
	}

	/**
	 * Returns the number of quizs where quizid = &#63;.
	 *
	 * @param quizid the quizid
	 * @return the number of matching quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByquizid(long quizid) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_QUIZID;

		Object[] finderArgs = new Object[] { quizid };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_QUIZ_WHERE);

			query.append(_FINDER_COLUMN_QUIZID_QUIZID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(quizid);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_QUIZID_QUIZID_2 = "quiz.quizid = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TITLE = new FinderPath(QuizModelImpl.ENTITY_CACHE_ENABLED,
			QuizModelImpl.FINDER_CACHE_ENABLED, QuizImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBytitle",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITLE = new FinderPath(QuizModelImpl.ENTITY_CACHE_ENABLED,
			QuizModelImpl.FINDER_CACHE_ENABLED, QuizImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBytitle",
			new String[] { String.class.getName() },
			QuizModelImpl.TITLE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_TITLE = new FinderPath(QuizModelImpl.ENTITY_CACHE_ENABLED,
			QuizModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBytitle",
			new String[] { String.class.getName() });

	/**
	 * Returns all the quizs where title = &#63;.
	 *
	 * @param title the title
	 * @return the matching quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quiz> findBytitle(String title) throws SystemException {
		return findBytitle(title, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the quizs where title = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param title the title
	 * @param start the lower bound of the range of quizs
	 * @param end the upper bound of the range of quizs (not inclusive)
	 * @return the range of matching quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quiz> findBytitle(String title, int start, int end)
		throws SystemException {
		return findBytitle(title, start, end, null);
	}

	/**
	 * Returns an ordered range of all the quizs where title = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param title the title
	 * @param start the lower bound of the range of quizs
	 * @param end the upper bound of the range of quizs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quiz> findBytitle(String title, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITLE;
			finderArgs = new Object[] { title };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TITLE;
			finderArgs = new Object[] { title, start, end, orderByComparator };
		}

		List<Quiz> list = (List<Quiz>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Quiz quiz : list) {
				if (!Validator.equals(title, quiz.getTitle())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_QUIZ_WHERE);

			boolean bindTitle = false;

			if (title == null) {
				query.append(_FINDER_COLUMN_TITLE_TITLE_1);
			}
			else if (title.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TITLE_TITLE_3);
			}
			else {
				bindTitle = true;

				query.append(_FINDER_COLUMN_TITLE_TITLE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(QuizModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindTitle) {
					qPos.add(title);
				}

				if (!pagination) {
					list = (List<Quiz>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Quiz>(list);
				}
				else {
					list = (List<Quiz>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first quiz in the ordered set where title = &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching quiz
	 * @throws kahoot_source.NoSuchQuizException if a matching quiz could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quiz findBytitle_First(String title,
		OrderByComparator orderByComparator)
		throws NoSuchQuizException, SystemException {
		Quiz quiz = fetchBytitle_First(title, orderByComparator);

		if (quiz != null) {
			return quiz;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("title=");
		msg.append(title);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuizException(msg.toString());
	}

	/**
	 * Returns the first quiz in the ordered set where title = &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching quiz, or <code>null</code> if a matching quiz could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quiz fetchBytitle_First(String title,
		OrderByComparator orderByComparator) throws SystemException {
		List<Quiz> list = findBytitle(title, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last quiz in the ordered set where title = &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching quiz
	 * @throws kahoot_source.NoSuchQuizException if a matching quiz could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quiz findBytitle_Last(String title,
		OrderByComparator orderByComparator)
		throws NoSuchQuizException, SystemException {
		Quiz quiz = fetchBytitle_Last(title, orderByComparator);

		if (quiz != null) {
			return quiz;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("title=");
		msg.append(title);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuizException(msg.toString());
	}

	/**
	 * Returns the last quiz in the ordered set where title = &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching quiz, or <code>null</code> if a matching quiz could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quiz fetchBytitle_Last(String title,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBytitle(title);

		if (count == 0) {
			return null;
		}

		List<Quiz> list = findBytitle(title, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the quizs before and after the current quiz in the ordered set where title = &#63;.
	 *
	 * @param quizid the primary key of the current quiz
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next quiz
	 * @throws kahoot_source.NoSuchQuizException if a quiz with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quiz[] findBytitle_PrevAndNext(long quizid, String title,
		OrderByComparator orderByComparator)
		throws NoSuchQuizException, SystemException {
		Quiz quiz = findByPrimaryKey(quizid);

		Session session = null;

		try {
			session = openSession();

			Quiz[] array = new QuizImpl[3];

			array[0] = getBytitle_PrevAndNext(session, quiz, title,
					orderByComparator, true);

			array[1] = quiz;

			array[2] = getBytitle_PrevAndNext(session, quiz, title,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Quiz getBytitle_PrevAndNext(Session session, Quiz quiz,
		String title, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_QUIZ_WHERE);

		boolean bindTitle = false;

		if (title == null) {
			query.append(_FINDER_COLUMN_TITLE_TITLE_1);
		}
		else if (title.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_TITLE_TITLE_3);
		}
		else {
			bindTitle = true;

			query.append(_FINDER_COLUMN_TITLE_TITLE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(QuizModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindTitle) {
			qPos.add(title);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(quiz);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Quiz> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the quizs where title = &#63; from the database.
	 *
	 * @param title the title
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBytitle(String title) throws SystemException {
		for (Quiz quiz : findBytitle(title, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(quiz);
		}
	}

	/**
	 * Returns the number of quizs where title = &#63;.
	 *
	 * @param title the title
	 * @return the number of matching quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBytitle(String title) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_TITLE;

		Object[] finderArgs = new Object[] { title };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_QUIZ_WHERE);

			boolean bindTitle = false;

			if (title == null) {
				query.append(_FINDER_COLUMN_TITLE_TITLE_1);
			}
			else if (title.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TITLE_TITLE_3);
			}
			else {
				bindTitle = true;

				query.append(_FINDER_COLUMN_TITLE_TITLE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindTitle) {
					qPos.add(title);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_TITLE_TITLE_1 = "quiz.title IS NULL";
	private static final String _FINDER_COLUMN_TITLE_TITLE_2 = "quiz.title = ?";
	private static final String _FINDER_COLUMN_TITLE_TITLE_3 = "(quiz.title IS NULL OR quiz.title = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_RUNNING = new FinderPath(QuizModelImpl.ENTITY_CACHE_ENABLED,
			QuizModelImpl.FINDER_CACHE_ENABLED, QuizImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByrunning",
			new String[] {
				Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RUNNING =
		new FinderPath(QuizModelImpl.ENTITY_CACHE_ENABLED,
			QuizModelImpl.FINDER_CACHE_ENABLED, QuizImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByrunning",
			new String[] { Boolean.class.getName() },
			QuizModelImpl.RUNNING_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_RUNNING = new FinderPath(QuizModelImpl.ENTITY_CACHE_ENABLED,
			QuizModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByrunning",
			new String[] { Boolean.class.getName() });

	/**
	 * Returns all the quizs where running = &#63;.
	 *
	 * @param running the running
	 * @return the matching quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quiz> findByrunning(boolean running) throws SystemException {
		return findByrunning(running, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the quizs where running = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param running the running
	 * @param start the lower bound of the range of quizs
	 * @param end the upper bound of the range of quizs (not inclusive)
	 * @return the range of matching quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quiz> findByrunning(boolean running, int start, int end)
		throws SystemException {
		return findByrunning(running, start, end, null);
	}

	/**
	 * Returns an ordered range of all the quizs where running = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param running the running
	 * @param start the lower bound of the range of quizs
	 * @param end the upper bound of the range of quizs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quiz> findByrunning(boolean running, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RUNNING;
			finderArgs = new Object[] { running };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_RUNNING;
			finderArgs = new Object[] { running, start, end, orderByComparator };
		}

		List<Quiz> list = (List<Quiz>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Quiz quiz : list) {
				if ((running != quiz.getRunning())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_QUIZ_WHERE);

			query.append(_FINDER_COLUMN_RUNNING_RUNNING_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(QuizModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(running);

				if (!pagination) {
					list = (List<Quiz>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Quiz>(list);
				}
				else {
					list = (List<Quiz>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first quiz in the ordered set where running = &#63;.
	 *
	 * @param running the running
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching quiz
	 * @throws kahoot_source.NoSuchQuizException if a matching quiz could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quiz findByrunning_First(boolean running,
		OrderByComparator orderByComparator)
		throws NoSuchQuizException, SystemException {
		Quiz quiz = fetchByrunning_First(running, orderByComparator);

		if (quiz != null) {
			return quiz;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("running=");
		msg.append(running);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuizException(msg.toString());
	}

	/**
	 * Returns the first quiz in the ordered set where running = &#63;.
	 *
	 * @param running the running
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching quiz, or <code>null</code> if a matching quiz could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quiz fetchByrunning_First(boolean running,
		OrderByComparator orderByComparator) throws SystemException {
		List<Quiz> list = findByrunning(running, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last quiz in the ordered set where running = &#63;.
	 *
	 * @param running the running
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching quiz
	 * @throws kahoot_source.NoSuchQuizException if a matching quiz could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quiz findByrunning_Last(boolean running,
		OrderByComparator orderByComparator)
		throws NoSuchQuizException, SystemException {
		Quiz quiz = fetchByrunning_Last(running, orderByComparator);

		if (quiz != null) {
			return quiz;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("running=");
		msg.append(running);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuizException(msg.toString());
	}

	/**
	 * Returns the last quiz in the ordered set where running = &#63;.
	 *
	 * @param running the running
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching quiz, or <code>null</code> if a matching quiz could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quiz fetchByrunning_Last(boolean running,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByrunning(running);

		if (count == 0) {
			return null;
		}

		List<Quiz> list = findByrunning(running, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the quizs before and after the current quiz in the ordered set where running = &#63;.
	 *
	 * @param quizid the primary key of the current quiz
	 * @param running the running
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next quiz
	 * @throws kahoot_source.NoSuchQuizException if a quiz with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quiz[] findByrunning_PrevAndNext(long quizid, boolean running,
		OrderByComparator orderByComparator)
		throws NoSuchQuizException, SystemException {
		Quiz quiz = findByPrimaryKey(quizid);

		Session session = null;

		try {
			session = openSession();

			Quiz[] array = new QuizImpl[3];

			array[0] = getByrunning_PrevAndNext(session, quiz, running,
					orderByComparator, true);

			array[1] = quiz;

			array[2] = getByrunning_PrevAndNext(session, quiz, running,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Quiz getByrunning_PrevAndNext(Session session, Quiz quiz,
		boolean running, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_QUIZ_WHERE);

		query.append(_FINDER_COLUMN_RUNNING_RUNNING_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(QuizModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(running);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(quiz);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Quiz> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the quizs where running = &#63; from the database.
	 *
	 * @param running the running
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByrunning(boolean running) throws SystemException {
		for (Quiz quiz : findByrunning(running, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(quiz);
		}
	}

	/**
	 * Returns the number of quizs where running = &#63;.
	 *
	 * @param running the running
	 * @return the number of matching quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByrunning(boolean running) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_RUNNING;

		Object[] finderArgs = new Object[] { running };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_QUIZ_WHERE);

			query.append(_FINDER_COLUMN_RUNNING_RUNNING_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(running);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_RUNNING_RUNNING_2 = "quiz.running = ?";

	public QuizPersistenceImpl() {
		setModelClass(Quiz.class);
	}

	/**
	 * Caches the quiz in the entity cache if it is enabled.
	 *
	 * @param quiz the quiz
	 */
	@Override
	public void cacheResult(Quiz quiz) {
		EntityCacheUtil.putResult(QuizModelImpl.ENTITY_CACHE_ENABLED,
			QuizImpl.class, quiz.getPrimaryKey(), quiz);

		quiz.resetOriginalValues();
	}

	/**
	 * Caches the quizs in the entity cache if it is enabled.
	 *
	 * @param quizs the quizs
	 */
	@Override
	public void cacheResult(List<Quiz> quizs) {
		for (Quiz quiz : quizs) {
			if (EntityCacheUtil.getResult(QuizModelImpl.ENTITY_CACHE_ENABLED,
						QuizImpl.class, quiz.getPrimaryKey()) == null) {
				cacheResult(quiz);
			}
			else {
				quiz.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all quizs.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(QuizImpl.class.getName());
		}

		EntityCacheUtil.clearCache(QuizImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the quiz.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Quiz quiz) {
		EntityCacheUtil.removeResult(QuizModelImpl.ENTITY_CACHE_ENABLED,
			QuizImpl.class, quiz.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Quiz> quizs) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Quiz quiz : quizs) {
			EntityCacheUtil.removeResult(QuizModelImpl.ENTITY_CACHE_ENABLED,
				QuizImpl.class, quiz.getPrimaryKey());
		}
	}

	/**
	 * Creates a new quiz with the primary key. Does not add the quiz to the database.
	 *
	 * @param quizid the primary key for the new quiz
	 * @return the new quiz
	 */
	@Override
	public Quiz create(long quizid) {
		Quiz quiz = new QuizImpl();

		quiz.setNew(true);
		quiz.setPrimaryKey(quizid);

		return quiz;
	}

	/**
	 * Removes the quiz with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param quizid the primary key of the quiz
	 * @return the quiz that was removed
	 * @throws kahoot_source.NoSuchQuizException if a quiz with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quiz remove(long quizid) throws NoSuchQuizException, SystemException {
		return remove((Serializable)quizid);
	}

	/**
	 * Removes the quiz with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the quiz
	 * @return the quiz that was removed
	 * @throws kahoot_source.NoSuchQuizException if a quiz with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quiz remove(Serializable primaryKey)
		throws NoSuchQuizException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Quiz quiz = (Quiz)session.get(QuizImpl.class, primaryKey);

			if (quiz == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchQuizException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(quiz);
		}
		catch (NoSuchQuizException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Quiz removeImpl(Quiz quiz) throws SystemException {
		quiz = toUnwrappedModel(quiz);

		quizToQuizgroupTableMapper.deleteLeftPrimaryKeyTableMappings(quiz.getPrimaryKey());

		quizToQuestionTableMapper.deleteLeftPrimaryKeyTableMappings(quiz.getPrimaryKey());

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(quiz)) {
				quiz = (Quiz)session.get(QuizImpl.class, quiz.getPrimaryKeyObj());
			}

			if (quiz != null) {
				session.delete(quiz);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (quiz != null) {
			clearCache(quiz);
		}

		return quiz;
	}

	@Override
	public Quiz updateImpl(kahoot_source.model.Quiz quiz)
		throws SystemException {
		quiz = toUnwrappedModel(quiz);

		boolean isNew = quiz.isNew();

		QuizModelImpl quizModelImpl = (QuizModelImpl)quiz;

		Session session = null;

		try {
			session = openSession();

			if (quiz.isNew()) {
				session.save(quiz);

				quiz.setNew(false);
			}
			else {
				session.merge(quiz);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !QuizModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((quizModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUIZID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { quizModelImpl.getOriginalQuizid() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUIZID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUIZID,
					args);

				args = new Object[] { quizModelImpl.getQuizid() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUIZID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUIZID,
					args);
			}

			if ((quizModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITLE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { quizModelImpl.getOriginalTitle() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TITLE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITLE,
					args);

				args = new Object[] { quizModelImpl.getTitle() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TITLE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITLE,
					args);
			}

			if ((quizModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RUNNING.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { quizModelImpl.getOriginalRunning() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_RUNNING, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RUNNING,
					args);

				args = new Object[] { quizModelImpl.getRunning() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_RUNNING, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RUNNING,
					args);
			}
		}

		EntityCacheUtil.putResult(QuizModelImpl.ENTITY_CACHE_ENABLED,
			QuizImpl.class, quiz.getPrimaryKey(), quiz);

		return quiz;
	}

	protected Quiz toUnwrappedModel(Quiz quiz) {
		if (quiz instanceof QuizImpl) {
			return quiz;
		}

		QuizImpl quizImpl = new QuizImpl();

		quizImpl.setNew(quiz.isNew());
		quizImpl.setPrimaryKey(quiz.getPrimaryKey());

		quizImpl.setQuizid(quiz.getQuizid());
		quizImpl.setTitle(quiz.getTitle());
		quizImpl.setTimestamp(quiz.getTimestamp());
		quizImpl.setRunning(quiz.isRunning());

		return quizImpl;
	}

	/**
	 * Returns the quiz with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the quiz
	 * @return the quiz
	 * @throws kahoot_source.NoSuchQuizException if a quiz with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quiz findByPrimaryKey(Serializable primaryKey)
		throws NoSuchQuizException, SystemException {
		Quiz quiz = fetchByPrimaryKey(primaryKey);

		if (quiz == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchQuizException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return quiz;
	}

	/**
	 * Returns the quiz with the primary key or throws a {@link kahoot_source.NoSuchQuizException} if it could not be found.
	 *
	 * @param quizid the primary key of the quiz
	 * @return the quiz
	 * @throws kahoot_source.NoSuchQuizException if a quiz with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quiz findByPrimaryKey(long quizid)
		throws NoSuchQuizException, SystemException {
		return findByPrimaryKey((Serializable)quizid);
	}

	/**
	 * Returns the quiz with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the quiz
	 * @return the quiz, or <code>null</code> if a quiz with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quiz fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Quiz quiz = (Quiz)EntityCacheUtil.getResult(QuizModelImpl.ENTITY_CACHE_ENABLED,
				QuizImpl.class, primaryKey);

		if (quiz == _nullQuiz) {
			return null;
		}

		if (quiz == null) {
			Session session = null;

			try {
				session = openSession();

				quiz = (Quiz)session.get(QuizImpl.class, primaryKey);

				if (quiz != null) {
					cacheResult(quiz);
				}
				else {
					EntityCacheUtil.putResult(QuizModelImpl.ENTITY_CACHE_ENABLED,
						QuizImpl.class, primaryKey, _nullQuiz);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(QuizModelImpl.ENTITY_CACHE_ENABLED,
					QuizImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return quiz;
	}

	/**
	 * Returns the quiz with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param quizid the primary key of the quiz
	 * @return the quiz, or <code>null</code> if a quiz with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Quiz fetchByPrimaryKey(long quizid) throws SystemException {
		return fetchByPrimaryKey((Serializable)quizid);
	}

	/**
	 * Returns all the quizs.
	 *
	 * @return the quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quiz> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the quizs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of quizs
	 * @param end the upper bound of the range of quizs (not inclusive)
	 * @return the range of quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quiz> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the quizs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of quizs
	 * @param end the upper bound of the range of quizs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Quiz> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Quiz> list = (List<Quiz>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_QUIZ);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_QUIZ;

				if (pagination) {
					sql = sql.concat(QuizModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Quiz>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Quiz>(list);
				}
				else {
					list = (List<Quiz>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the quizs from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Quiz quiz : findAll()) {
			remove(quiz);
		}
	}

	/**
	 * Returns the number of quizs.
	 *
	 * @return the number of quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_QUIZ);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns all the quizgroups associated with the quiz.
	 *
	 * @param pk the primary key of the quiz
	 * @return the quizgroups associated with the quiz
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<kahoot_source.model.Quizgroup> getQuizgroups(long pk)
		throws SystemException {
		return getQuizgroups(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the quizgroups associated with the quiz.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the quiz
	 * @param start the lower bound of the range of quizs
	 * @param end the upper bound of the range of quizs (not inclusive)
	 * @return the range of quizgroups associated with the quiz
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<kahoot_source.model.Quizgroup> getQuizgroups(long pk,
		int start, int end) throws SystemException {
		return getQuizgroups(pk, start, end, null);
	}

	/**
	 * Returns an ordered range of all the quizgroups associated with the quiz.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the quiz
	 * @param start the lower bound of the range of quizs
	 * @param end the upper bound of the range of quizs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of quizgroups associated with the quiz
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<kahoot_source.model.Quizgroup> getQuizgroups(long pk,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		return quizToQuizgroupTableMapper.getRightBaseModels(pk, start, end,
			orderByComparator);
	}

	/**
	 * Returns the number of quizgroups associated with the quiz.
	 *
	 * @param pk the primary key of the quiz
	 * @return the number of quizgroups associated with the quiz
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int getQuizgroupsSize(long pk) throws SystemException {
		long[] pks = quizToQuizgroupTableMapper.getRightPrimaryKeys(pk);

		return pks.length;
	}

	/**
	 * Returns <code>true</code> if the quizgroup is associated with the quiz.
	 *
	 * @param pk the primary key of the quiz
	 * @param quizgroupPK the primary key of the quizgroup
	 * @return <code>true</code> if the quizgroup is associated with the quiz; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public boolean containsQuizgroup(long pk, long quizgroupPK)
		throws SystemException {
		return quizToQuizgroupTableMapper.containsTableMapping(pk, quizgroupPK);
	}

	/**
	 * Returns <code>true</code> if the quiz has any quizgroups associated with it.
	 *
	 * @param pk the primary key of the quiz to check for associations with quizgroups
	 * @return <code>true</code> if the quiz has any quizgroups associated with it; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public boolean containsQuizgroups(long pk) throws SystemException {
		if (getQuizgroupsSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Adds an association between the quiz and the quizgroup. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz
	 * @param quizgroupPK the primary key of the quizgroup
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addQuizgroup(long pk, long quizgroupPK)
		throws SystemException {
		quizToQuizgroupTableMapper.addTableMapping(pk, quizgroupPK);
	}

	/**
	 * Adds an association between the quiz and the quizgroup. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz
	 * @param quizgroup the quizgroup
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addQuizgroup(long pk, kahoot_source.model.Quizgroup quizgroup)
		throws SystemException {
		quizToQuizgroupTableMapper.addTableMapping(pk, quizgroup.getPrimaryKey());
	}

	/**
	 * Adds an association between the quiz and the quizgroups. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz
	 * @param quizgroupPKs the primary keys of the quizgroups
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addQuizgroups(long pk, long[] quizgroupPKs)
		throws SystemException {
		for (long quizgroupPK : quizgroupPKs) {
			quizToQuizgroupTableMapper.addTableMapping(pk, quizgroupPK);
		}
	}

	/**
	 * Adds an association between the quiz and the quizgroups. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz
	 * @param quizgroups the quizgroups
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addQuizgroups(long pk,
		List<kahoot_source.model.Quizgroup> quizgroups)
		throws SystemException {
		for (kahoot_source.model.Quizgroup quizgroup : quizgroups) {
			quizToQuizgroupTableMapper.addTableMapping(pk,
				quizgroup.getPrimaryKey());
		}
	}

	/**
	 * Clears all associations between the quiz and its quizgroups. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz to clear the associated quizgroups from
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void clearQuizgroups(long pk) throws SystemException {
		quizToQuizgroupTableMapper.deleteLeftPrimaryKeyTableMappings(pk);
	}

	/**
	 * Removes the association between the quiz and the quizgroup. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz
	 * @param quizgroupPK the primary key of the quizgroup
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeQuizgroup(long pk, long quizgroupPK)
		throws SystemException {
		quizToQuizgroupTableMapper.deleteTableMapping(pk, quizgroupPK);
	}

	/**
	 * Removes the association between the quiz and the quizgroup. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz
	 * @param quizgroup the quizgroup
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeQuizgroup(long pk, kahoot_source.model.Quizgroup quizgroup)
		throws SystemException {
		quizToQuizgroupTableMapper.deleteTableMapping(pk,
			quizgroup.getPrimaryKey());
	}

	/**
	 * Removes the association between the quiz and the quizgroups. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz
	 * @param quizgroupPKs the primary keys of the quizgroups
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeQuizgroups(long pk, long[] quizgroupPKs)
		throws SystemException {
		for (long quizgroupPK : quizgroupPKs) {
			quizToQuizgroupTableMapper.deleteTableMapping(pk, quizgroupPK);
		}
	}

	/**
	 * Removes the association between the quiz and the quizgroups. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz
	 * @param quizgroups the quizgroups
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeQuizgroups(long pk,
		List<kahoot_source.model.Quizgroup> quizgroups)
		throws SystemException {
		for (kahoot_source.model.Quizgroup quizgroup : quizgroups) {
			quizToQuizgroupTableMapper.deleteTableMapping(pk,
				quizgroup.getPrimaryKey());
		}
	}

	/**
	 * Sets the quizgroups associated with the quiz, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz
	 * @param quizgroupPKs the primary keys of the quizgroups to be associated with the quiz
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void setQuizgroups(long pk, long[] quizgroupPKs)
		throws SystemException {
		Set<Long> newQuizgroupPKsSet = SetUtil.fromArray(quizgroupPKs);
		Set<Long> oldQuizgroupPKsSet = SetUtil.fromArray(quizToQuizgroupTableMapper.getRightPrimaryKeys(
					pk));

		Set<Long> removeQuizgroupPKsSet = new HashSet<Long>(oldQuizgroupPKsSet);

		removeQuizgroupPKsSet.removeAll(newQuizgroupPKsSet);

		for (long removeQuizgroupPK : removeQuizgroupPKsSet) {
			quizToQuizgroupTableMapper.deleteTableMapping(pk, removeQuizgroupPK);
		}

		newQuizgroupPKsSet.removeAll(oldQuizgroupPKsSet);

		for (long newQuizgroupPK : newQuizgroupPKsSet) {
			quizToQuizgroupTableMapper.addTableMapping(pk, newQuizgroupPK);
		}
	}

	/**
	 * Sets the quizgroups associated with the quiz, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz
	 * @param quizgroups the quizgroups to be associated with the quiz
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void setQuizgroups(long pk,
		List<kahoot_source.model.Quizgroup> quizgroups)
		throws SystemException {
		try {
			long[] quizgroupPKs = new long[quizgroups.size()];

			for (int i = 0; i < quizgroups.size(); i++) {
				kahoot_source.model.Quizgroup quizgroup = quizgroups.get(i);

				quizgroupPKs[i] = quizgroup.getPrimaryKey();
			}

			setQuizgroups(pk, quizgroupPKs);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(QuizModelImpl.MAPPING_TABLE_KAHOOT_QUIZ_QUIZGROUP_NAME);
		}
	}

	/**
	 * Returns all the questions associated with the quiz.
	 *
	 * @param pk the primary key of the quiz
	 * @return the questions associated with the quiz
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<kahoot_source.model.Question> getQuestions(long pk)
		throws SystemException {
		return getQuestions(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the questions associated with the quiz.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the quiz
	 * @param start the lower bound of the range of quizs
	 * @param end the upper bound of the range of quizs (not inclusive)
	 * @return the range of questions associated with the quiz
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<kahoot_source.model.Question> getQuestions(long pk, int start,
		int end) throws SystemException {
		return getQuestions(pk, start, end, null);
	}

	/**
	 * Returns an ordered range of all the questions associated with the quiz.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the quiz
	 * @param start the lower bound of the range of quizs
	 * @param end the upper bound of the range of quizs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of questions associated with the quiz
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<kahoot_source.model.Question> getQuestions(long pk, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		return quizToQuestionTableMapper.getRightBaseModels(pk, start, end,
			orderByComparator);
	}

	/**
	 * Returns the number of questions associated with the quiz.
	 *
	 * @param pk the primary key of the quiz
	 * @return the number of questions associated with the quiz
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int getQuestionsSize(long pk) throws SystemException {
		long[] pks = quizToQuestionTableMapper.getRightPrimaryKeys(pk);

		return pks.length;
	}

	/**
	 * Returns <code>true</code> if the question is associated with the quiz.
	 *
	 * @param pk the primary key of the quiz
	 * @param questionPK the primary key of the question
	 * @return <code>true</code> if the question is associated with the quiz; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public boolean containsQuestion(long pk, long questionPK)
		throws SystemException {
		return quizToQuestionTableMapper.containsTableMapping(pk, questionPK);
	}

	/**
	 * Returns <code>true</code> if the quiz has any questions associated with it.
	 *
	 * @param pk the primary key of the quiz to check for associations with questions
	 * @return <code>true</code> if the quiz has any questions associated with it; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public boolean containsQuestions(long pk) throws SystemException {
		if (getQuestionsSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Adds an association between the quiz and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz
	 * @param questionPK the primary key of the question
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addQuestion(long pk, long questionPK) throws SystemException {
		quizToQuestionTableMapper.addTableMapping(pk, questionPK);
	}

	/**
	 * Adds an association between the quiz and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz
	 * @param question the question
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addQuestion(long pk, kahoot_source.model.Question question)
		throws SystemException {
		quizToQuestionTableMapper.addTableMapping(pk, question.getPrimaryKey());
	}

	/**
	 * Adds an association between the quiz and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz
	 * @param questionPKs the primary keys of the questions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addQuestions(long pk, long[] questionPKs)
		throws SystemException {
		for (long questionPK : questionPKs) {
			quizToQuestionTableMapper.addTableMapping(pk, questionPK);
		}
	}

	/**
	 * Adds an association between the quiz and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz
	 * @param questions the questions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addQuestions(long pk,
		List<kahoot_source.model.Question> questions) throws SystemException {
		for (kahoot_source.model.Question question : questions) {
			quizToQuestionTableMapper.addTableMapping(pk,
				question.getPrimaryKey());
		}
	}

	/**
	 * Clears all associations between the quiz and its questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz to clear the associated questions from
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void clearQuestions(long pk) throws SystemException {
		quizToQuestionTableMapper.deleteLeftPrimaryKeyTableMappings(pk);
	}

	/**
	 * Removes the association between the quiz and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz
	 * @param questionPK the primary key of the question
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeQuestion(long pk, long questionPK)
		throws SystemException {
		quizToQuestionTableMapper.deleteTableMapping(pk, questionPK);
	}

	/**
	 * Removes the association between the quiz and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz
	 * @param question the question
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeQuestion(long pk, kahoot_source.model.Question question)
		throws SystemException {
		quizToQuestionTableMapper.deleteTableMapping(pk,
			question.getPrimaryKey());
	}

	/**
	 * Removes the association between the quiz and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz
	 * @param questionPKs the primary keys of the questions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeQuestions(long pk, long[] questionPKs)
		throws SystemException {
		for (long questionPK : questionPKs) {
			quizToQuestionTableMapper.deleteTableMapping(pk, questionPK);
		}
	}

	/**
	 * Removes the association between the quiz and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz
	 * @param questions the questions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeQuestions(long pk,
		List<kahoot_source.model.Question> questions) throws SystemException {
		for (kahoot_source.model.Question question : questions) {
			quizToQuestionTableMapper.deleteTableMapping(pk,
				question.getPrimaryKey());
		}
	}

	/**
	 * Sets the questions associated with the quiz, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz
	 * @param questionPKs the primary keys of the questions to be associated with the quiz
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void setQuestions(long pk, long[] questionPKs)
		throws SystemException {
		Set<Long> newQuestionPKsSet = SetUtil.fromArray(questionPKs);
		Set<Long> oldQuestionPKsSet = SetUtil.fromArray(quizToQuestionTableMapper.getRightPrimaryKeys(
					pk));

		Set<Long> removeQuestionPKsSet = new HashSet<Long>(oldQuestionPKsSet);

		removeQuestionPKsSet.removeAll(newQuestionPKsSet);

		for (long removeQuestionPK : removeQuestionPKsSet) {
			quizToQuestionTableMapper.deleteTableMapping(pk, removeQuestionPK);
		}

		newQuestionPKsSet.removeAll(oldQuestionPKsSet);

		for (long newQuestionPK : newQuestionPKsSet) {
			quizToQuestionTableMapper.addTableMapping(pk, newQuestionPK);
		}
	}

	/**
	 * Sets the questions associated with the quiz, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the quiz
	 * @param questions the questions to be associated with the quiz
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void setQuestions(long pk,
		List<kahoot_source.model.Question> questions) throws SystemException {
		try {
			long[] questionPKs = new long[questions.size()];

			for (int i = 0; i < questions.size(); i++) {
				kahoot_source.model.Question question = questions.get(i);

				questionPKs[i] = question.getPrimaryKey();
			}

			setQuestions(pk, questionPKs);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(QuizModelImpl.MAPPING_TABLE_KAHOOT_QUIZ_QUESTION_NAME);
		}
	}

	/**
	 * Initializes the quiz persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.kahoot_source.model.Quiz")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Quiz>> listenersList = new ArrayList<ModelListener<Quiz>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Quiz>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}

		quizToQuizgroupTableMapper = TableMapperFactory.getTableMapper("kahoot_Quiz_Quizgroup",
				"quizid", "quizgroupid", this, quizgroupPersistence);

		quizToQuestionTableMapper = TableMapperFactory.getTableMapper("kahoot_Quiz_Question",
				"quizid", "questionid", this, questionPersistence);
	}

	public void destroy() {
		EntityCacheUtil.removeCache(QuizImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		TableMapperFactory.removeTableMapper("kahoot_Quiz_Quizgroup");
		TableMapperFactory.removeTableMapper("kahoot_Quiz_Question");
	}

	@BeanReference(type = QuizgroupPersistence.class)
	protected QuizgroupPersistence quizgroupPersistence;
	protected TableMapper<Quiz, kahoot_source.model.Quizgroup> quizToQuizgroupTableMapper;
	@BeanReference(type = QuestionPersistence.class)
	protected QuestionPersistence questionPersistence;
	protected TableMapper<Quiz, kahoot_source.model.Question> quizToQuestionTableMapper;
	private static final String _SQL_SELECT_QUIZ = "SELECT quiz FROM Quiz quiz";
	private static final String _SQL_SELECT_QUIZ_WHERE = "SELECT quiz FROM Quiz quiz WHERE ";
	private static final String _SQL_COUNT_QUIZ = "SELECT COUNT(quiz) FROM Quiz quiz";
	private static final String _SQL_COUNT_QUIZ_WHERE = "SELECT COUNT(quiz) FROM Quiz quiz WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "quiz.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Quiz exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Quiz exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(QuizPersistenceImpl.class);
	private static Quiz _nullQuiz = new QuizImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Quiz> toCacheModel() {
				return _nullQuizCacheModel;
			}
		};

	private static CacheModel<Quiz> _nullQuizCacheModel = new CacheModel<Quiz>() {
			@Override
			public Quiz toEntityModel() {
				return _nullQuiz;
			}
		};
}