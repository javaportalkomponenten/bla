package kahoot_source.service.persistence;

import java.util.List;


import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Order;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import de.htwsaar.kahoot.exceptions.UpdateException;
import kahoot_source.model.Quiz;
import kahoot_source.service.QuizLocalServiceUtil;

public class QuizFinderImpl extends BasePersistenceImpl<Quiz> implements QuizFinder{

	public List<Quiz> findByTitleLike(String title) {
		Session session = null;
		try {
			session = openSession();
			
			DynamicQuery quizQuery = DynamicQueryFactoryUtil.forClass(Quiz.class);
			quizQuery.add(RestrictionsFactoryUtil.ilike("title", title));
			
			Order order = OrderFactoryUtil.desc("timestamp");
			quizQuery.addOrder(order);
			
			List<Quiz> resultList = QuizLocalServiceUtil.dynamicQuery(quizQuery);
			return resultList;
			
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			closeSession(session);
		}
		return null;
	}
	
	public List<Quiz> findByTimestampBefore(Long timestamp) {
		Session session = null;
		try {
			session = openSession();
			
			DynamicQuery quizQuery = DynamicQueryFactoryUtil.forClass(Quiz.class);
			quizQuery.add(RestrictionsFactoryUtil.lt("timestamp", timestamp));
			
			Order order = OrderFactoryUtil.desc("timestamp");
			quizQuery.addOrder(order);
			
			List<Quiz> resultList = QuizLocalServiceUtil.dynamicQuery(quizQuery);
			return resultList;
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			closeSession(session);
		}
		return null;
	}
	public List<Quiz> findByTimestampAfter(Long timestamp) {
		Session session = null;
		try {
			session = openSession();
			
			DynamicQuery quizQuery = DynamicQueryFactoryUtil.forClass(Quiz.class);
			quizQuery.add(RestrictionsFactoryUtil.gt("timestamp", timestamp));
			
			Order order = OrderFactoryUtil.desc("timestamp");
			quizQuery.addOrder(order);
			
			List<Quiz> resultList = QuizLocalServiceUtil.dynamicQuery(quizQuery);
			return resultList;
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			closeSession(session);
		}
		return null;
	}
}
