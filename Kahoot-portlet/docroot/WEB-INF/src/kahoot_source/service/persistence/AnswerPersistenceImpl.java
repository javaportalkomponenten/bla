/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service.persistence;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.service.persistence.impl.TableMapper;
import com.liferay.portal.service.persistence.impl.TableMapperFactory;

import kahoot_source.NoSuchAnswerException;

import kahoot_source.model.Answer;

import kahoot_source.model.impl.AnswerImpl;
import kahoot_source.model.impl.AnswerModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the answer service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author krzysiek
 * @see AnswerPersistence
 * @see AnswerUtil
 * @generated
 */
public class AnswerPersistenceImpl extends BasePersistenceImpl<Answer>
	implements AnswerPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AnswerUtil} to access the answer persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AnswerImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AnswerModelImpl.ENTITY_CACHE_ENABLED,
			AnswerModelImpl.FINDER_CACHE_ENABLED, AnswerImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AnswerModelImpl.ENTITY_CACHE_ENABLED,
			AnswerModelImpl.FINDER_CACHE_ENABLED, AnswerImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AnswerModelImpl.ENTITY_CACHE_ENABLED,
			AnswerModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ANSWERID = new FinderPath(AnswerModelImpl.ENTITY_CACHE_ENABLED,
			AnswerModelImpl.FINDER_CACHE_ENABLED, AnswerImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByanswerid",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANSWERID =
		new FinderPath(AnswerModelImpl.ENTITY_CACHE_ENABLED,
			AnswerModelImpl.FINDER_CACHE_ENABLED, AnswerImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByanswerid",
			new String[] { Long.class.getName() },
			AnswerModelImpl.ANSWERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ANSWERID = new FinderPath(AnswerModelImpl.ENTITY_CACHE_ENABLED,
			AnswerModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByanswerid",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the answers where answerid = &#63;.
	 *
	 * @param answerid the answerid
	 * @return the matching answers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Answer> findByanswerid(long answerid) throws SystemException {
		return findByanswerid(answerid, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the answers where answerid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param answerid the answerid
	 * @param start the lower bound of the range of answers
	 * @param end the upper bound of the range of answers (not inclusive)
	 * @return the range of matching answers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Answer> findByanswerid(long answerid, int start, int end)
		throws SystemException {
		return findByanswerid(answerid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the answers where answerid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param answerid the answerid
	 * @param start the lower bound of the range of answers
	 * @param end the upper bound of the range of answers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching answers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Answer> findByanswerid(long answerid, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANSWERID;
			finderArgs = new Object[] { answerid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ANSWERID;
			finderArgs = new Object[] { answerid, start, end, orderByComparator };
		}

		List<Answer> list = (List<Answer>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Answer answer : list) {
				if ((answerid != answer.getAnswerid())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ANSWER_WHERE);

			query.append(_FINDER_COLUMN_ANSWERID_ANSWERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(AnswerModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(answerid);

				if (!pagination) {
					list = (List<Answer>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Answer>(list);
				}
				else {
					list = (List<Answer>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first answer in the ordered set where answerid = &#63;.
	 *
	 * @param answerid the answerid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching answer
	 * @throws kahoot_source.NoSuchAnswerException if a matching answer could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Answer findByanswerid_First(long answerid,
		OrderByComparator orderByComparator)
		throws NoSuchAnswerException, SystemException {
		Answer answer = fetchByanswerid_First(answerid, orderByComparator);

		if (answer != null) {
			return answer;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("answerid=");
		msg.append(answerid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAnswerException(msg.toString());
	}

	/**
	 * Returns the first answer in the ordered set where answerid = &#63;.
	 *
	 * @param answerid the answerid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching answer, or <code>null</code> if a matching answer could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Answer fetchByanswerid_First(long answerid,
		OrderByComparator orderByComparator) throws SystemException {
		List<Answer> list = findByanswerid(answerid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last answer in the ordered set where answerid = &#63;.
	 *
	 * @param answerid the answerid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching answer
	 * @throws kahoot_source.NoSuchAnswerException if a matching answer could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Answer findByanswerid_Last(long answerid,
		OrderByComparator orderByComparator)
		throws NoSuchAnswerException, SystemException {
		Answer answer = fetchByanswerid_Last(answerid, orderByComparator);

		if (answer != null) {
			return answer;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("answerid=");
		msg.append(answerid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAnswerException(msg.toString());
	}

	/**
	 * Returns the last answer in the ordered set where answerid = &#63;.
	 *
	 * @param answerid the answerid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching answer, or <code>null</code> if a matching answer could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Answer fetchByanswerid_Last(long answerid,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByanswerid(answerid);

		if (count == 0) {
			return null;
		}

		List<Answer> list = findByanswerid(answerid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Removes all the answers where answerid = &#63; from the database.
	 *
	 * @param answerid the answerid
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByanswerid(long answerid) throws SystemException {
		for (Answer answer : findByanswerid(answerid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(answer);
		}
	}

	/**
	 * Returns the number of answers where answerid = &#63;.
	 *
	 * @param answerid the answerid
	 * @return the number of matching answers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByanswerid(long answerid) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ANSWERID;

		Object[] finderArgs = new Object[] { answerid };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ANSWER_WHERE);

			query.append(_FINDER_COLUMN_ANSWERID_ANSWERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(answerid);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ANSWERID_ANSWERID_2 = "answer.answerid = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_RIGHT = new FinderPath(AnswerModelImpl.ENTITY_CACHE_ENABLED,
			AnswerModelImpl.FINDER_CACHE_ENABLED, AnswerImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByright",
			new String[] {
				Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RIGHT = new FinderPath(AnswerModelImpl.ENTITY_CACHE_ENABLED,
			AnswerModelImpl.FINDER_CACHE_ENABLED, AnswerImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByright",
			new String[] { Boolean.class.getName() },
			AnswerModelImpl.RIGHT_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_RIGHT = new FinderPath(AnswerModelImpl.ENTITY_CACHE_ENABLED,
			AnswerModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByright",
			new String[] { Boolean.class.getName() });

	/**
	 * Returns all the answers where right = &#63;.
	 *
	 * @param right the right
	 * @return the matching answers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Answer> findByright(boolean right) throws SystemException {
		return findByright(right, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the answers where right = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param right the right
	 * @param start the lower bound of the range of answers
	 * @param end the upper bound of the range of answers (not inclusive)
	 * @return the range of matching answers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Answer> findByright(boolean right, int start, int end)
		throws SystemException {
		return findByright(right, start, end, null);
	}

	/**
	 * Returns an ordered range of all the answers where right = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param right the right
	 * @param start the lower bound of the range of answers
	 * @param end the upper bound of the range of answers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching answers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Answer> findByright(boolean right, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RIGHT;
			finderArgs = new Object[] { right };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_RIGHT;
			finderArgs = new Object[] { right, start, end, orderByComparator };
		}

		List<Answer> list = (List<Answer>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Answer answer : list) {
				if ((right != answer.getRight())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ANSWER_WHERE);

			query.append(_FINDER_COLUMN_RIGHT_RIGHT_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(AnswerModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(right);

				if (!pagination) {
					list = (List<Answer>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Answer>(list);
				}
				else {
					list = (List<Answer>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first answer in the ordered set where right = &#63;.
	 *
	 * @param right the right
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching answer
	 * @throws kahoot_source.NoSuchAnswerException if a matching answer could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Answer findByright_First(boolean right,
		OrderByComparator orderByComparator)
		throws NoSuchAnswerException, SystemException {
		Answer answer = fetchByright_First(right, orderByComparator);

		if (answer != null) {
			return answer;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("right=");
		msg.append(right);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAnswerException(msg.toString());
	}

	/**
	 * Returns the first answer in the ordered set where right = &#63;.
	 *
	 * @param right the right
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching answer, or <code>null</code> if a matching answer could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Answer fetchByright_First(boolean right,
		OrderByComparator orderByComparator) throws SystemException {
		List<Answer> list = findByright(right, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last answer in the ordered set where right = &#63;.
	 *
	 * @param right the right
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching answer
	 * @throws kahoot_source.NoSuchAnswerException if a matching answer could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Answer findByright_Last(boolean right,
		OrderByComparator orderByComparator)
		throws NoSuchAnswerException, SystemException {
		Answer answer = fetchByright_Last(right, orderByComparator);

		if (answer != null) {
			return answer;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("right=");
		msg.append(right);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAnswerException(msg.toString());
	}

	/**
	 * Returns the last answer in the ordered set where right = &#63;.
	 *
	 * @param right the right
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching answer, or <code>null</code> if a matching answer could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Answer fetchByright_Last(boolean right,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByright(right);

		if (count == 0) {
			return null;
		}

		List<Answer> list = findByright(right, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the answers before and after the current answer in the ordered set where right = &#63;.
	 *
	 * @param answerid the primary key of the current answer
	 * @param right the right
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next answer
	 * @throws kahoot_source.NoSuchAnswerException if a answer with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Answer[] findByright_PrevAndNext(long answerid, boolean right,
		OrderByComparator orderByComparator)
		throws NoSuchAnswerException, SystemException {
		Answer answer = findByPrimaryKey(answerid);

		Session session = null;

		try {
			session = openSession();

			Answer[] array = new AnswerImpl[3];

			array[0] = getByright_PrevAndNext(session, answer, right,
					orderByComparator, true);

			array[1] = answer;

			array[2] = getByright_PrevAndNext(session, answer, right,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Answer getByright_PrevAndNext(Session session, Answer answer,
		boolean right, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ANSWER_WHERE);

		query.append(_FINDER_COLUMN_RIGHT_RIGHT_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(AnswerModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(right);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(answer);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Answer> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the answers where right = &#63; from the database.
	 *
	 * @param right the right
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByright(boolean right) throws SystemException {
		for (Answer answer : findByright(right, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(answer);
		}
	}

	/**
	 * Returns the number of answers where right = &#63;.
	 *
	 * @param right the right
	 * @return the number of matching answers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByright(boolean right) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_RIGHT;

		Object[] finderArgs = new Object[] { right };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ANSWER_WHERE);

			query.append(_FINDER_COLUMN_RIGHT_RIGHT_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(right);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_RIGHT_RIGHT_2 = "answer.right = ?";

	public AnswerPersistenceImpl() {
		setModelClass(Answer.class);
	}

	/**
	 * Caches the answer in the entity cache if it is enabled.
	 *
	 * @param answer the answer
	 */
	@Override
	public void cacheResult(Answer answer) {
		EntityCacheUtil.putResult(AnswerModelImpl.ENTITY_CACHE_ENABLED,
			AnswerImpl.class, answer.getPrimaryKey(), answer);

		answer.resetOriginalValues();
	}

	/**
	 * Caches the answers in the entity cache if it is enabled.
	 *
	 * @param answers the answers
	 */
	@Override
	public void cacheResult(List<Answer> answers) {
		for (Answer answer : answers) {
			if (EntityCacheUtil.getResult(
						AnswerModelImpl.ENTITY_CACHE_ENABLED, AnswerImpl.class,
						answer.getPrimaryKey()) == null) {
				cacheResult(answer);
			}
			else {
				answer.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all answers.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(AnswerImpl.class.getName());
		}

		EntityCacheUtil.clearCache(AnswerImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the answer.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Answer answer) {
		EntityCacheUtil.removeResult(AnswerModelImpl.ENTITY_CACHE_ENABLED,
			AnswerImpl.class, answer.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Answer> answers) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Answer answer : answers) {
			EntityCacheUtil.removeResult(AnswerModelImpl.ENTITY_CACHE_ENABLED,
				AnswerImpl.class, answer.getPrimaryKey());
		}
	}

	/**
	 * Creates a new answer with the primary key. Does not add the answer to the database.
	 *
	 * @param answerid the primary key for the new answer
	 * @return the new answer
	 */
	@Override
	public Answer create(long answerid) {
		Answer answer = new AnswerImpl();

		answer.setNew(true);
		answer.setPrimaryKey(answerid);

		return answer;
	}

	/**
	 * Removes the answer with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param answerid the primary key of the answer
	 * @return the answer that was removed
	 * @throws kahoot_source.NoSuchAnswerException if a answer with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Answer remove(long answerid)
		throws NoSuchAnswerException, SystemException {
		return remove((Serializable)answerid);
	}

	/**
	 * Removes the answer with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the answer
	 * @return the answer that was removed
	 * @throws kahoot_source.NoSuchAnswerException if a answer with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Answer remove(Serializable primaryKey)
		throws NoSuchAnswerException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Answer answer = (Answer)session.get(AnswerImpl.class, primaryKey);

			if (answer == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAnswerException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(answer);
		}
		catch (NoSuchAnswerException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Answer removeImpl(Answer answer) throws SystemException {
		answer = toUnwrappedModel(answer);

		answerToQuestionTableMapper.deleteLeftPrimaryKeyTableMappings(answer.getPrimaryKey());

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(answer)) {
				answer = (Answer)session.get(AnswerImpl.class,
						answer.getPrimaryKeyObj());
			}

			if (answer != null) {
				session.delete(answer);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (answer != null) {
			clearCache(answer);
		}

		return answer;
	}

	@Override
	public Answer updateImpl(kahoot_source.model.Answer answer)
		throws SystemException {
		answer = toUnwrappedModel(answer);

		boolean isNew = answer.isNew();

		AnswerModelImpl answerModelImpl = (AnswerModelImpl)answer;

		Session session = null;

		try {
			session = openSession();

			if (answer.isNew()) {
				session.save(answer);

				answer.setNew(false);
			}
			else {
				session.merge(answer);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !AnswerModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((answerModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANSWERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						answerModelImpl.getOriginalAnswerid()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ANSWERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANSWERID,
					args);

				args = new Object[] { answerModelImpl.getAnswerid() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ANSWERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ANSWERID,
					args);
			}

			if ((answerModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RIGHT.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { answerModelImpl.getOriginalRight() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_RIGHT, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RIGHT,
					args);

				args = new Object[] { answerModelImpl.getRight() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_RIGHT, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RIGHT,
					args);
			}
		}

		EntityCacheUtil.putResult(AnswerModelImpl.ENTITY_CACHE_ENABLED,
			AnswerImpl.class, answer.getPrimaryKey(), answer);

		return answer;
	}

	protected Answer toUnwrappedModel(Answer answer) {
		if (answer instanceof AnswerImpl) {
			return answer;
		}

		AnswerImpl answerImpl = new AnswerImpl();

		answerImpl.setNew(answer.isNew());
		answerImpl.setPrimaryKey(answer.getPrimaryKey());

		answerImpl.setAnswerid(answer.getAnswerid());
		answerImpl.setAnswertext(answer.getAnswertext());
		answerImpl.setRight(answer.isRight());

		return answerImpl;
	}

	/**
	 * Returns the answer with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the answer
	 * @return the answer
	 * @throws kahoot_source.NoSuchAnswerException if a answer with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Answer findByPrimaryKey(Serializable primaryKey)
		throws NoSuchAnswerException, SystemException {
		Answer answer = fetchByPrimaryKey(primaryKey);

		if (answer == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchAnswerException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return answer;
	}

	/**
	 * Returns the answer with the primary key or throws a {@link kahoot_source.NoSuchAnswerException} if it could not be found.
	 *
	 * @param answerid the primary key of the answer
	 * @return the answer
	 * @throws kahoot_source.NoSuchAnswerException if a answer with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Answer findByPrimaryKey(long answerid)
		throws NoSuchAnswerException, SystemException {
		return findByPrimaryKey((Serializable)answerid);
	}

	/**
	 * Returns the answer with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the answer
	 * @return the answer, or <code>null</code> if a answer with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Answer fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Answer answer = (Answer)EntityCacheUtil.getResult(AnswerModelImpl.ENTITY_CACHE_ENABLED,
				AnswerImpl.class, primaryKey);

		if (answer == _nullAnswer) {
			return null;
		}

		if (answer == null) {
			Session session = null;

			try {
				session = openSession();

				answer = (Answer)session.get(AnswerImpl.class, primaryKey);

				if (answer != null) {
					cacheResult(answer);
				}
				else {
					EntityCacheUtil.putResult(AnswerModelImpl.ENTITY_CACHE_ENABLED,
						AnswerImpl.class, primaryKey, _nullAnswer);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(AnswerModelImpl.ENTITY_CACHE_ENABLED,
					AnswerImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return answer;
	}

	/**
	 * Returns the answer with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param answerid the primary key of the answer
	 * @return the answer, or <code>null</code> if a answer with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Answer fetchByPrimaryKey(long answerid) throws SystemException {
		return fetchByPrimaryKey((Serializable)answerid);
	}

	/**
	 * Returns all the answers.
	 *
	 * @return the answers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Answer> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the answers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of answers
	 * @param end the upper bound of the range of answers (not inclusive)
	 * @return the range of answers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Answer> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the answers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of answers
	 * @param end the upper bound of the range of answers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of answers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Answer> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Answer> list = (List<Answer>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ANSWER);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ANSWER;

				if (pagination) {
					sql = sql.concat(AnswerModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Answer>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Answer>(list);
				}
				else {
					list = (List<Answer>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the answers from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Answer answer : findAll()) {
			remove(answer);
		}
	}

	/**
	 * Returns the number of answers.
	 *
	 * @return the number of answers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ANSWER);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns all the questions associated with the answer.
	 *
	 * @param pk the primary key of the answer
	 * @return the questions associated with the answer
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<kahoot_source.model.Question> getQuestions(long pk)
		throws SystemException {
		return getQuestions(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the questions associated with the answer.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the answer
	 * @param start the lower bound of the range of answers
	 * @param end the upper bound of the range of answers (not inclusive)
	 * @return the range of questions associated with the answer
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<kahoot_source.model.Question> getQuestions(long pk, int start,
		int end) throws SystemException {
		return getQuestions(pk, start, end, null);
	}

	/**
	 * Returns an ordered range of all the questions associated with the answer.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.AnswerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the answer
	 * @param start the lower bound of the range of answers
	 * @param end the upper bound of the range of answers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of questions associated with the answer
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<kahoot_source.model.Question> getQuestions(long pk, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		return answerToQuestionTableMapper.getRightBaseModels(pk, start, end,
			orderByComparator);
	}

	/**
	 * Returns the number of questions associated with the answer.
	 *
	 * @param pk the primary key of the answer
	 * @return the number of questions associated with the answer
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int getQuestionsSize(long pk) throws SystemException {
		long[] pks = answerToQuestionTableMapper.getRightPrimaryKeys(pk);

		return pks.length;
	}

	/**
	 * Returns <code>true</code> if the question is associated with the answer.
	 *
	 * @param pk the primary key of the answer
	 * @param questionPK the primary key of the question
	 * @return <code>true</code> if the question is associated with the answer; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public boolean containsQuestion(long pk, long questionPK)
		throws SystemException {
		return answerToQuestionTableMapper.containsTableMapping(pk, questionPK);
	}

	/**
	 * Returns <code>true</code> if the answer has any questions associated with it.
	 *
	 * @param pk the primary key of the answer to check for associations with questions
	 * @return <code>true</code> if the answer has any questions associated with it; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public boolean containsQuestions(long pk) throws SystemException {
		if (getQuestionsSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Adds an association between the answer and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the answer
	 * @param questionPK the primary key of the question
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addQuestion(long pk, long questionPK) throws SystemException {
		answerToQuestionTableMapper.addTableMapping(pk, questionPK);
	}

	/**
	 * Adds an association between the answer and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the answer
	 * @param question the question
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addQuestion(long pk, kahoot_source.model.Question question)
		throws SystemException {
		answerToQuestionTableMapper.addTableMapping(pk, question.getPrimaryKey());
	}

	/**
	 * Adds an association between the answer and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the answer
	 * @param questionPKs the primary keys of the questions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addQuestions(long pk, long[] questionPKs)
		throws SystemException {
		for (long questionPK : questionPKs) {
			answerToQuestionTableMapper.addTableMapping(pk, questionPK);
		}
	}

	/**
	 * Adds an association between the answer and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the answer
	 * @param questions the questions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addQuestions(long pk,
		List<kahoot_source.model.Question> questions) throws SystemException {
		for (kahoot_source.model.Question question : questions) {
			answerToQuestionTableMapper.addTableMapping(pk,
				question.getPrimaryKey());
		}
	}

	/**
	 * Clears all associations between the answer and its questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the answer to clear the associated questions from
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void clearQuestions(long pk) throws SystemException {
		answerToQuestionTableMapper.deleteLeftPrimaryKeyTableMappings(pk);
	}

	/**
	 * Removes the association between the answer and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the answer
	 * @param questionPK the primary key of the question
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeQuestion(long pk, long questionPK)
		throws SystemException {
		answerToQuestionTableMapper.deleteTableMapping(pk, questionPK);
	}

	/**
	 * Removes the association between the answer and the question. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the answer
	 * @param question the question
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeQuestion(long pk, kahoot_source.model.Question question)
		throws SystemException {
		answerToQuestionTableMapper.deleteTableMapping(pk,
			question.getPrimaryKey());
	}

	/**
	 * Removes the association between the answer and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the answer
	 * @param questionPKs the primary keys of the questions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeQuestions(long pk, long[] questionPKs)
		throws SystemException {
		for (long questionPK : questionPKs) {
			answerToQuestionTableMapper.deleteTableMapping(pk, questionPK);
		}
	}

	/**
	 * Removes the association between the answer and the questions. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the answer
	 * @param questions the questions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeQuestions(long pk,
		List<kahoot_source.model.Question> questions) throws SystemException {
		for (kahoot_source.model.Question question : questions) {
			answerToQuestionTableMapper.deleteTableMapping(pk,
				question.getPrimaryKey());
		}
	}

	/**
	 * Sets the questions associated with the answer, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the answer
	 * @param questionPKs the primary keys of the questions to be associated with the answer
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void setQuestions(long pk, long[] questionPKs)
		throws SystemException {
		Set<Long> newQuestionPKsSet = SetUtil.fromArray(questionPKs);
		Set<Long> oldQuestionPKsSet = SetUtil.fromArray(answerToQuestionTableMapper.getRightPrimaryKeys(
					pk));

		Set<Long> removeQuestionPKsSet = new HashSet<Long>(oldQuestionPKsSet);

		removeQuestionPKsSet.removeAll(newQuestionPKsSet);

		for (long removeQuestionPK : removeQuestionPKsSet) {
			answerToQuestionTableMapper.deleteTableMapping(pk, removeQuestionPK);
		}

		newQuestionPKsSet.removeAll(oldQuestionPKsSet);

		for (long newQuestionPK : newQuestionPKsSet) {
			answerToQuestionTableMapper.addTableMapping(pk, newQuestionPK);
		}
	}

	/**
	 * Sets the questions associated with the answer, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the answer
	 * @param questions the questions to be associated with the answer
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void setQuestions(long pk,
		List<kahoot_source.model.Question> questions) throws SystemException {
		try {
			long[] questionPKs = new long[questions.size()];

			for (int i = 0; i < questions.size(); i++) {
				kahoot_source.model.Question question = questions.get(i);

				questionPKs[i] = question.getPrimaryKey();
			}

			setQuestions(pk, questionPKs);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(AnswerModelImpl.MAPPING_TABLE_KAHOOT_QUESTION_ANSWER_NAME);
		}
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the answer persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.kahoot_source.model.Answer")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Answer>> listenersList = new ArrayList<ModelListener<Answer>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Answer>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}

		answerToQuestionTableMapper = TableMapperFactory.getTableMapper("kahoot_Question_Answer",
				"answerid", "questionid", this, questionPersistence);
	}

	public void destroy() {
		EntityCacheUtil.removeCache(AnswerImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		TableMapperFactory.removeTableMapper("kahoot_Question_Answer");
	}

	@BeanReference(type = QuestionPersistence.class)
	protected QuestionPersistence questionPersistence;
	protected TableMapper<Answer, kahoot_source.model.Question> answerToQuestionTableMapper;
	private static final String _SQL_SELECT_ANSWER = "SELECT answer FROM Answer answer";
	private static final String _SQL_SELECT_ANSWER_WHERE = "SELECT answer FROM Answer answer WHERE ";
	private static final String _SQL_COUNT_ANSWER = "SELECT COUNT(answer) FROM Answer answer";
	private static final String _SQL_COUNT_ANSWER_WHERE = "SELECT COUNT(answer) FROM Answer answer WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "answer.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Answer exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Answer exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(AnswerPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"right"
			});
	private static Answer _nullAnswer = new AnswerImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Answer> toCacheModel() {
				return _nullAnswerCacheModel;
			}
		};

	private static CacheModel<Answer> _nullAnswerCacheModel = new CacheModel<Answer>() {
			@Override
			public Answer toEntityModel() {
				return _nullAnswer;
			}
		};
}