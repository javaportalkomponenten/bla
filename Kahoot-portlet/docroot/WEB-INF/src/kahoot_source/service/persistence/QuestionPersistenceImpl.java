/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service.persistence;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.service.persistence.impl.TableMapper;
import com.liferay.portal.service.persistence.impl.TableMapperFactory;

import kahoot_source.NoSuchQuestionException;

import kahoot_source.model.Question;

import kahoot_source.model.impl.QuestionImpl;
import kahoot_source.model.impl.QuestionModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the question service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author krzysiek
 * @see QuestionPersistence
 * @see QuestionUtil
 * @generated
 */
public class QuestionPersistenceImpl extends BasePersistenceImpl<Question>
	implements QuestionPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link QuestionUtil} to access the question persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = QuestionImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(QuestionModelImpl.ENTITY_CACHE_ENABLED,
			QuestionModelImpl.FINDER_CACHE_ENABLED, QuestionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(QuestionModelImpl.ENTITY_CACHE_ENABLED,
			QuestionModelImpl.FINDER_CACHE_ENABLED, QuestionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(QuestionModelImpl.ENTITY_CACHE_ENABLED,
			QuestionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_QUESTIONID =
		new FinderPath(QuestionModelImpl.ENTITY_CACHE_ENABLED,
			QuestionModelImpl.FINDER_CACHE_ENABLED, QuestionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByquestionid",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESTIONID =
		new FinderPath(QuestionModelImpl.ENTITY_CACHE_ENABLED,
			QuestionModelImpl.FINDER_CACHE_ENABLED, QuestionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByquestionid",
			new String[] { Long.class.getName() },
			QuestionModelImpl.QUESTIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_QUESTIONID = new FinderPath(QuestionModelImpl.ENTITY_CACHE_ENABLED,
			QuestionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByquestionid",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the questions where questionid = &#63;.
	 *
	 * @param questionid the questionid
	 * @return the matching questions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Question> findByquestionid(long questionid)
		throws SystemException {
		return findByquestionid(questionid, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the questions where questionid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param questionid the questionid
	 * @param start the lower bound of the range of questions
	 * @param end the upper bound of the range of questions (not inclusive)
	 * @return the range of matching questions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Question> findByquestionid(long questionid, int start, int end)
		throws SystemException {
		return findByquestionid(questionid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the questions where questionid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param questionid the questionid
	 * @param start the lower bound of the range of questions
	 * @param end the upper bound of the range of questions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching questions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Question> findByquestionid(long questionid, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESTIONID;
			finderArgs = new Object[] { questionid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_QUESTIONID;
			finderArgs = new Object[] { questionid, start, end, orderByComparator };
		}

		List<Question> list = (List<Question>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Question question : list) {
				if ((questionid != question.getQuestionid())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_QUESTION_WHERE);

			query.append(_FINDER_COLUMN_QUESTIONID_QUESTIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(QuestionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(questionid);

				if (!pagination) {
					list = (List<Question>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Question>(list);
				}
				else {
					list = (List<Question>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first question in the ordered set where questionid = &#63;.
	 *
	 * @param questionid the questionid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching question
	 * @throws kahoot_source.NoSuchQuestionException if a matching question could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Question findByquestionid_First(long questionid,
		OrderByComparator orderByComparator)
		throws NoSuchQuestionException, SystemException {
		Question question = fetchByquestionid_First(questionid,
				orderByComparator);

		if (question != null) {
			return question;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("questionid=");
		msg.append(questionid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuestionException(msg.toString());
	}

	/**
	 * Returns the first question in the ordered set where questionid = &#63;.
	 *
	 * @param questionid the questionid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching question, or <code>null</code> if a matching question could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Question fetchByquestionid_First(long questionid,
		OrderByComparator orderByComparator) throws SystemException {
		List<Question> list = findByquestionid(questionid, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last question in the ordered set where questionid = &#63;.
	 *
	 * @param questionid the questionid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching question
	 * @throws kahoot_source.NoSuchQuestionException if a matching question could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Question findByquestionid_Last(long questionid,
		OrderByComparator orderByComparator)
		throws NoSuchQuestionException, SystemException {
		Question question = fetchByquestionid_Last(questionid, orderByComparator);

		if (question != null) {
			return question;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("questionid=");
		msg.append(questionid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuestionException(msg.toString());
	}

	/**
	 * Returns the last question in the ordered set where questionid = &#63;.
	 *
	 * @param questionid the questionid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching question, or <code>null</code> if a matching question could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Question fetchByquestionid_Last(long questionid,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByquestionid(questionid);

		if (count == 0) {
			return null;
		}

		List<Question> list = findByquestionid(questionid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Removes all the questions where questionid = &#63; from the database.
	 *
	 * @param questionid the questionid
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByquestionid(long questionid) throws SystemException {
		for (Question question : findByquestionid(questionid,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(question);
		}
	}

	/**
	 * Returns the number of questions where questionid = &#63;.
	 *
	 * @param questionid the questionid
	 * @return the number of matching questions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByquestionid(long questionid) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_QUESTIONID;

		Object[] finderArgs = new Object[] { questionid };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_QUESTION_WHERE);

			query.append(_FINDER_COLUMN_QUESTIONID_QUESTIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(questionid);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_QUESTIONID_QUESTIONID_2 = "question.questionid = ?";

	public QuestionPersistenceImpl() {
		setModelClass(Question.class);
	}

	/**
	 * Caches the question in the entity cache if it is enabled.
	 *
	 * @param question the question
	 */
	@Override
	public void cacheResult(Question question) {
		EntityCacheUtil.putResult(QuestionModelImpl.ENTITY_CACHE_ENABLED,
			QuestionImpl.class, question.getPrimaryKey(), question);

		question.resetOriginalValues();
	}

	/**
	 * Caches the questions in the entity cache if it is enabled.
	 *
	 * @param questions the questions
	 */
	@Override
	public void cacheResult(List<Question> questions) {
		for (Question question : questions) {
			if (EntityCacheUtil.getResult(
						QuestionModelImpl.ENTITY_CACHE_ENABLED,
						QuestionImpl.class, question.getPrimaryKey()) == null) {
				cacheResult(question);
			}
			else {
				question.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all questions.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(QuestionImpl.class.getName());
		}

		EntityCacheUtil.clearCache(QuestionImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the question.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Question question) {
		EntityCacheUtil.removeResult(QuestionModelImpl.ENTITY_CACHE_ENABLED,
			QuestionImpl.class, question.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Question> questions) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Question question : questions) {
			EntityCacheUtil.removeResult(QuestionModelImpl.ENTITY_CACHE_ENABLED,
				QuestionImpl.class, question.getPrimaryKey());
		}
	}

	/**
	 * Creates a new question with the primary key. Does not add the question to the database.
	 *
	 * @param questionid the primary key for the new question
	 * @return the new question
	 */
	@Override
	public Question create(long questionid) {
		Question question = new QuestionImpl();

		question.setNew(true);
		question.setPrimaryKey(questionid);

		return question;
	}

	/**
	 * Removes the question with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param questionid the primary key of the question
	 * @return the question that was removed
	 * @throws kahoot_source.NoSuchQuestionException if a question with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Question remove(long questionid)
		throws NoSuchQuestionException, SystemException {
		return remove((Serializable)questionid);
	}

	/**
	 * Removes the question with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the question
	 * @return the question that was removed
	 * @throws kahoot_source.NoSuchQuestionException if a question with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Question remove(Serializable primaryKey)
		throws NoSuchQuestionException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Question question = (Question)session.get(QuestionImpl.class,
					primaryKey);

			if (question == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchQuestionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(question);
		}
		catch (NoSuchQuestionException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Question removeImpl(Question question) throws SystemException {
		question = toUnwrappedModel(question);

		questionToQuizTableMapper.deleteLeftPrimaryKeyTableMappings(question.getPrimaryKey());

		questionToAnswerTableMapper.deleteLeftPrimaryKeyTableMappings(question.getPrimaryKey());

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(question)) {
				question = (Question)session.get(QuestionImpl.class,
						question.getPrimaryKeyObj());
			}

			if (question != null) {
				session.delete(question);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (question != null) {
			clearCache(question);
		}

		return question;
	}

	@Override
	public Question updateImpl(kahoot_source.model.Question question)
		throws SystemException {
		question = toUnwrappedModel(question);

		boolean isNew = question.isNew();

		QuestionModelImpl questionModelImpl = (QuestionModelImpl)question;

		Session session = null;

		try {
			session = openSession();

			if (question.isNew()) {
				session.save(question);

				question.setNew(false);
			}
			else {
				session.merge(question);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !QuestionModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((questionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESTIONID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						questionModelImpl.getOriginalQuestionid()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUESTIONID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESTIONID,
					args);

				args = new Object[] { questionModelImpl.getQuestionid() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUESTIONID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESTIONID,
					args);
			}
		}

		EntityCacheUtil.putResult(QuestionModelImpl.ENTITY_CACHE_ENABLED,
			QuestionImpl.class, question.getPrimaryKey(), question);

		return question;
	}

	protected Question toUnwrappedModel(Question question) {
		if (question instanceof QuestionImpl) {
			return question;
		}

		QuestionImpl questionImpl = new QuestionImpl();

		questionImpl.setNew(question.isNew());
		questionImpl.setPrimaryKey(question.getPrimaryKey());

		questionImpl.setQuestionid(question.getQuestionid());
		questionImpl.setQuestiontext(question.getQuestiontext());
		questionImpl.setTime(question.getTime());
		questionImpl.setBildurl(question.getBildurl());

		return questionImpl;
	}

	/**
	 * Returns the question with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the question
	 * @return the question
	 * @throws kahoot_source.NoSuchQuestionException if a question with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Question findByPrimaryKey(Serializable primaryKey)
		throws NoSuchQuestionException, SystemException {
		Question question = fetchByPrimaryKey(primaryKey);

		if (question == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchQuestionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return question;
	}

	/**
	 * Returns the question with the primary key or throws a {@link kahoot_source.NoSuchQuestionException} if it could not be found.
	 *
	 * @param questionid the primary key of the question
	 * @return the question
	 * @throws kahoot_source.NoSuchQuestionException if a question with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Question findByPrimaryKey(long questionid)
		throws NoSuchQuestionException, SystemException {
		return findByPrimaryKey((Serializable)questionid);
	}

	/**
	 * Returns the question with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the question
	 * @return the question, or <code>null</code> if a question with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Question fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Question question = (Question)EntityCacheUtil.getResult(QuestionModelImpl.ENTITY_CACHE_ENABLED,
				QuestionImpl.class, primaryKey);

		if (question == _nullQuestion) {
			return null;
		}

		if (question == null) {
			Session session = null;

			try {
				session = openSession();

				question = (Question)session.get(QuestionImpl.class, primaryKey);

				if (question != null) {
					cacheResult(question);
				}
				else {
					EntityCacheUtil.putResult(QuestionModelImpl.ENTITY_CACHE_ENABLED,
						QuestionImpl.class, primaryKey, _nullQuestion);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(QuestionModelImpl.ENTITY_CACHE_ENABLED,
					QuestionImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return question;
	}

	/**
	 * Returns the question with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param questionid the primary key of the question
	 * @return the question, or <code>null</code> if a question with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Question fetchByPrimaryKey(long questionid)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)questionid);
	}

	/**
	 * Returns all the questions.
	 *
	 * @return the questions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Question> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the questions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of questions
	 * @param end the upper bound of the range of questions (not inclusive)
	 * @return the range of questions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Question> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the questions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of questions
	 * @param end the upper bound of the range of questions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of questions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Question> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Question> list = (List<Question>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_QUESTION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_QUESTION;

				if (pagination) {
					sql = sql.concat(QuestionModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Question>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Question>(list);
				}
				else {
					list = (List<Question>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the questions from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Question question : findAll()) {
			remove(question);
		}
	}

	/**
	 * Returns the number of questions.
	 *
	 * @return the number of questions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_QUESTION);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns all the quizs associated with the question.
	 *
	 * @param pk the primary key of the question
	 * @return the quizs associated with the question
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<kahoot_source.model.Quiz> getQuizs(long pk)
		throws SystemException {
		return getQuizs(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the quizs associated with the question.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the question
	 * @param start the lower bound of the range of questions
	 * @param end the upper bound of the range of questions (not inclusive)
	 * @return the range of quizs associated with the question
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<kahoot_source.model.Quiz> getQuizs(long pk, int start, int end)
		throws SystemException {
		return getQuizs(pk, start, end, null);
	}

	/**
	 * Returns an ordered range of all the quizs associated with the question.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the question
	 * @param start the lower bound of the range of questions
	 * @param end the upper bound of the range of questions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of quizs associated with the question
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<kahoot_source.model.Quiz> getQuizs(long pk, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return questionToQuizTableMapper.getRightBaseModels(pk, start, end,
			orderByComparator);
	}

	/**
	 * Returns the number of quizs associated with the question.
	 *
	 * @param pk the primary key of the question
	 * @return the number of quizs associated with the question
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int getQuizsSize(long pk) throws SystemException {
		long[] pks = questionToQuizTableMapper.getRightPrimaryKeys(pk);

		return pks.length;
	}

	/**
	 * Returns <code>true</code> if the quiz is associated with the question.
	 *
	 * @param pk the primary key of the question
	 * @param quizPK the primary key of the quiz
	 * @return <code>true</code> if the quiz is associated with the question; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public boolean containsQuiz(long pk, long quizPK) throws SystemException {
		return questionToQuizTableMapper.containsTableMapping(pk, quizPK);
	}

	/**
	 * Returns <code>true</code> if the question has any quizs associated with it.
	 *
	 * @param pk the primary key of the question to check for associations with quizs
	 * @return <code>true</code> if the question has any quizs associated with it; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public boolean containsQuizs(long pk) throws SystemException {
		if (getQuizsSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Adds an association between the question and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question
	 * @param quizPK the primary key of the quiz
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addQuiz(long pk, long quizPK) throws SystemException {
		questionToQuizTableMapper.addTableMapping(pk, quizPK);
	}

	/**
	 * Adds an association between the question and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question
	 * @param quiz the quiz
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addQuiz(long pk, kahoot_source.model.Quiz quiz)
		throws SystemException {
		questionToQuizTableMapper.addTableMapping(pk, quiz.getPrimaryKey());
	}

	/**
	 * Adds an association between the question and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question
	 * @param quizPKs the primary keys of the quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addQuizs(long pk, long[] quizPKs) throws SystemException {
		for (long quizPK : quizPKs) {
			questionToQuizTableMapper.addTableMapping(pk, quizPK);
		}
	}

	/**
	 * Adds an association between the question and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question
	 * @param quizs the quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addQuizs(long pk, List<kahoot_source.model.Quiz> quizs)
		throws SystemException {
		for (kahoot_source.model.Quiz quiz : quizs) {
			questionToQuizTableMapper.addTableMapping(pk, quiz.getPrimaryKey());
		}
	}

	/**
	 * Clears all associations between the question and its quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question to clear the associated quizs from
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void clearQuizs(long pk) throws SystemException {
		questionToQuizTableMapper.deleteLeftPrimaryKeyTableMappings(pk);
	}

	/**
	 * Removes the association between the question and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question
	 * @param quizPK the primary key of the quiz
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeQuiz(long pk, long quizPK) throws SystemException {
		questionToQuizTableMapper.deleteTableMapping(pk, quizPK);
	}

	/**
	 * Removes the association between the question and the quiz. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question
	 * @param quiz the quiz
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeQuiz(long pk, kahoot_source.model.Quiz quiz)
		throws SystemException {
		questionToQuizTableMapper.deleteTableMapping(pk, quiz.getPrimaryKey());
	}

	/**
	 * Removes the association between the question and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question
	 * @param quizPKs the primary keys of the quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeQuizs(long pk, long[] quizPKs) throws SystemException {
		for (long quizPK : quizPKs) {
			questionToQuizTableMapper.deleteTableMapping(pk, quizPK);
		}
	}

	/**
	 * Removes the association between the question and the quizs. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question
	 * @param quizs the quizs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeQuizs(long pk, List<kahoot_source.model.Quiz> quizs)
		throws SystemException {
		for (kahoot_source.model.Quiz quiz : quizs) {
			questionToQuizTableMapper.deleteTableMapping(pk,
				quiz.getPrimaryKey());
		}
	}

	/**
	 * Sets the quizs associated with the question, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question
	 * @param quizPKs the primary keys of the quizs to be associated with the question
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void setQuizs(long pk, long[] quizPKs) throws SystemException {
		Set<Long> newQuizPKsSet = SetUtil.fromArray(quizPKs);
		Set<Long> oldQuizPKsSet = SetUtil.fromArray(questionToQuizTableMapper.getRightPrimaryKeys(
					pk));

		Set<Long> removeQuizPKsSet = new HashSet<Long>(oldQuizPKsSet);

		removeQuizPKsSet.removeAll(newQuizPKsSet);

		for (long removeQuizPK : removeQuizPKsSet) {
			questionToQuizTableMapper.deleteTableMapping(pk, removeQuizPK);
		}

		newQuizPKsSet.removeAll(oldQuizPKsSet);

		for (long newQuizPK : newQuizPKsSet) {
			questionToQuizTableMapper.addTableMapping(pk, newQuizPK);
		}
	}

	/**
	 * Sets the quizs associated with the question, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question
	 * @param quizs the quizs to be associated with the question
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void setQuizs(long pk, List<kahoot_source.model.Quiz> quizs)
		throws SystemException {
		try {
			long[] quizPKs = new long[quizs.size()];

			for (int i = 0; i < quizs.size(); i++) {
				kahoot_source.model.Quiz quiz = quizs.get(i);

				quizPKs[i] = quiz.getPrimaryKey();
			}

			setQuizs(pk, quizPKs);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(QuestionModelImpl.MAPPING_TABLE_KAHOOT_QUIZ_QUESTION_NAME);
		}
	}

	/**
	 * Returns all the answers associated with the question.
	 *
	 * @param pk the primary key of the question
	 * @return the answers associated with the question
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<kahoot_source.model.Answer> getAnswers(long pk)
		throws SystemException {
		return getAnswers(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the answers associated with the question.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the question
	 * @param start the lower bound of the range of questions
	 * @param end the upper bound of the range of questions (not inclusive)
	 * @return the range of answers associated with the question
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<kahoot_source.model.Answer> getAnswers(long pk, int start,
		int end) throws SystemException {
		return getAnswers(pk, start, end, null);
	}

	/**
	 * Returns an ordered range of all the answers associated with the question.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link kahoot_source.model.impl.QuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the question
	 * @param start the lower bound of the range of questions
	 * @param end the upper bound of the range of questions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of answers associated with the question
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<kahoot_source.model.Answer> getAnswers(long pk, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		return questionToAnswerTableMapper.getRightBaseModels(pk, start, end,
			orderByComparator);
	}

	/**
	 * Returns the number of answers associated with the question.
	 *
	 * @param pk the primary key of the question
	 * @return the number of answers associated with the question
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int getAnswersSize(long pk) throws SystemException {
		long[] pks = questionToAnswerTableMapper.getRightPrimaryKeys(pk);

		return pks.length;
	}

	/**
	 * Returns <code>true</code> if the answer is associated with the question.
	 *
	 * @param pk the primary key of the question
	 * @param answerPK the primary key of the answer
	 * @return <code>true</code> if the answer is associated with the question; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public boolean containsAnswer(long pk, long answerPK)
		throws SystemException {
		return questionToAnswerTableMapper.containsTableMapping(pk, answerPK);
	}

	/**
	 * Returns <code>true</code> if the question has any answers associated with it.
	 *
	 * @param pk the primary key of the question to check for associations with answers
	 * @return <code>true</code> if the question has any answers associated with it; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public boolean containsAnswers(long pk) throws SystemException {
		if (getAnswersSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Adds an association between the question and the answer. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question
	 * @param answerPK the primary key of the answer
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addAnswer(long pk, long answerPK) throws SystemException {
		questionToAnswerTableMapper.addTableMapping(pk, answerPK);
	}

	/**
	 * Adds an association between the question and the answer. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question
	 * @param answer the answer
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addAnswer(long pk, kahoot_source.model.Answer answer)
		throws SystemException {
		questionToAnswerTableMapper.addTableMapping(pk, answer.getPrimaryKey());
	}

	/**
	 * Adds an association between the question and the answers. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question
	 * @param answerPKs the primary keys of the answers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addAnswers(long pk, long[] answerPKs) throws SystemException {
		for (long answerPK : answerPKs) {
			questionToAnswerTableMapper.addTableMapping(pk, answerPK);
		}
	}

	/**
	 * Adds an association between the question and the answers. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question
	 * @param answers the answers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void addAnswers(long pk, List<kahoot_source.model.Answer> answers)
		throws SystemException {
		for (kahoot_source.model.Answer answer : answers) {
			questionToAnswerTableMapper.addTableMapping(pk,
				answer.getPrimaryKey());
		}
	}

	/**
	 * Clears all associations between the question and its answers. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question to clear the associated answers from
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void clearAnswers(long pk) throws SystemException {
		questionToAnswerTableMapper.deleteLeftPrimaryKeyTableMappings(pk);
	}

	/**
	 * Removes the association between the question and the answer. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question
	 * @param answerPK the primary key of the answer
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAnswer(long pk, long answerPK) throws SystemException {
		questionToAnswerTableMapper.deleteTableMapping(pk, answerPK);
	}

	/**
	 * Removes the association between the question and the answer. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question
	 * @param answer the answer
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAnswer(long pk, kahoot_source.model.Answer answer)
		throws SystemException {
		questionToAnswerTableMapper.deleteTableMapping(pk,
			answer.getPrimaryKey());
	}

	/**
	 * Removes the association between the question and the answers. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question
	 * @param answerPKs the primary keys of the answers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAnswers(long pk, long[] answerPKs)
		throws SystemException {
		for (long answerPK : answerPKs) {
			questionToAnswerTableMapper.deleteTableMapping(pk, answerPK);
		}
	}

	/**
	 * Removes the association between the question and the answers. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question
	 * @param answers the answers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAnswers(long pk, List<kahoot_source.model.Answer> answers)
		throws SystemException {
		for (kahoot_source.model.Answer answer : answers) {
			questionToAnswerTableMapper.deleteTableMapping(pk,
				answer.getPrimaryKey());
		}
	}

	/**
	 * Sets the answers associated with the question, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question
	 * @param answerPKs the primary keys of the answers to be associated with the question
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void setAnswers(long pk, long[] answerPKs) throws SystemException {
		Set<Long> newAnswerPKsSet = SetUtil.fromArray(answerPKs);
		Set<Long> oldAnswerPKsSet = SetUtil.fromArray(questionToAnswerTableMapper.getRightPrimaryKeys(
					pk));

		Set<Long> removeAnswerPKsSet = new HashSet<Long>(oldAnswerPKsSet);

		removeAnswerPKsSet.removeAll(newAnswerPKsSet);

		for (long removeAnswerPK : removeAnswerPKsSet) {
			questionToAnswerTableMapper.deleteTableMapping(pk, removeAnswerPK);
		}

		newAnswerPKsSet.removeAll(oldAnswerPKsSet);

		for (long newAnswerPK : newAnswerPKsSet) {
			questionToAnswerTableMapper.addTableMapping(pk, newAnswerPK);
		}
	}

	/**
	 * Sets the answers associated with the question, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the question
	 * @param answers the answers to be associated with the question
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void setAnswers(long pk, List<kahoot_source.model.Answer> answers)
		throws SystemException {
		try {
			long[] answerPKs = new long[answers.size()];

			for (int i = 0; i < answers.size(); i++) {
				kahoot_source.model.Answer answer = answers.get(i);

				answerPKs[i] = answer.getPrimaryKey();
			}

			setAnswers(pk, answerPKs);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(QuestionModelImpl.MAPPING_TABLE_KAHOOT_QUESTION_ANSWER_NAME);
		}
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the question persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.kahoot_source.model.Question")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Question>> listenersList = new ArrayList<ModelListener<Question>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Question>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}

		questionToQuizTableMapper = TableMapperFactory.getTableMapper("kahoot_Quiz_Question",
				"questionid", "quizid", this, quizPersistence);

		questionToAnswerTableMapper = TableMapperFactory.getTableMapper("kahoot_Question_Answer",
				"questionid", "answerid", this, answerPersistence);
	}

	public void destroy() {
		EntityCacheUtil.removeCache(QuestionImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		TableMapperFactory.removeTableMapper("kahoot_Quiz_Question");
		TableMapperFactory.removeTableMapper("kahoot_Question_Answer");
	}

	@BeanReference(type = QuizPersistence.class)
	protected QuizPersistence quizPersistence;
	protected TableMapper<Question, kahoot_source.model.Quiz> questionToQuizTableMapper;
	@BeanReference(type = AnswerPersistence.class)
	protected AnswerPersistence answerPersistence;
	protected TableMapper<Question, kahoot_source.model.Answer> questionToAnswerTableMapper;
	private static final String _SQL_SELECT_QUESTION = "SELECT question FROM Question question";
	private static final String _SQL_SELECT_QUESTION_WHERE = "SELECT question FROM Question question WHERE ";
	private static final String _SQL_COUNT_QUESTION = "SELECT COUNT(question) FROM Question question";
	private static final String _SQL_COUNT_QUESTION_WHERE = "SELECT COUNT(question) FROM Question question WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "question.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Question exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Question exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(QuestionPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"time"
			});
	private static Question _nullQuestion = new QuestionImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Question> toCacheModel() {
				return _nullQuestionCacheModel;
			}
		};

	private static CacheModel<Question> _nullQuestionCacheModel = new CacheModel<Question>() {
			@Override
			public Question toEntityModel() {
				return _nullQuestion;
			}
		};
}