/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.service.base;

import kahoot_source.service.QuestionLocalServiceUtil;

import java.util.Arrays;

/**
 * @author krzysiek
 * @generated
 */
public class QuestionLocalServiceClpInvoker {
	public QuestionLocalServiceClpInvoker() {
		_methodName0 = "addQuestion";

		_methodParameterTypes0 = new String[] { "kahoot_source.model.Question" };

		_methodName1 = "createQuestion";

		_methodParameterTypes1 = new String[] { "long" };

		_methodName2 = "deleteQuestion";

		_methodParameterTypes2 = new String[] { "long" };

		_methodName3 = "deleteQuestion";

		_methodParameterTypes3 = new String[] { "kahoot_source.model.Question" };

		_methodName4 = "dynamicQuery";

		_methodParameterTypes4 = new String[] {  };

		_methodName5 = "dynamicQuery";

		_methodParameterTypes5 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName6 = "dynamicQuery";

		_methodParameterTypes6 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
			};

		_methodName7 = "dynamicQuery";

		_methodParameterTypes7 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
				"com.liferay.portal.kernel.util.OrderByComparator"
			};

		_methodName8 = "dynamicQueryCount";

		_methodParameterTypes8 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName9 = "dynamicQueryCount";

		_methodParameterTypes9 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery",
				"com.liferay.portal.kernel.dao.orm.Projection"
			};

		_methodName10 = "fetchQuestion";

		_methodParameterTypes10 = new String[] { "long" };

		_methodName11 = "getQuestion";

		_methodParameterTypes11 = new String[] { "long" };

		_methodName12 = "getPersistedModel";

		_methodParameterTypes12 = new String[] { "java.io.Serializable" };

		_methodName13 = "getQuestions";

		_methodParameterTypes13 = new String[] { "int", "int" };

		_methodName14 = "getQuestionsCount";

		_methodParameterTypes14 = new String[] {  };

		_methodName15 = "updateQuestion";

		_methodParameterTypes15 = new String[] { "kahoot_source.model.Question" };

		_methodName16 = "addQuizQuestion";

		_methodParameterTypes16 = new String[] { "long", "long" };

		_methodName17 = "addQuizQuestion";

		_methodParameterTypes17 = new String[] {
				"long", "kahoot_source.model.Question"
			};

		_methodName18 = "addQuizQuestions";

		_methodParameterTypes18 = new String[] { "long", "long[][]" };

		_methodName19 = "addQuizQuestions";

		_methodParameterTypes19 = new String[] { "long", "java.util.List" };

		_methodName20 = "clearQuizQuestions";

		_methodParameterTypes20 = new String[] { "long" };

		_methodName21 = "deleteQuizQuestion";

		_methodParameterTypes21 = new String[] { "long", "long" };

		_methodName22 = "deleteQuizQuestion";

		_methodParameterTypes22 = new String[] {
				"long", "kahoot_source.model.Question"
			};

		_methodName23 = "deleteQuizQuestions";

		_methodParameterTypes23 = new String[] { "long", "long[][]" };

		_methodName24 = "deleteQuizQuestions";

		_methodParameterTypes24 = new String[] { "long", "java.util.List" };

		_methodName25 = "getQuizQuestions";

		_methodParameterTypes25 = new String[] { "long" };

		_methodName26 = "getQuizQuestions";

		_methodParameterTypes26 = new String[] { "long", "int", "int" };

		_methodName27 = "getQuizQuestions";

		_methodParameterTypes27 = new String[] {
				"long", "int", "int",
				"com.liferay.portal.kernel.util.OrderByComparator"
			};

		_methodName28 = "getQuizQuestionsCount";

		_methodParameterTypes28 = new String[] { "long" };

		_methodName29 = "hasQuizQuestion";

		_methodParameterTypes29 = new String[] { "long", "long" };

		_methodName30 = "hasQuizQuestions";

		_methodParameterTypes30 = new String[] { "long" };

		_methodName31 = "setQuizQuestions";

		_methodParameterTypes31 = new String[] { "long", "long[][]" };

		_methodName32 = "addAnswerQuestion";

		_methodParameterTypes32 = new String[] { "long", "long" };

		_methodName33 = "addAnswerQuestion";

		_methodParameterTypes33 = new String[] {
				"long", "kahoot_source.model.Question"
			};

		_methodName34 = "addAnswerQuestions";

		_methodParameterTypes34 = new String[] { "long", "long[][]" };

		_methodName35 = "addAnswerQuestions";

		_methodParameterTypes35 = new String[] { "long", "java.util.List" };

		_methodName36 = "clearAnswerQuestions";

		_methodParameterTypes36 = new String[] { "long" };

		_methodName37 = "deleteAnswerQuestion";

		_methodParameterTypes37 = new String[] { "long", "long" };

		_methodName38 = "deleteAnswerQuestion";

		_methodParameterTypes38 = new String[] {
				"long", "kahoot_source.model.Question"
			};

		_methodName39 = "deleteAnswerQuestions";

		_methodParameterTypes39 = new String[] { "long", "long[][]" };

		_methodName40 = "deleteAnswerQuestions";

		_methodParameterTypes40 = new String[] { "long", "java.util.List" };

		_methodName41 = "getAnswerQuestions";

		_methodParameterTypes41 = new String[] { "long" };

		_methodName42 = "getAnswerQuestions";

		_methodParameterTypes42 = new String[] { "long", "int", "int" };

		_methodName43 = "getAnswerQuestions";

		_methodParameterTypes43 = new String[] {
				"long", "int", "int",
				"com.liferay.portal.kernel.util.OrderByComparator"
			};

		_methodName44 = "getAnswerQuestionsCount";

		_methodParameterTypes44 = new String[] { "long" };

		_methodName45 = "hasAnswerQuestion";

		_methodParameterTypes45 = new String[] { "long", "long" };

		_methodName46 = "hasAnswerQuestions";

		_methodParameterTypes46 = new String[] { "long" };

		_methodName47 = "setAnswerQuestions";

		_methodParameterTypes47 = new String[] { "long", "long[][]" };

		_methodName78 = "getBeanIdentifier";

		_methodParameterTypes78 = new String[] {  };

		_methodName79 = "setBeanIdentifier";

		_methodParameterTypes79 = new String[] { "java.lang.String" };

		_methodName84 = "addQuestion";

		_methodParameterTypes84 = new String[] {
				"java.lang.String", "java.lang.Long", "int", "java.lang.String",
				"java.util.List", "java.util.List"
			};

		_methodName85 = "updateQuestion";

		_methodParameterTypes85 = new String[] {
				"java.lang.Long", "java.lang.String", "java.lang.Long",
				"java.lang.String", "java.util.List", "java.util.List"
			};

		_methodName88 = "deleteQuestion";

		_methodParameterTypes88 = new String[] { "java.lang.Long" };

		_methodName89 = "deleteQuestion";

		_methodParameterTypes89 = new String[] { "kahoot_source.model.Question" };
	}

	public Object invokeMethod(String name, String[] parameterTypes,
		Object[] arguments) throws Throwable {
		if (_methodName0.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
			return QuestionLocalServiceUtil.addQuestion((kahoot_source.model.Question)arguments[0]);
		}

		if (_methodName1.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
			return QuestionLocalServiceUtil.createQuestion(((Long)arguments[0]).longValue());
		}

		if (_methodName2.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
			return QuestionLocalServiceUtil.deleteQuestion(((Long)arguments[0]).longValue());
		}

		if (_methodName3.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
			return QuestionLocalServiceUtil.deleteQuestion((kahoot_source.model.Question)arguments[0]);
		}

		if (_methodName4.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
			return QuestionLocalServiceUtil.dynamicQuery();
		}

		if (_methodName5.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
			return QuestionLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0]);
		}

		if (_methodName6.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
			return QuestionLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue());
		}

		if (_methodName7.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
			return QuestionLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue(),
				(com.liferay.portal.kernel.util.OrderByComparator)arguments[3]);
		}

		if (_methodName8.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
			return QuestionLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0]);
		}

		if (_methodName9.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
			return QuestionLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				(com.liferay.portal.kernel.dao.orm.Projection)arguments[1]);
		}

		if (_methodName10.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
			return QuestionLocalServiceUtil.fetchQuestion(((Long)arguments[0]).longValue());
		}

		if (_methodName11.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
			return QuestionLocalServiceUtil.getQuestion(((Long)arguments[0]).longValue());
		}

		if (_methodName12.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
			return QuestionLocalServiceUtil.getPersistedModel((java.io.Serializable)arguments[0]);
		}

		if (_methodName13.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
			return QuestionLocalServiceUtil.getQuestions(((Integer)arguments[0]).intValue(),
				((Integer)arguments[1]).intValue());
		}

		if (_methodName14.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
			return QuestionLocalServiceUtil.getQuestionsCount();
		}

		if (_methodName15.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
			return QuestionLocalServiceUtil.updateQuestion((kahoot_source.model.Question)arguments[0]);
		}

		if (_methodName16.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes16, parameterTypes)) {
			QuestionLocalServiceUtil.addQuizQuestion(((Long)arguments[0]).longValue(),
				((Long)arguments[1]).longValue());

			return null;
		}

		if (_methodName17.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes17, parameterTypes)) {
			QuestionLocalServiceUtil.addQuizQuestion(((Long)arguments[0]).longValue(),
				(kahoot_source.model.Question)arguments[1]);

			return null;
		}

		if (_methodName18.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes18, parameterTypes)) {
			QuestionLocalServiceUtil.addQuizQuestions(((Long)arguments[0]).longValue(),
				(long[])arguments[1]);

			return null;
		}

		if (_methodName19.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes19, parameterTypes)) {
			QuestionLocalServiceUtil.addQuizQuestions(((Long)arguments[0]).longValue(),
				(java.util.List<kahoot_source.model.Question>)arguments[1]);

			return null;
		}

		if (_methodName20.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes20, parameterTypes)) {
			QuestionLocalServiceUtil.clearQuizQuestions(((Long)arguments[0]).longValue());

			return null;
		}

		if (_methodName21.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes21, parameterTypes)) {
			QuestionLocalServiceUtil.deleteQuizQuestion(((Long)arguments[0]).longValue(),
				((Long)arguments[1]).longValue());

			return null;
		}

		if (_methodName22.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes22, parameterTypes)) {
			QuestionLocalServiceUtil.deleteQuizQuestion(((Long)arguments[0]).longValue(),
				(kahoot_source.model.Question)arguments[1]);

			return null;
		}

		if (_methodName23.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes23, parameterTypes)) {
			QuestionLocalServiceUtil.deleteQuizQuestions(((Long)arguments[0]).longValue(),
				(long[])arguments[1]);

			return null;
		}

		if (_methodName24.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes24, parameterTypes)) {
			QuestionLocalServiceUtil.deleteQuizQuestions(((Long)arguments[0]).longValue(),
				(java.util.List<kahoot_source.model.Question>)arguments[1]);

			return null;
		}

		if (_methodName25.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes25, parameterTypes)) {
			return QuestionLocalServiceUtil.getQuizQuestions(((Long)arguments[0]).longValue());
		}

		if (_methodName26.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes26, parameterTypes)) {
			return QuestionLocalServiceUtil.getQuizQuestions(((Long)arguments[0]).longValue(),
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue());
		}

		if (_methodName27.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes27, parameterTypes)) {
			return QuestionLocalServiceUtil.getQuizQuestions(((Long)arguments[0]).longValue(),
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue(),
				(com.liferay.portal.kernel.util.OrderByComparator)arguments[3]);
		}

		if (_methodName28.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes28, parameterTypes)) {
			return QuestionLocalServiceUtil.getQuizQuestionsCount(((Long)arguments[0]).longValue());
		}

		if (_methodName29.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes29, parameterTypes)) {
			return QuestionLocalServiceUtil.hasQuizQuestion(((Long)arguments[0]).longValue(),
				((Long)arguments[1]).longValue());
		}

		if (_methodName30.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes30, parameterTypes)) {
			return QuestionLocalServiceUtil.hasQuizQuestions(((Long)arguments[0]).longValue());
		}

		if (_methodName31.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes31, parameterTypes)) {
			QuestionLocalServiceUtil.setQuizQuestions(((Long)arguments[0]).longValue(),
				(long[])arguments[1]);

			return null;
		}

		if (_methodName32.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes32, parameterTypes)) {
			QuestionLocalServiceUtil.addAnswerQuestion(((Long)arguments[0]).longValue(),
				((Long)arguments[1]).longValue());

			return null;
		}

		if (_methodName33.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes33, parameterTypes)) {
			QuestionLocalServiceUtil.addAnswerQuestion(((Long)arguments[0]).longValue(),
				(kahoot_source.model.Question)arguments[1]);

			return null;
		}

		if (_methodName34.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes34, parameterTypes)) {
			QuestionLocalServiceUtil.addAnswerQuestions(((Long)arguments[0]).longValue(),
				(long[])arguments[1]);

			return null;
		}

		if (_methodName35.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes35, parameterTypes)) {
			QuestionLocalServiceUtil.addAnswerQuestions(((Long)arguments[0]).longValue(),
				(java.util.List<kahoot_source.model.Question>)arguments[1]);

			return null;
		}

		if (_methodName36.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes36, parameterTypes)) {
			QuestionLocalServiceUtil.clearAnswerQuestions(((Long)arguments[0]).longValue());

			return null;
		}

		if (_methodName37.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes37, parameterTypes)) {
			QuestionLocalServiceUtil.deleteAnswerQuestion(((Long)arguments[0]).longValue(),
				((Long)arguments[1]).longValue());

			return null;
		}

		if (_methodName38.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes38, parameterTypes)) {
			QuestionLocalServiceUtil.deleteAnswerQuestion(((Long)arguments[0]).longValue(),
				(kahoot_source.model.Question)arguments[1]);

			return null;
		}

		if (_methodName39.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes39, parameterTypes)) {
			QuestionLocalServiceUtil.deleteAnswerQuestions(((Long)arguments[0]).longValue(),
				(long[])arguments[1]);

			return null;
		}

		if (_methodName40.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes40, parameterTypes)) {
			QuestionLocalServiceUtil.deleteAnswerQuestions(((Long)arguments[0]).longValue(),
				(java.util.List<kahoot_source.model.Question>)arguments[1]);

			return null;
		}

		if (_methodName41.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes41, parameterTypes)) {
			return QuestionLocalServiceUtil.getAnswerQuestions(((Long)arguments[0]).longValue());
		}

		if (_methodName42.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes42, parameterTypes)) {
			return QuestionLocalServiceUtil.getAnswerQuestions(((Long)arguments[0]).longValue(),
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue());
		}

		if (_methodName43.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes43, parameterTypes)) {
			return QuestionLocalServiceUtil.getAnswerQuestions(((Long)arguments[0]).longValue(),
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue(),
				(com.liferay.portal.kernel.util.OrderByComparator)arguments[3]);
		}

		if (_methodName44.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes44, parameterTypes)) {
			return QuestionLocalServiceUtil.getAnswerQuestionsCount(((Long)arguments[0]).longValue());
		}

		if (_methodName45.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes45, parameterTypes)) {
			return QuestionLocalServiceUtil.hasAnswerQuestion(((Long)arguments[0]).longValue(),
				((Long)arguments[1]).longValue());
		}

		if (_methodName46.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes46, parameterTypes)) {
			return QuestionLocalServiceUtil.hasAnswerQuestions(((Long)arguments[0]).longValue());
		}

		if (_methodName47.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes47, parameterTypes)) {
			QuestionLocalServiceUtil.setAnswerQuestions(((Long)arguments[0]).longValue(),
				(long[])arguments[1]);

			return null;
		}

		if (_methodName78.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes78, parameterTypes)) {
			return QuestionLocalServiceUtil.getBeanIdentifier();
		}

		if (_methodName79.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes79, parameterTypes)) {
			QuestionLocalServiceUtil.setBeanIdentifier((java.lang.String)arguments[0]);

			return null;
		}

		if (_methodName84.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes84, parameterTypes)) {
			return QuestionLocalServiceUtil.addQuestion((java.lang.String)arguments[0],
				(java.lang.Long)arguments[1],
				((Integer)arguments[2]).intValue(),
				(java.lang.String)arguments[3],
				(java.util.List<kahoot_source.model.Quiz>)arguments[4],
				(java.util.List<kahoot_source.model.Answer>)arguments[5]);
		}

		if (_methodName85.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes85, parameterTypes)) {
			return QuestionLocalServiceUtil.updateQuestion((java.lang.Long)arguments[0],
				(java.lang.String)arguments[1], (java.lang.Long)arguments[2],
				(java.lang.String)arguments[3],
				(java.util.List<kahoot_source.model.Quiz>)arguments[4],
				(java.util.List<kahoot_source.model.Answer>)arguments[5]);
		}

		if (_methodName88.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes88, parameterTypes)) {
			return QuestionLocalServiceUtil.deleteQuestion((java.lang.Long)arguments[0]);
		}

		if (_methodName89.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes89, parameterTypes)) {
			return QuestionLocalServiceUtil.deleteQuestion((kahoot_source.model.Question)arguments[0]);
		}

		throw new UnsupportedOperationException();
	}

	private String _methodName0;
	private String[] _methodParameterTypes0;
	private String _methodName1;
	private String[] _methodParameterTypes1;
	private String _methodName2;
	private String[] _methodParameterTypes2;
	private String _methodName3;
	private String[] _methodParameterTypes3;
	private String _methodName4;
	private String[] _methodParameterTypes4;
	private String _methodName5;
	private String[] _methodParameterTypes5;
	private String _methodName6;
	private String[] _methodParameterTypes6;
	private String _methodName7;
	private String[] _methodParameterTypes7;
	private String _methodName8;
	private String[] _methodParameterTypes8;
	private String _methodName9;
	private String[] _methodParameterTypes9;
	private String _methodName10;
	private String[] _methodParameterTypes10;
	private String _methodName11;
	private String[] _methodParameterTypes11;
	private String _methodName12;
	private String[] _methodParameterTypes12;
	private String _methodName13;
	private String[] _methodParameterTypes13;
	private String _methodName14;
	private String[] _methodParameterTypes14;
	private String _methodName15;
	private String[] _methodParameterTypes15;
	private String _methodName16;
	private String[] _methodParameterTypes16;
	private String _methodName17;
	private String[] _methodParameterTypes17;
	private String _methodName18;
	private String[] _methodParameterTypes18;
	private String _methodName19;
	private String[] _methodParameterTypes19;
	private String _methodName20;
	private String[] _methodParameterTypes20;
	private String _methodName21;
	private String[] _methodParameterTypes21;
	private String _methodName22;
	private String[] _methodParameterTypes22;
	private String _methodName23;
	private String[] _methodParameterTypes23;
	private String _methodName24;
	private String[] _methodParameterTypes24;
	private String _methodName25;
	private String[] _methodParameterTypes25;
	private String _methodName26;
	private String[] _methodParameterTypes26;
	private String _methodName27;
	private String[] _methodParameterTypes27;
	private String _methodName28;
	private String[] _methodParameterTypes28;
	private String _methodName29;
	private String[] _methodParameterTypes29;
	private String _methodName30;
	private String[] _methodParameterTypes30;
	private String _methodName31;
	private String[] _methodParameterTypes31;
	private String _methodName32;
	private String[] _methodParameterTypes32;
	private String _methodName33;
	private String[] _methodParameterTypes33;
	private String _methodName34;
	private String[] _methodParameterTypes34;
	private String _methodName35;
	private String[] _methodParameterTypes35;
	private String _methodName36;
	private String[] _methodParameterTypes36;
	private String _methodName37;
	private String[] _methodParameterTypes37;
	private String _methodName38;
	private String[] _methodParameterTypes38;
	private String _methodName39;
	private String[] _methodParameterTypes39;
	private String _methodName40;
	private String[] _methodParameterTypes40;
	private String _methodName41;
	private String[] _methodParameterTypes41;
	private String _methodName42;
	private String[] _methodParameterTypes42;
	private String _methodName43;
	private String[] _methodParameterTypes43;
	private String _methodName44;
	private String[] _methodParameterTypes44;
	private String _methodName45;
	private String[] _methodParameterTypes45;
	private String _methodName46;
	private String[] _methodParameterTypes46;
	private String _methodName47;
	private String[] _methodParameterTypes47;
	private String _methodName78;
	private String[] _methodParameterTypes78;
	private String _methodName79;
	private String[] _methodParameterTypes79;
	private String _methodName84;
	private String[] _methodParameterTypes84;
	private String _methodName85;
	private String[] _methodParameterTypes85;
	private String _methodName88;
	private String[] _methodParameterTypes88;
	private String _methodName89;
	private String[] _methodParameterTypes89;
}