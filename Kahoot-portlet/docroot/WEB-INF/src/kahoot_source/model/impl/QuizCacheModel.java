/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import kahoot_source.model.Quiz;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Quiz in entity cache.
 *
 * @author krzysiek
 * @see Quiz
 * @generated
 */
public class QuizCacheModel implements CacheModel<Quiz>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{quizid=");
		sb.append(quizid);
		sb.append(", title=");
		sb.append(title);
		sb.append(", timestamp=");
		sb.append(timestamp);
		sb.append(", running=");
		sb.append(running);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Quiz toEntityModel() {
		QuizImpl quizImpl = new QuizImpl();

		quizImpl.setQuizid(quizid);

		if (title == null) {
			quizImpl.setTitle(StringPool.BLANK);
		}
		else {
			quizImpl.setTitle(title);
		}

		quizImpl.setTimestamp(timestamp);
		quizImpl.setRunning(running);

		quizImpl.resetOriginalValues();

		return quizImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		quizid = objectInput.readLong();
		title = objectInput.readUTF();
		timestamp = objectInput.readLong();
		running = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(quizid);

		if (title == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(title);
		}

		objectOutput.writeLong(timestamp);
		objectOutput.writeBoolean(running);
	}

	public long quizid;
	public String title;
	public long timestamp;
	public boolean running;
}