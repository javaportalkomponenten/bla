/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import kahoot_source.model.Question;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Question in entity cache.
 *
 * @author krzysiek
 * @see Question
 * @generated
 */
public class QuestionCacheModel implements CacheModel<Question>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{questionid=");
		sb.append(questionid);
		sb.append(", questiontext=");
		sb.append(questiontext);
		sb.append(", time=");
		sb.append(time);
		sb.append(", bildurl=");
		sb.append(bildurl);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Question toEntityModel() {
		QuestionImpl questionImpl = new QuestionImpl();

		questionImpl.setQuestionid(questionid);

		if (questiontext == null) {
			questionImpl.setQuestiontext(StringPool.BLANK);
		}
		else {
			questionImpl.setQuestiontext(questiontext);
		}

		questionImpl.setTime(time);

		if (bildurl == null) {
			questionImpl.setBildurl(StringPool.BLANK);
		}
		else {
			questionImpl.setBildurl(bildurl);
		}

		questionImpl.resetOriginalValues();

		return questionImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		questionid = objectInput.readLong();
		questiontext = objectInput.readUTF();
		time = objectInput.readLong();
		bildurl = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(questionid);

		if (questiontext == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(questiontext);
		}

		objectOutput.writeLong(time);

		if (bildurl == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(bildurl);
		}
	}

	public long questionid;
	public String questiontext;
	public Long time;
	public String bildurl;
}