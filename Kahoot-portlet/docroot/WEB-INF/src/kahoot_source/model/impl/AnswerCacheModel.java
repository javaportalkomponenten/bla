/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package kahoot_source.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import kahoot_source.model.Answer;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Answer in entity cache.
 *
 * @author krzysiek
 * @see Answer
 * @generated
 */
public class AnswerCacheModel implements CacheModel<Answer>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{answerid=");
		sb.append(answerid);
		sb.append(", answertext=");
		sb.append(answertext);
		sb.append(", right=");
		sb.append(right);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Answer toEntityModel() {
		AnswerImpl answerImpl = new AnswerImpl();

		answerImpl.setAnswerid(answerid);

		if (answertext == null) {
			answerImpl.setAnswertext(StringPool.BLANK);
		}
		else {
			answerImpl.setAnswertext(answertext);
		}

		answerImpl.setRight(right);

		answerImpl.resetOriginalValues();

		return answerImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		answerid = objectInput.readLong();
		answertext = objectInput.readUTF();
		right = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(answerid);

		if (answertext == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(answertext);
		}

		objectOutput.writeBoolean(right);
	}

	public long answerid;
	public String answertext;
	public boolean right;
}