package kahoot_source.liferaymvc;

import com.liferay.util.bridges.mvc.MVCPortlet;

import kahoot_source.model.Quiz;
import kahoot_source.model.impl.QuizImpl;
import kahoot_source.service.QuizLocalServiceUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import java.io.IOException;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;

public class QuizMVCPortletAction {

	private static Log log = LogFactoryUtil.getLog(QuizMVCPortletAction.class);

	// Analog weitere Funktionen definieren updateQuiz, deleteQuiz,....
	// F�r Question, Answer, Quizgroup... je nachdem wie es sinnvoll ist.
	public void addQuiz(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException {
		try {

			String title = ParamUtil.getString(actionRequest, "title");
			long timestamp = ParamUtil.getLong(actionRequest, "timestamp");
			boolean isRunning = ParamUtil.getBoolean(actionRequest, "isrunning");
			boolean isNew = ParamUtil.getBoolean(actionRequest, "isnew");
			// Quiz einf�gen
			// Prim�r Schl�ssel erstellen
			long quizId = CounterLocalServiceUtil.increment();
			// Quiz-Persistence Objekt erstellen
			Quiz quiz = null;
			quiz = new QuizImpl();
			quiz.setTitle(title);
			quiz.setTimestamp(timestamp);
			quiz.setRunning(isRunning);
			quiz.setNew(isNew);

			// Quiz Persistence-Objekt zur Quiz-Tabelle in DB einf�gen
			quiz = QuizLocalServiceUtil.addQuiz(quiz);

			if (quiz != null) {

				// Meldungen
				SessionMessages.add(actionRequest.getPortletSession(), "quiz-add-success");
				log.info("Quiz erfolgreich hinzugef�gt");
			} else {
				SessionErrors.add(actionRequest.getPortletSession(), "quiz-add-error");
				log.error("Fehler beim Hinzuf�gen");
			}

			// navigate to add_quiz.jsp page
			actionResponse.setRenderParameter("mvcPath", "/html/jsps/add_quiz.jsp");

		} catch (Exception e) {
			SessionErrors.add(actionRequest.getPortletSession(), "quiz-add-error");
			e.printStackTrace();
		}
	}
	
	public void removeQuiz(ActionRequest actionRequest,ActionResponse actionResponse) throws PortalException, SystemException{
	try{
		long id = ParamUtil.getLong(actionRequest, "ID");
	
		QuizLocalServiceUtil.deleteQuiz(id);
	} catch (Exception e){
		SessionErrors.add(actionRequest.getPortletSession(), "quiz-remove-error");
	}
	}
	
	public void updateQuiz(ActionRequest actionRequest, ActionResponse actionResponse) {
		
	}

	// Analog weitere Funktionen definieren updateQuiz, deleteQuiz,....
	// F�r Question, Answer, Quizgroup... je nachdem wie es sinnvoll ist.

}
