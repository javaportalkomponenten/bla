package kahoot_source.liferaymvc;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;

import kahoot_source.model.Question;
import kahoot_source.model.impl.QuestionImpl;
import kahoot_source.service.QuestionLocalServiceUtil;

public class QuestionMVCPortletAction {
	
	private static Log log = LogFactoryUtil.getLog(QuizMVCPortletAction.class);

	// Analog weitere Funktionen definieren updateQuiz, deleteQuiz,....
	// F�r Question, Answer, Quizgroup... je nachdem wie es sinnvoll ist.
	public void addQuestion(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException {
		try {
			System.out.println("BLABLALBALA");
			
			String questiontext = ParamUtil.getString(actionRequest, "questiontext");
			long time = ParamUtil.getLong(actionRequest, "time");
			String bildurl = ParamUtil.getString(actionRequest, "bildurl");


			// Question einf�gen
			// Prim�r Schl�ssel erstellen
			long questionId = CounterLocalServiceUtil.increment();
			// Question-Persistence Objekt erstellen
			Question question = null;
			question = new QuestionImpl();
			
			question.setQuestiontext(questiontext);
			question.setTime(time);
			question.setBildurl(bildurl);
			
	

			// Quiz Persistence-Objekt zur Quiz-Tabelle in DB einf�gen
			question = QuestionLocalServiceUtil.addQuestion(question);

			if (question != null) {

				// Meldungen
				SessionMessages.add(actionRequest.getPortletSession(), "question-add-success");
				log.info("Question erfolgreich hinzugef�gt");
			} else {
				SessionErrors.add(actionRequest.getPortletSession(), "question-add-error");
				log.error("Fehler beim Hinzuf�gen");
			}

			// navigate to add_quiz.jsp page
			actionResponse.setRenderParameter("mvcPath", "/html/jsps/add_question.jsp");

		} catch (Exception e) {
			SessionErrors.add(actionRequest.getPortletSession(), "question-add-error");
			e.printStackTrace();
		}
	}

}
