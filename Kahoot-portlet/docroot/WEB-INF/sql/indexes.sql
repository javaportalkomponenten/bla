create index IX_645BAD0E on kahoot_Answer (answerid);
create index IX_A3C9BBB8 on kahoot_Answer (right_);

create index IX_C4B28B5E on kahoot_Question (questionid);

create index IX_E3E6AACD on kahoot_Question_Answer (answerid);
create index IX_AA2DBA75 on kahoot_Question_Answer (questionid);

create index IX_64613DFC on kahoot_Quiz (quizid);
create index IX_B01563D7 on kahoot_Quiz (running);
create index IX_D85C9FB0 on kahoot_Quiz (title);

create index IX_F502E7CE on kahoot_Quiz_Question (questionid);
create index IX_91EE43D on kahoot_Quiz_Question (quizid);

create index IX_282DB4F2 on kahoot_Quiz_Quizgroup (quizgroupid);
create index IX_7BC47167 on kahoot_Quiz_Quizgroup (quizid);

create index IX_B49172B2 on kahoot_Quizgroup (name);
create index IX_CA82B482 on kahoot_Quizgroup (quizgroupid);

create index IX_BC275B28 on kahoot_Quizgroup_Quiz (quizgroupid);
create index IX_B7DD4171 on kahoot_Quizgroup_Quiz (quizid);