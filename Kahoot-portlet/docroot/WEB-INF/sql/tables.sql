create table kahoot_Answer (
	answerid LONG not null primary key,
	answertext VARCHAR(75) null,
	right_ BOOLEAN
);

create table kahoot_Question (
	questionid LONG not null primary key,
	questiontext VARCHAR(75) null,
	time_ LONG,
	bildurl VARCHAR(75) null
);

create table kahoot_Question_Answer (
	answerid LONG not null,
	questionid LONG not null,
	primary key (answerid, questionid)
);

create table kahoot_Quiz (
	quizid LONG not null primary key,
	title VARCHAR(75) null,
	timestamp LONG,
	running BOOLEAN
);

create table kahoot_Quiz_Question (
	questionid LONG not null,
	quizid LONG not null,
	primary key (questionid, quizid)
);

create table kahoot_Quiz_Quizgroup (
	quizid LONG not null,
	quizgroupid LONG not null,
	primary key (quizid, quizgroupid)
);

create table kahoot_Quizgroup (
	quizgroupid LONG not null primary key,
	name VARCHAR(75) null
);

create table kahoot_Quizgroup_Quiz (
	quizid LONG not null,
	quizgroupid LONG not null,
	primary key (quizid, quizgroupid)
);