<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionErrors"%>

<portlet:defineObjects />
<portlet:renderURL var="homeURL"></portlet:renderURL>
<portlet:actionURL var="addQuestionActionURL" windowState="normal"
	name="addQuestion">
</portlet:actionURL>
<%
	if (SessionMessages.contains(renderRequest.getPortletSession(), "question-add-success")) {
%>
<liferay-ui:success key="question-add-success"
	message="Question Information sind erfolgreich hinzugefügt." />
<%
	}
%>

<%
	if (SessionErrors.contains(renderRequest.getPortletSession(), "question-add-error")) {
%>
<liferay-ui:error key="question-add-error"
	message="Fehler beim Hinzufügen vom Question, bitte versuchen Sie es erneut" />
<%
	}
%>
<h2>Add Question</h2>
<a href="<%=homeURL.toString()%>">Home</a>
<br />
<br />
<br />

<form action="<%=addQuestionActionURL%>" name="questionForm"
	method="POST">
	<b>Text</b><br /> <input type="text"
		name="<portlet:namespace/>questiontext"
		id="<portlet:namespace/>questiontext" /><br /> <b>questiontext</b><br /> <input
		type="long" name="<portlet:namespace/>time"
		id="<portlet:namespace/>time" /><br /> <b>time</b><br /> <input
		type="string" name="<portlet:namespace/>bildUrl"
		id="<portlet:namespace/>bildUrl" /><br /> <b>BuildUrl</b><br /> <input
		type="text" name="<portlet:namespace/>isnew"
		id="<portlet:namespace/>isnew" /><br /> <input
		name="<portlet:namespace/>isnew"
		id="<portlet:namespace/>isnew" type="checkbox" checked="checked">
	<input type="submit" name="addQuestion" id="addQuestion"
		value="Add Question" />
</form>