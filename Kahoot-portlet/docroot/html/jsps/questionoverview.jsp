<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="/init.jsp" %>

<liferay-ui:search-container emptyResultsMessage="No Questions available."
	headerNames="Questiontext, Timestamp"
	
	delta="5"
	deltaConfigurable="true"
	>
	<liferay-ui:search-container-results>
		<%
			total = QuestionLocalServiceUtil.getQuestionsCount();
		    results=QuestionLocalServiceUtil.getQuestions(searchContainer.getStart(), searchContainer.getEnd());
			searchContainer.setTotal(total);
			searchContainer.setResults(results);
		%>
	</liferay-ui:search-container-results>
	<liferay-ui:search-container-row className="kahoot_source.model.Question" keyProperty="questionid" modelVar="currentQuestion">
		<liferay-ui:search-container-column-text href="<%= currentQuestion %>"
			name="questiontext" property="questiontext" />

		<liferay-ui:search-container-column-text 
			name="time" property="time" />
	</liferay-ui:search-container-row>
		</liferay-ui:search-container>
