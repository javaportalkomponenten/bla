<%@page import="de.htwsaar.kahoot.portlet.KahootPortlet"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>

<portlet:defineObjects />
<portlet:renderURL var="homeURL"></portlet:renderURL>
<portlet:actionURL var="addQuizActionURL" windowState="normal"
	name="addQuiz">
</portlet:actionURL>

<%
	if (SessionMessages.contains(renderRequest.getPortletSession(), "quiz-add-success")) {
%>
<liferay-ui:success key="quiz-add-success"
	message="Quiz Information sind erfolgreich hinzugef�gt." />
<%
	}
%>

<%
	if (SessionErrors.contains(renderRequest.getPortletSession(), "quiz-add-error")) {
%>
<liferay-ui:error key="quiz-add-error"
	message="Fehler beim Hinzuf�gen vom Quiz, bitte versuchen Sie es erneut" />
<%
	}
%>
<h2>
	Add Quiz
	<h2>
		<a href="<%=homeURL.toString()%>">Home</a><br />
		<br />
		<br />

		<form action="<%=addQuizActionURL%>" name="quizForm" method="POST">
			<b>Title</b><br /> <input type="text"
				name="<portlet:namespace/>title" id="<portlet:namespace/>title" /><br />
			<b>TimeStamp</b><br /> <input type="text"
				name="<portlet:namespace/>timestamp"
				id="<portlet:namespace/>timestamp" /><br /> <b>IsRunning</b><br /> <input
				type="boolean" name="<portlet:namespace/>isrunning"
				id="<portlet:namespace/>isrunning" /><br /> <input type="submit"
				name="addQuiz" id="addQuiz" value="Add Quiz" />
		</form>

		