<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<portlet:defineObjects />
<liferay-theme:defineObjects />
<h1>Question CRUD Operations</h1>
<portlet:renderURL var="addQuestion">
<portlet:param name="mvcPath" value="/html/jsps/add_question.jsp"/>
</portlet:renderURL>
<portlet:renderURL var="updateQuestion">
<portlet:param name="mvcPath" value="/html/jsps/update_question.jsp"/>
</portlet:renderURL>
<portlet:renderURL var="questionSearch">
<portlet:param name="mvcPath" value="/html/jsps/question_search_container.jsp"/>
</portlet:renderURL>
<portlet:renderURL var="deleteQuestion">
<portlet:param name="mvcPath" value="/html/jsps/delete_question.jsp"/>
</portlet:renderURL>
<portlet:renderURL var="viewAllQuestions">
<portlet:param name="mvcPath" value="/html/jsps/view_all_questions.jsp"/>
</portlet:renderURL>

<portlet:renderURL var="addQuiz">
<portlet:param name="mvcPath" value="/html/jsps/add_quiz.jsp"/>
</portlet:renderURL>


<br/>
<h2>jep</h2>
<a href="<%=addQuestion.toString()%>">Add Question</a><br/>
<a href="<%=updateQuestion.toString()%>">Update Question</a><br/>
<a href="<%=deleteQuestion.toString()%>">Delete Question</a><br/>
<a href="<%=questionSearch.toString()%>">Question Search</a><br/>
<a href="<%=viewAllQuestions.toString()%>">View All Questions</a><br/>
<a href="<%=addQuiz.toString()%>">Add Quiz</a><br/>